#pragma once

static inline int compute_elapsed(struct timeval *start, struct timeval *end)
{
    return ((end->tv_sec * 1000000 + end->tv_usec)
            - (start->tv_sec * 1000000 + start->tv_usec));
}

void radix_sort(unsigned	*begin,	unsigned *end);
void radix_sort64(uint64_t	*begin,	uint64_t *end);

// gpu_look.cu
void gpu_lookup(AccAlign *f, int gpu_id,
        size_t *d_fseed_hash, size_t *d_rseed_hash, int nseeds,
        unsigned nreads, unsigned rlen,
        int &total_fhits, int &total_rhits,
        uint64_t **d_fhits, uint64_t **d_rhits);
void cpu_seed_lookup(uint32_t *keyv, size_t *d_hash, int total_nseeds,
                     uint32_t *start_pos, uint32_t *end_pos, uint32_t *npos);
void gpu_seedhash_and_embedread(int gpu_id, char *input, int nreads, unsigned rlen, int nseeds, int nwindows, int kmer_window,
        size_t **d_seed_hash, Embedding *e, char **d_Qembed);
void gpu_parse(int gpu_id, vector<const char *> &reads, uint8_t *code, char *rcsymbol, int rlen,
        char **d_fParsed, char **d_rParsed, char **d_rParsed_str);

// gpu_sort.cu
void gpu_countBykey(int gpu_id, int num_items, uint64_t *d_key, uint64_t **d_unique_key, uint64_t **d_count, int &nunique_key);
void gpu_sort(int gpu_id, uint64_t *d_hits, int &size);
void gpu_dedup_and_prioritize(int gpu_id, uint64_t *d_input, int ninput, uint64_t **d_uniq_inputs, uint64_t **d_counts,
        int &nunique, uint64_t **d_hcov_hit, uint64_t **d_hcov_cnt, int nreads, int &nhcov_hits);
void gpu_over_one_hit(int gpu_id, uint64_t *d_hcov_fcounts, uint64_t *d_hcov_rcounts, int nreads, bool **d_over_one_hit);
void gpu_remove_hits(int gpu_id, uint64_t *d_uniq_fhits, uint64_t *d_fcounts,
                     int &n_funique, bool *d_over_one_hit);
void gpu_combine_hcov_hamming(int gpu_id, uint64_t *d_hcov_fhamming, uint64_t *d_hcov_rhamming, int nreads,
                              uint64_t **d_hcov_hamming);
void gpu_hit_offset_per_read(int gpu_id, int nunique, uint64_t *_d_uniq_input,
                             int &nreads_has_hits, uint64_t **d_cnt_offset);
void gpu_fullfill_hamming(int gpu_id, int nreads, int nhcov_hits, int max_hamming, uint64_t *d_hcov_hamming, uint64_t **d_filled_hamming);
void gpu_get_best_pos(int nreads, uint64_t *d_hcov_filled_hamming, uint64_t *d_min_hamming);

//gpu_embed.cu
void gpu_embed_pos(uint64_t **d_ref, Embedding *e, unsigned in_size,
        uint64_t *d_hits, int total_hits, char **d_cembed);
void gpu_compute_hamming(Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits,
        char *d_cembed, char *d_Qembed, unsigned nQembed, uint64_t **d_hamming);
void gpu_embed_reference(Embedding *e, unsigned rlen, int kmer_step,
        int nposv, uint64_t **d_ref, char **h_output);
void gpu_merged_embed_hamming(int gpu_id, uint64_t **d_ref, Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits, char *d_Qembed, unsigned nQembed,
        uint64_t *d_hcov_hamming, uint64_t **d_hamming);
void gpu_all_merged (uint64_t **d_ref, Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits, char *d_Qembed,
        unsigned nQembed, uint64_t **d_hamming);
void gpu_hit_offset(int gpu_id, uint64_t *d_uniq_hits, int nunique_hits, int *nreads_has_hits, uint64_t **d_hit_offset_per_read);
void gpu_get_min_hamming(int gpu_id, int unique_hits, int max_hamming, uint64_t *d_hamming, uint64_t *d_hit_offset_per_read, int nreads, uint64_t **d_min_hamming);

// support routines in gpu_embed.cu not used
void bench_gpu_embed_data_by_str(Embedding *e, vector<const char *> &input, unsigned in_size, vector<string> &output);
void bench_gpu_embed_data_by_pos(char **d_ref, Embedding *e,
        vector<uint32_t> &input, unsigned in_size, vector<string> &output);
void benchmark_gpu_embed(uint64_t **d_ref, Embedding *e, vector<uint32_t> &input, string &Q,
        unsigned in_size, vector<short> &hamming);
//AlignParams *gpu_find_region(int gpu_id, int nreads, int rlen,
//        int nfwd_hits, int *d_nfwd_hits, uint64_t *d_fhits,
//        int nrev_hits, int *d_nrev_hits, uint64_t *d_rhits,
//        size_t ref_size);

