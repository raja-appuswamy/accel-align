#include "header.h"

extern std::mutex *g_gpu_mutex;
extern MemMgr *g_memmgr;

__global__
void device_count_region(int nreads,
        int ntotal_hits, int *nhits_per_read, uint64_t *hits,
        int *best_cov, int *next_cov, int *nregions)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < nreads) {
        int t_bestcov = best_cov[idx];
        int t_nextcov = next_cov[idx];
        int t_nregions = nregions[idx];
        int soff = nhits_per_read[idx];
        int eoff = (idx == (nreads - 1) ? ntotal_hits : nhits_per_read[idx + 1]);

        while (soff < eoff) {
            uint64_t last_hit = hits[soff];

            int j = soff + 1;
            for (; j < eoff && hits[j] == last_hit; j++)
                ;

            int cov = j - soff;
            t_bestcov = cov > t_bestcov ? cov : t_bestcov;
            t_nextcov = cov > t_nextcov ? cov : t_nextcov;
            t_nregions++;

            soff = j;
        }

        best_cov[idx] = t_bestcov;
        next_cov[idx] = t_nextcov;
        nregions[idx] = t_nregions;
    }
}

__global__
void device_find_region(int nreads,
        int ntotal_hits, int *nhits_per_read, uint64_t *hits,
        char strand, int rlen, size_t ref_size,
        int *d_nregions, Region *d_regions)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < nreads) {
        int soff = nhits_per_read[idx];
        int eoff = (idx == (nreads - 1) ? ntotal_hits : nhits_per_read[idx + 1]);
        int region_idx = d_nregions[idx];

        while (soff < eoff) {
            uint64_t last_hit = hits[soff];

            int j = soff + 1;
            for (; j < eoff && hits[j] == last_hit; j++)
                ;

            // XXX: Our region struct is way too big. Have a special gpuregion
            // and pass that back.
            Region *r = d_regions + region_idx;
            region_idx++;
            r->pos = last_hit & 0xFFFFFFFF;
            r->cov = j - soff;
#if DEFAULT_ALIGNER
            r->beg = r->pos > MAX_INDEL ? r->pos - MAX_INDEL : 0;
            r->end = r->pos + rlen + MAX_INDEL < ref_size ?
                r->pos + rlen + MAX_INDEL : ref_size;
#else
            r->beg = r->pos;
            r->end = r->pos + rlen < ref_size ? r->pos + rlen : ref_size;
#endif
            r->str = strand;
            
            soff = j;
        }

        d_nregions[idx] = region_idx;
    }
}

//AlignParams *gpu_find_region(int gpu_id, int nreads, int rlen,
//        int nfwd_hits, int *d_nfwd_hits, uint64_t *d_fhits,
//        int nrev_hits, int *d_nrev_hits, uint64_t *d_rhits,
//        size_t ref_size)
//{
//    dim3 block(1024);
//    dim3 grid((nreads + block.x -1) / block.x);
//    MemMgr &my_mgr = g_memmgr[gpu_id];
//
//    // first count the number of best and next regions in both forward and reverse
//    int *d_nregions, *d_bestcov, *d_nextcov;
//    size_t sz = sizeof(int) * nreads;
//    int *stats = reinterpret_cast<int *>(my_mgr.alloc(sz * 3));
//    cuda(Memset(stats, 0, sz * 3));
//    d_bestcov = stats;
//    d_nextcov = stats + 1;
//    d_nregions = stats + 2;
//
//    device_count_region<<<grid, block>>>(nreads,
//            nfwd_hits, d_nfwd_hits, d_fhits,
//            d_bestcov, d_nextcov, d_nregions);
//    cuda(DeviceSynchronize());
//
//    device_count_region<<<grid, block>>>(nreads,
//            nrev_hits, d_nrev_hits, d_rhits,
//            d_bestcov, d_nextcov, d_nregions);
//    cuda(DeviceSynchronize());
//
//    // now get the number of best and next regions.
//    thrust::device_ptr<int> dptr_nregions(d_nregions);
//    int nregions = thrust::reduce(dptr_nregions, dptr_nregions + nreads);
//    std::cerr << "Found " << nregions << " regions" << endl;
//    Region *d_regions;
//    d_regions = reinterpret_cast<Region *>(my_mgr.alloc(sizeof(Region) *
//                nregions));
//
//    // Do prefix sum on best and // next so that next run knows where to write.
//    // but before that take a copy as we will need later
//    int *h_nregions = new int[nreads];
//    cuda(Memcpy(h_nregions, d_nregions, sizeof(int) * nreads, cudaMemcpyDeviceToHost));
//    thrust::exclusive_scan(dptr_nregions, dptr_nregions + nreads, dptr_nregions);
//    cuda(DeviceSynchronize());
//
//    // now go and get actual regions
//    device_find_region<<<grid, block>>>(nreads,
//            nfwd_hits, d_nfwd_hits, d_fhits,
//            '+', rlen, ref_size,
//            d_nregions, d_regions);
//    cuda(DeviceSynchronize());
//
//    device_find_region<<<grid, block>>>(nreads,
//            nrev_hits, d_nrev_hits, d_rhits,
//            '-', rlen, ref_size,
//            d_nregions, d_regions);
//    cuda(DeviceSynchronize());
//
//    // now allocate memory for regions and copy back result
//    Region *h_regions = new Region[nregions];
//    cuda(Memcpy(h_regions, d_regions,
//                sizeof(Region) * nregions, cudaMemcpyDeviceToHost));
//
//    // copy back best and next cov values
//    int *h_best_cov = new int[nreads];
//    int *h_next_cov = new int[nreads];
//    cuda(Memcpy(h_best_cov, d_bestcov, sizeof(int) * nreads, cudaMemcpyDeviceToHost));
//    cuda(Memcpy(h_next_cov, d_nextcov, sizeof(int) * nreads, cudaMemcpyDeviceToHost));
//
//    my_mgr.free(d_regions);
//    my_mgr.free(stats);
//
//    // NOTE: This free is releasing memory allocated in gpu_lookup
//    my_mgr.free(d_nrev_hits);
//    my_mgr.free(d_nfwd_hits);
//    my_mgr.free(d_fhits);
//    my_mgr.free(d_rhits);
//
//    // unlock gpu mutex. this must be acquired in gpu_lookup. we release it here
//    my_mgr.compact();
//    g_gpu_mutex[gpu_id].unlock();
//
//    // alloc align params and set region pointers appropriately
//    AlignParams *h_ap = new AlignParams[nreads];
//    int ridx = 0;
//    for (int i = 0; i < nreads; i++) {
//        for (int j = 0; j < h_nregions[i]; j++)
//            h_ap[i].best_region = *(h_regions[ridx + j]);
//
//        h_ap[i].best_cov = h_best_cov[i];
//        h_ap[i].next_cov = h_next_cov[i];
//        ridx += h_nregions[i];
//    }
//
//    delete[] h_next_cov;
//    delete[] h_best_cov;
//    delete[] h_nregions;
//    delete[] h_regions;
//
//    return h_ap;
//}
