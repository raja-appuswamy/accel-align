CC=g++
NVCC_PATH=/usr/local/cuda-11.0/bin
NVCC=${NVCC_PATH}/nvcc

INCLUDES=-I/usr/local/cuda-11.0/include/ -I/media/ssd/ngs-data-analysis/code/WFA/
ifneq ($(DEBUG),)
	CFLAGS=-g -fopenmp -Wall -pthread -std=c++14 -O0 -DDBGPRINT
else
	CFLAGS=-g -fopenmp -Wall -pthread -std=c++14 -O3 -mavx2
endif

NVCC_LDFLAGS=-rdc=true -lcudadevrt -lcudart -L${NVCC_PATH}
CC_LDFLAGS=-lz -ltbb -std=c++14
NVCC_FLAGS=-arch sm_75

TARGETS=index accalign-cpu
CPUSRC=reference.cpp accalign.cpp ssw.c ssw_cpp.cpp radix_sort.cpp embedding.cpp ksw2_extz2_sse.c
GPUSRC=memmgr.cpp
IDXSRC=index.cpp embedding.cpp
CUDASRC=gpu_embed.cu gpu_sort.cu gpu_find_region.cu gpu_seed_lookup.cu
CUDAOBJS=$(CUDASRC:.cu=.o)
HEADERS=$(wildcard *.h)

ifneq ($(DEBUG),)
	NVCC_FLAGS+=-g -G -O0
else
	NVCC_FLAGS+=-O3
endif

all:	${TARGETS}

index:	${IDXSRC} ${HEADERS}
	${CC} ${IDXSRC} -o $@ ${CFLAGS} ${CC_LDFLAGS} ${INCLUDES} -L/media/ssd/ngs-data-analysis/code/WFA/build -lwfa

%.o:%.cu
	${NVCC} $^ -c --compiler-options='-DENABLE_GPU ${CFLAGS}' ${NVCC_FLAGS} -I${INCLUDES}

accalign-gpu: ${CUDAOBJS} ${CPUSRC} ${GPUSRC} ${HEADERS}
	${NVCC} ${CPUSRC} ${GPUSRC} ${CUDAOBJS} -o $@ ${NVCC_FLAGS} --compiler-options='-DENABLE_GPU ${CFLAGS}' ${INCLUDES} ${CC_LDFLAGS} ${NVCC_LDFLAGS} -L/media/ssd/ngs-data-analysis/code/WFA/build -lwfa

accalign-cpu: ${CPUSRC} ${HEADERS}
	${CC} ${CPUSRC} -o $@ ${CFLAGS} ${CC_LDFLAGS} ${INCLUDES} -L/media/ssd/ngs-data-analysis/code/WFA/build -lwfa

clean:
	rm ${TARGETS} *.o
