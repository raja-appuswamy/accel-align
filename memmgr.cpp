#include "header.h"

void GPUMemMgr::init(int gpu_id)
{
    cudaSetDevice(gpu_id);
    std::cerr << "Initializing memory on gpu " << gpu_id << std::endl;
    cuda(Malloc((void **)&heap_base, GPU_PREALLOC_SIZE));
    space_left = GPU_PREALLOC_SIZE;
    free_mem[heap_base] = GPU_PREALLOC_SIZE;
}

GPUMemMgr::~GPUMemMgr()
{
    cuda(Free(heap_base));
}

void HostMemMgr::init()
{
    std::cerr << "Initializing host memory" << endl;
    cuda(HostAlloc((void **)&heap_base, HOST_PREALLOC_SIZE, cudaHostAllocPortable));
    space_left = HOST_PREALLOC_SIZE;
    free_mem[heap_base] = HOST_PREALLOC_SIZE;
}

HostMemMgr::~HostMemMgr()
{
    cuda(FreeHost(heap_base));
}

void *MemMgr::alloc(size_t sz)
{
    assert(sz);

    /* round up size to multiple of 256.
     * We do this as otherwise, we can get misalignment error. cudamalloc avoids
     * such errors by a similar rounding
     */
    sz = sz + 256 - sz % 256;

    //cerr << "GPU memmgr allocating " << sz << " out of " << space_left << endl;
    if (sz > space_left)
        return nullptr;

    char *target = nullptr;
    size_t target_sz = 0;
    for (const auto &chunk : free_mem) {
        // if we find exact match, stop
        if (chunk.second == sz) {
            target = reinterpret_cast<char *>(chunk.first);
            target_sz = sz;
            break;
        } else if (chunk.second > sz) {
            if (!target || target_sz > chunk.second) {
                target = reinterpret_cast<char *>(chunk.first);
                target_sz = chunk.second;
            }
        }
    }
    assert(target);
    assert(target_sz >= sz);

    // if target is larger than what we want, break it up and mark it as free
    if (target_sz > sz) {
        char *remainder = target + sz;
        assert(free_mem.find(remainder) == free_mem.end());
        free_mem[remainder] = target_sz - sz;
    }

    // mark target as taken and update space stats
    free_mem.erase(target);
    assert(used_mem.find(target) == used_mem.end());
    used_mem[target] = sz;
    space_left -= sz;

    return target;
}

void MemMgr::compact()
{
    auto prev = free_mem.cend();
    for (auto itr = free_mem.cbegin(); itr != free_mem.cend(); ) {
        if (prev == free_mem.end()) {
            prev = itr;
            ++itr;
            continue;
        }

        assert(prev->first < itr->first);

        if (reinterpret_cast<char *>(prev->first) + prev->second == 
                reinterpret_cast<char *>(itr->first)) {
            free_mem[prev->first] = prev->second + itr->second;
            itr = free_mem.erase(itr);
        } else {
            prev = itr;
            ++itr;
        }
    }

    // after compaction, we should be left with one giant slab
    //assert(free_mem.size() == 1);
}

void MemMgr::free(void *target)
{
    assert (used_mem.find(target) != used_mem.end());
    assert (free_mem.find(target) == free_mem.end());

    // move memory from used to free mem and update stats
    free_mem[target] = used_mem[target];
    space_left += used_mem[target];
    used_mem.erase(target);
}
