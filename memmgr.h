#pragma once

/* Managing GPU allocations.
 * Assumes that the calling thread has already done cudaSetDevice
 */
class MemMgr
{
    public:
        void *alloc(size_t sz);
        void free(void *data);
        void compact();

        void *heap_base;
        size_t space_left;
        std::map<void *,size_t> free_mem, used_mem;
};

class GPUMemMgr:public MemMgr
{
    public:
        void init(int gpu_id);
        ~GPUMemMgr();

    private:
        static const size_t GPU_PREALLOC_SIZE = 9.5 * GB;
};

class HostMemMgr:public MemMgr
{
    public:
        void init();
        ~HostMemMgr();

    private:
        static const size_t HOST_PREALLOC_SIZE = 1 * GB;
};
