/media/ssd/ngs-data-analysis/code/mason2-2.0.9-Linux-x86_64/bin/mason_simulator \
--illumina-read-length 76 \
-ir /media/ssd/ngs-data-analysis/data/hg37.fna \
-n 1000 \
-o ./r1.fastq \
-or ./r2.fastq \
-oa ./aligned.sam

echo "=== pair-end test==="
time /media/ssd/ngs-data-analysis/code/accel-align/accalign-cpu \
-l 32  -t 4 \
-o ./accalign.sam \
/media/ssd/ngs-data-analysis/data/fsva-hg37/hg37.fna \
./r1.fastq ./r2.fastq 

python3 /media/ssd/ngs-data-analysis/code/accalign-benchmark/final-benchmark/match.py \
./aligned.sam ./accalign.sam ./accalign.csv accalign

var=`head -2 accalign.csv | tail -1 | awk -F ',' '{print $3}'`

echo "Exact match percent $var should be close to 95.15"

rm accalign.sam accalign.csv
rm r1.fastq r2.fastq aligned.sam