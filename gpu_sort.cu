#include "header.h"
#include <cub/cub.cuh>

extern int compute_elapsed(struct timeval *start, struct timeval *end);
extern GPUMemMgr *g_memmgr;
extern struct gpu_stats gstats[50];
using namespace cub;

// if hits is passed in, that means the caller wants us to copy out raw hits.
// This will be the case if find region will be done on the CPU.
void gpu_sort(int gpu_id, uint64_t *d_input, int &ninput)
{
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    struct timeval tstart, tend;

    cudaDeviceSynchronize();
    gettimeofday(&tstart, NULL);
    void *d_temp_storage = NULL;
    size_t temp_storage_bytes = 0;
    cub::DeviceRadixSort::SortKeys(d_temp_storage, temp_storage_bytes, d_input, d_input, ninput);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cub::DeviceRadixSort::SortKeys(d_temp_storage, temp_storage_bytes, d_input, d_input, ninput);
    my_mgr.free(d_temp_storage);
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[1].gpu_time += compute_elapsed(&tstart, &tend);
}

struct append_rid
{
    __host__ __device__
        uint64_t operator() (const uint64_t &lhs, const uint64_t &rhs) const
        {
            return ((lhs & 0xFFFFFFFF00000000) | rhs);
        }
};


__global__
void device_set_hcov_hit(KeyValuePair<int, uint64_t> *d_hcov_pair, uint64_t * d_cnt_offset, uint64_t * d_uniq_input,
        int nreads, uint64_t *d_hcov_hit, uint64_t *d_hcov_cnt, int nunique)
{

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx  < nreads) {
        uint64_t index = d_hcov_pair[idx].key + d_cnt_offset[idx]; // the index of hcov hit in unique hit
        assert(index < nunique);
        d_hcov_hit[idx] = d_uniq_input[index];
        d_hcov_cnt[idx] = ((d_uniq_input[index] & 0xFFFFFFFF00000000) | d_hcov_pair[idx].value); //append read id
    }
}

__global__
void device_get_rid(int nhits, uint64_t *d_hits, uint64_t *d_hit_rid)
{

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < nhits) {
        d_hit_rid[idx] = (d_hits[idx] & 0xFFFFFFFF00000000) >> 32;
    }
}

void gpu_countBykey(int gpu_id, int num_items, uint64_t *d_key, uint64_t **d_unique_key, uint64_t **d_count, int &nunique_key){
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    uint64_t *_d_unique_key = reinterpret_cast<uint64_t *>(my_mgr.alloc(num_items * sizeof(uint64_t)));
    assert(_d_unique_key);
    uint64_t *_d_count = reinterpret_cast<uint64_t *>(my_mgr.alloc(num_items * sizeof(uint64_t)));
    assert(_d_count);

    void     *d_temp_storage = NULL;
    size_t   temp_storage_bytes = 0;
    int *d_nunique = reinterpret_cast<int *>(my_mgr.alloc(sizeof(int)));
    assert(d_nunique);
    uint8_t *d_values = reinterpret_cast<uint8_t *>(my_mgr.alloc(num_items * sizeof(uint8_t)));
    assert(d_values);
    cudaMemset(d_values, 1, num_items * sizeof(uint8_t));

    cub::DeviceReduce::ReduceByKey(d_temp_storage, temp_storage_bytes,
                                   d_key, _d_unique_key, d_values, _d_count, d_nunique, cub::Sum(), num_items);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cub::DeviceReduce::ReduceByKey(d_temp_storage, temp_storage_bytes,
                                   d_key, _d_unique_key, d_values, _d_count, d_nunique, cub::Sum(), num_items);
    cudaMemcpy(&nunique_key, d_nunique, sizeof(int), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();

//    thrust::device_ptr<uint64_t> d_key_ptr(d_key);
//    thrust::device_ptr<uint64_t> d_uniq_key_ptr(_d_unique_key);
//    thrust::device_ptr<uint64_t> d_count_ptr(_d_count);
//    thrust::pair<thrust::device_ptr<uint64_t>, thrust::device_ptr<uint64_t>> new_end = thrust::reduce_by_key(d_key_ptr, d_key_ptr + num_items, thrust::make_constant_iterator<uint64_t>(1), d_uniq_key_ptr, d_count_ptr);
//    nunique_key = new_end.first - d_uniq_key_ptr;

    *d_unique_key = _d_unique_key;
    *d_count = _d_count;
    my_mgr.free(d_values);
    my_mgr.free(d_nunique);
    my_mgr.free(d_temp_storage);
}

void gpu_hit_offset_per_read(int gpu_id, int nunique, uint64_t *_d_uniq_input,
        int &nreads_has_hits, uint64_t **d_cnt_offset){
    struct timeval tstart, tend;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    void     *d_temp_storage = NULL;
    size_t   temp_storage_bytes = 0;

    //transform: extract read id
    cout << "extract read id" << endl;
    gettimeofday(&tstart, NULL);
    uint64_t *d_hit_rid = reinterpret_cast<uint64_t *>(my_mgr.alloc(nunique * sizeof(uint64_t)));
    assert(d_hit_rid);
    dim3 block1(128);
    dim3 grid1((nunique + block1.x -1) / block1.x);
    device_get_rid<<<grid1.x, block1>>>(nunique, _d_uniq_input, d_hit_rid);
    cudaDeviceSynchronize();

    //reduce_by_key: get the number of reads which has at least one hit, and count the number of unique hits for each read
    cout << "reduce_by_key: count the number of unique hits for each read" << endl;
    uint64_t *d_nuniq_hit_per_read = NULL, *d_rid = NULL;
    gpu_countBykey(gpu_id, nunique, d_hit_rid, &d_rid, &d_nuniq_hit_per_read, nreads_has_hits);
    my_mgr.free(d_rid);
    gettimeofday(&tend, NULL);
    gstats[33].gpu_time += compute_elapsed(&tstart, &tend);

    //InclusiveSum: get the offset of every reads' hits (d_nuniq_hit_per_read)
    cout << "InclusiveSum: get the offset of every reads' hits (d_nuniq_hit_per_read)" << endl;
    gettimeofday(&tstart, NULL);
    d_temp_storage = NULL;
    uint64_t *_d_cnt_offset = reinterpret_cast<uint64_t *>(my_mgr.alloc((nreads_has_hits + 1) * sizeof(uint64_t)));
    assert(_d_cnt_offset);
    cudaMemset(_d_cnt_offset, 0,  sizeof(uint64_t)); //set the first offset to 0
    cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, d_nuniq_hit_per_read, _d_cnt_offset+1, nreads_has_hits);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, d_nuniq_hit_per_read, _d_cnt_offset+1, nreads_has_hits);
    my_mgr.free(d_temp_storage);
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[34].gpu_time += compute_elapsed(&tstart, &tend);

    my_mgr.free(d_hit_rid);
    my_mgr.free(d_nuniq_hit_per_read);

    *d_cnt_offset = _d_cnt_offset;
}

void gpu_dedup_and_prioritize(int gpu_id, uint64_t *d_input, int ninput, uint64_t **d_uniq_input, uint64_t **d_counts,
        int &nunique, uint64_t **d_hcov_hit, uint64_t **d_hcov_cnt, int nreads, int &nreads_has_hits)
{
    struct timeval tstart, tend;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    void     *d_temp_storage = NULL;
    size_t   temp_storage_bytes = 0;

    //ReduceByKey: get unique hits, number of unique hits, and cnt of each unique hit
    cout << "get cnt of each unique hit && the unique hits"  << endl;
    gettimeofday(&tstart, NULL);
    uint64_t *_d_uniq_input = NULL, *_d_counts = NULL;
    gpu_countBykey(gpu_id, ninput, d_input, &_d_uniq_input, &_d_counts, nunique);
    gettimeofday(&tend, NULL);
    gstats[31].gpu_time += compute_elapsed(&tstart, &tend);

    // get gpu_hit_offset_per_read
    uint64_t *d_cnt_offset = NULL ;
    gpu_hit_offset_per_read(gpu_id, nunique, _d_uniq_input, nreads_has_hits, &d_cnt_offset);

    //ArgMax: get the hcov_cnt, and it's index of the segment
    cout << "ArgMax: get the hcov_cnt, and it's index of the segment" << endl;
    gettimeofday(&tstart, NULL);
    KeyValuePair<int, uint64_t> *_d_hcov_pair = reinterpret_cast<KeyValuePair<int, uint64_t> *>(my_mgr.alloc(nreads_has_hits * (sizeof(KeyValuePair<int, uint64_t>))));
    assert(_d_hcov_pair);
    d_temp_storage = NULL;
    cub::DeviceSegmentedReduce::ArgMax(d_temp_storage, temp_storage_bytes,
            _d_counts, _d_hcov_pair, nreads_has_hits, d_cnt_offset, d_cnt_offset + 1);
    //d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cudaMalloc(&d_temp_storage, temp_storage_bytes);
    cub::DeviceSegmentedReduce::ArgMax(d_temp_storage, temp_storage_bytes,
            _d_counts, _d_hcov_pair, nreads_has_hits, d_cnt_offset, d_cnt_offset + 1);
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[35].gpu_time += compute_elapsed(&tstart, &tend);

    //call kernal get hcov_hit, hcov_cnt
    cout << "call kernal get hcov_hit, hcov_cnt" << endl;
    gettimeofday(&tstart, NULL);
    uint64_t *_d_hcov_hit = reinterpret_cast<uint64_t *>(my_mgr.alloc(nreads_has_hits * sizeof(uint64_t)));
    assert(_d_hcov_hit);
    uint64_t *_d_hcov_cnt = reinterpret_cast<uint64_t *>(my_mgr.alloc(nreads_has_hits * sizeof(uint64_t)));
    assert(_d_hcov_cnt);
    dim3 block(128);
    dim3 grid((nreads_has_hits + block.x -1) / block.x);
    device_set_hcov_hit<<<grid.x, block>>>(_d_hcov_pair, d_cnt_offset, _d_uniq_input, nreads_has_hits, _d_hcov_hit, _d_hcov_cnt, nunique);
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[36].gpu_time += compute_elapsed(&tstart, &tend);

    my_mgr.free(d_cnt_offset);
    my_mgr.free(_d_hcov_pair);
    *d_hcov_hit = _d_hcov_hit;
    *d_hcov_cnt = _d_hcov_cnt;

    //transform: append all unique hit and count with rid
    cout << "transform: append all unique hit and count with rid" << endl;
    gettimeofday(&tstart, NULL);
    thrust::device_ptr<uint64_t> d_uiptr(_d_uniq_input);
    thrust::device_ptr<uint64_t> d_cntptr(_d_counts);
    thrust::transform(d_uiptr, d_uiptr + nunique, d_cntptr, d_cntptr, append_rid());
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[37].gpu_time += compute_elapsed(&tstart, &tend);

    *d_uniq_input = _d_uniq_input;
    *d_counts = _d_counts;
}

__global__
void device_mark_pg(bool *d_read_hcov, uint64_t *d_counts, bool *d_mkptr, int nhits)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < nhits){
        int rid = (d_counts[idx] & 0xFFFFFFFF00000000) >> 32;
        bool is_one = (d_counts[idx] & 0x00000000FFFFFFFF) == 1;
        d_mkptr[idx] = (d_read_hcov[rid] && is_one);
    }
}

__global__
void device_mark_reads_over_one_hit(uint64_t *d_hcov_fcounts, uint64_t *d_hcov_rcounts,
        bool *_d_read_hcov, int nreads)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < nreads){
        bool f_high = (d_hcov_fcounts[idx] & 0x00000000FFFFFFFF) > 1;
        bool r_high = (d_hcov_rcounts[idx] & 0x00000000FFFFFFFF) > 1;
        _d_read_hcov[idx] = f_high || r_high;
    }
}

void gpu_over_one_hit(int gpu_id, uint64_t *d_hcov_fcounts, uint64_t *d_hcov_rcounts, int nreads, bool **d_over_one_hit){
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    bool *_d_over_one_hit = reinterpret_cast<bool *>(my_mgr.alloc(nreads * sizeof(bool)));
    assert(_d_over_one_hit);

    // set read to 1 if fwd or rev's hcov > 1
    dim3 block(128);
    dim3 grid((nreads + block.x -1) / block.x);
    device_mark_reads_over_one_hit<<<grid, block>>>(d_hcov_fcounts, d_hcov_rcounts, _d_over_one_hit, nreads);
    cuda(DeviceSynchronize());

    *d_over_one_hit = _d_over_one_hit;
}

void gpu_remove_hits(int gpu_id, uint64_t *d_uniq_fhits, uint64_t *d_fcounts, int &n_funique, bool *d_over_one_hit){

    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    dim3 block(128);
    dim3 grid_f((n_funique + block.x -1) / block.x);

    // set hit to 1 if only 1 hit while the hcov > 1
    bool *_d_fmk = reinterpret_cast<bool *>(my_mgr.alloc(n_funique * sizeof(bool)));
    assert(_d_fmk);
    device_mark_pg<<<grid_f, block>>>(d_over_one_hit, d_fcounts, _d_fmk, n_funique);
    cuda(DeviceSynchronize());
    thrust::device_ptr<bool> d_fmkptr(_d_fmk);
    //hit
    thrust::device_ptr<uint64_t> d_fhitptr(d_uniq_fhits);
    thrust::remove_if(d_fhitptr, d_fhitptr + n_funique, d_fmkptr, thrust::identity<bool>());
    //n
    n_funique = thrust::count(d_fmkptr, d_fmkptr + n_funique, false);

    my_mgr.free(_d_fmk);
}

__global__
void device_realign_hcov_hits(uint64_t *d_ihcov_ham, uint64_t *d_oall_ham, int nham)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < nham) {
        uint64_t hcov_ham = d_ihcov_ham[idx];
        int Qid = (hcov_ham & QID_MASK) >> QID_SHIFT;
        d_oall_ham[Qid] = hcov_ham;
    }
}

__global__
void device_fill_maxdist(uint64_t *d_max_ham, int max_hamming, int nreads)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < nreads) {
        d_max_ham[idx] = (((uint64_t)idx) << QID_SHIFT | ((uint64_t) max_hamming << HAMMING_SHIFT) );
    }
}

void gpu_fullfill_hamming(int gpu_id, int nreads, int nhcov_hits, int max_hamming, uint64_t *d_hcov_hamming, uint64_t **d_filled_hamming){
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    // we don't have all necessary reads because some reads might not have
    // candidates at all. so we have to expand.
    uint64_t *d_tmp = reinterpret_cast<uint64_t *>(my_mgr.alloc(nreads * sizeof(uint64_t)));
    assert(d_tmp);

    //fill d_tmp with rid (16 bit) + rlen*EMBED_FACTOR (16 bit) + 0 (32 bit)
    // r_id in first 16 bits, then hamming distance 16 bit , then position 32 bit.
    dim3 block(1024);
    dim3 grid1((nreads + block.x -1)/block.x);
    device_fill_maxdist<<<grid1.x, block.x>>>(d_tmp, max_hamming, nreads);
    cuda(DeviceSynchronize());

    dim3 grid((nhcov_hits + block.x -1)/block.x);
    device_realign_hcov_hits<<<grid.x, block.x>>>(d_hcov_hamming, d_tmp, nhcov_hits);
    cuda(DeviceSynchronize());

    *d_filled_hamming = d_tmp;
}

void gpu_combine_hcov_hamming(int gpu_id, uint64_t *d_hcov_fhamming, uint64_t *d_hcov_rhamming, int nreads,
        uint64_t **d_hcov_hamming){

    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    // combine _fhamming into fhamming. this way we avoid allocating another array needlessly.
    thrust::device_ptr<uint64_t> d_fptr(d_hcov_fhamming);
    thrust::device_ptr<uint64_t> d_rptr(d_hcov_rhamming);

    uint64_t *_d_hcov_hamming = reinterpret_cast<uint64_t *>(my_mgr.alloc(nreads * sizeof(uint64_t)));
    assert(_d_hcov_hamming);
    thrust::device_ptr<uint64_t> d_outptr(_d_hcov_hamming);
    thrust::transform(d_fptr, d_fptr + nreads, d_rptr, d_outptr, thrust::minimum<uint64_t>());

//    // free off tmp storage
//    //if (d_fsource != d_hcov_fhamming)
//        my_mgr.free(d_hcov_fhamming);
//    //if (d_rsource != d_hcov_rhamming)
//        my_mgr.free(d_hcov_rhamming);

    *d_hcov_hamming = _d_hcov_hamming;
}

struct get_best_pos
{
    __host__ __device__
    uint64_t operator() (const uint64_t &lhs, const uint64_t &rhs) const{
        assert((lhs & QID_MASK) == (rhs & QID_MASK));
        return (lhs & HAMMING_MASK) <= (rhs & HAMMING_MASK) ? lhs : rhs;
    }
};

void gpu_get_best_pos(int nreads, uint64_t *d_hcov_filled_hamming, uint64_t *d_min_hamming){
    thrust::device_ptr<uint64_t> d_fptr(d_hcov_filled_hamming);
    thrust::device_ptr<uint64_t> d_rptr(d_min_hamming);
    thrust::transform(d_fptr, d_fptr + nreads, d_rptr, d_rptr, get_best_pos());
    cudaDeviceSynchronize();
}
