#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <assert.h>
#include <cuda.h>
#include <sys/time.h>
#include <cuda_runtime_api.h>

#define gpuErrChk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
        if (code != cudaSuccess) 
        {
                fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
                if (abort) exit(code);
        }
}

__global__ void device_lookup(int *array, int SIZE, int *lookups, int *results, int NLOOKUPS)
{
        int i = blockDim.x * blockIdx.x + threadIdx.x;

        if (i < NLOOKUPS)
                results[i] = array[lookups[i]];
}

void gpu_lookup(int *d_array, int SIZE, int *d_lookups, int *d_results, int NUM_LOOKUPS)
{
        dim3 block(1024);
        dim3 grid((NUM_LOOKUPS + block.x -1) / block.x);

        device_lookup<<<grid, block>>>(d_array, SIZE, d_lookups, d_results, NUM_LOOKUPS);
}

int compute_elapsed(struct timeval *start, struct timeval *end)
{
        return ((end->tv_sec * 1000000 + end->tv_usec)
                        - (start->tv_sec * 1000000 + start->tv_usec));
} 

int main() {
        struct timeval start, end;
        int cpu_time, gpu_time;
        int NUM_LOOKUPS = 1024*1024*128;
        int *results = (int *)malloc(NUM_LOOKUPS * sizeof(int));
        srand(time(NULL));

        /* init input */
        int *lookups = (int *)malloc(NUM_LOOKUPS * sizeof(int));
        int SIZE = 1024 * 1024 * 1024;
        int *array = (int *)malloc(SIZE*sizeof(int));
        for (int i=0;i<SIZE;i++){
                array[i] = i;
        }

        for (int i=0;i<NUM_LOOKUPS;i++){
                lookups[i] = rand() % SIZE;
        }

        /* cpu lookup */
        gettimeofday(&start, NULL);

        for (int i=0;i<NUM_LOOKUPS;i++){
                results[i] = array[lookups[i]];
        }
        gettimeofday(&end, NULL);
        cpu_time = compute_elapsed(&start, &end);
        printf("cpu nlkups/sec: %f\n", (float)NUM_LOOKUPS * 1000000 / cpu_time); 
        /* end cpu */

        /* gpu lookup */
        int *d_array, *d_lookups, *d_results;
        gpuErrChk(cudaMalloc((void **)&d_array, SIZE * sizeof(int)));
        gpuErrChk(cudaMalloc((void **)&d_results, NUM_LOOKUPS * sizeof(int)));
        gpuErrChk(cudaMalloc((void **)&d_lookups, NUM_LOOKUPS * sizeof(int)));

        gettimeofday(&start, NULL);
        cudaMemcpy(d_array, array, SIZE * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_lookups, lookups, NUM_LOOKUPS * sizeof(int), cudaMemcpyHostToDevice);
        gettimeofday(&end, NULL);
        printf("Took %d to copy \n", compute_elapsed(&start, &end));

        gettimeofday(&start, NULL);
        gpu_lookup(d_array, SIZE, d_lookups, d_results, NUM_LOOKUPS);
        cudaDeviceSynchronize();
        gettimeofday(&end, NULL);
        gpu_time = compute_elapsed(&start, &end);

        printf("gpu nlkups/sec: %f\n", (float)NUM_LOOKUPS * 1000000 / gpu_time); 
        /* end gpu */

        /* verify results */
        int *gresults = (int *)malloc(NUM_LOOKUPS * sizeof(int));
        cudaMemcpy(gresults, d_results, NUM_LOOKUPS * sizeof(int), cudaMemcpyDeviceToHost);

        for (int i = 0; i < NUM_LOOKUPS; i++) {
                if (results[i] != gresults[i])
                        printf("variation at %d, cval %d, gval %d\n", i, results[i], gresults[i]);

                assert(results[i] == gresults[i]);
        }
        /* end verify */

        free(array);
        cudaFree(d_array);
        cudaFree(d_lookups);
        cudaFree(d_results);

        free(results);
        free(lookups);
}
