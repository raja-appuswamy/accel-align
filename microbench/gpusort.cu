#include <cassert>
#include <iostream>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sys/time.h>
#include "../radix_sort.cpp"

static void 
cuda_assert(const cudaError_t code, const char* const file, const int line, const bool abort)
{
  if (code != cudaSuccess)
    {
      fprintf(stderr,"cuda_assert: %s %s %d\n",cudaGetErrorString(code),file,line);

      if (abort)
        {
          cudaDeviceReset();          
          exit(code);
        }
    }
}

#define cuda(...) { cuda_assert((cuda##__VA_ARGS__), __FILE__, __LINE__, true); }

using namespace std;

int compute_elapsed(struct timeval *start, struct timeval *end)
{
    return ((end->tv_sec * 1000000 + end->tv_usec)
            - (start->tv_sec * 1000000 + start->tv_usec));
} 

 int main() {
     struct timeval start, end;
     unsigned SIZE32 = 440740290;
     unsigned SIZE64 = 440740290;
     unsigned *array = (unsigned *)malloc(SIZE32*sizeof(int));
     srand(time(NULL));
     for (int i=0;i<SIZE32;i++){
       array[i] = rand() % SIZE32;
     }

     uint64_t *larray = (uint64_t*)malloc(SIZE64*sizeof(uint64_t));
     srand(time(NULL));
     for (int i=0;i<SIZE64;i++){
       larray[i] = array[i];
     }

     cout << endl << "Sorting " << SIZE32 << " 32 bit integers and " << SIZE64 << 
         " 64 bit ones" << endl;

     // 32bit gpu test
     unsigned *d_array;
     cuda(Malloc((void **)&d_array, SIZE32 * sizeof(unsigned)));

     gettimeofday(&start, NULL);
     cuda(Memcpy(d_array, array, SIZE32 * sizeof(unsigned),
                 cudaMemcpyHostToDevice));
     thrust::device_ptr<unsigned> d_ptr(d_array);
     cudaDeviceSynchronize();
     gettimeofday(&end, NULL);
     double copy_time = compute_elapsed(&start, &end);

     gettimeofday(&start, NULL);
     thrust::sort(d_ptr, d_ptr + SIZE32);
     cudaDeviceSynchronize();
     gettimeofday(&end, NULL);
     double gpu_time = compute_elapsed(&start, &end);
     cudaFree(d_array);

     cout << " gpu 32 bit copy time " << copy_time / 1000000 << endl <<
        " gpu 32 bit sort time " << gpu_time / 1000000 << endl;

     // 64 bit gpu test
     uint64_t *d_larray;
     cuda(Malloc((void **)&d_larray, SIZE64 * sizeof(uint64_t)));

     gettimeofday(&start, NULL);
     cuda(Memcpy(d_larray, larray, SIZE64 * sizeof(uint64_t),
                 cudaMemcpyHostToDevice));
     gettimeofday(&end, NULL);
     double copy_time64 = compute_elapsed(&start, &end);

     gettimeofday(&start, NULL);
     thrust::device_ptr<uint64_t> d_ptr64(d_larray);
     thrust::sort(d_ptr64, d_ptr64 + SIZE64);
     cudaDeviceSynchronize();
     gettimeofday(&end, NULL);
     double gpu_time64 = compute_elapsed(&start, &end);

     cout << " gpu 64 bit copy time " << copy_time64 / 1000000 << endl <<
        " gpu 64 bit sort time " << gpu_time64 / 1000000 << endl;

     gettimeofday(&start, NULL);
     uint64_t sum = thrust::reduce(d_ptr64, d_ptr64 + SIZE64);
     cudaDeviceSynchronize();
     gettimeofday(&end, NULL);
     cout << "gpu 64 sum " << sum << " time " <<
         (double)compute_elapsed(&start, &end) / 1000000 << endl;

     gettimeofday(&start, NULL);
     sum = 0;
     for (int i = 0; i < SIZE64; i++)
         sum += larray[i];
     gettimeofday(&end, NULL);
     cout << "cpu 64 sum " << sum << " time " <<
         (double)compute_elapsed(&start, &end) / 1000000 << endl;

     gettimeofday(&start, NULL);
     radix_sort(array, array + SIZE32);
     gettimeofday(&end, NULL);
     double radix_cpu_time = compute_elapsed(&start, &end);

     cout << "Radix 32 bit sort time " << radix_cpu_time / 1000000 << endl;

     gettimeofday(&start, NULL);
     radix_sort64(larray, larray + SIZE64);
     gettimeofday(&end, NULL);
     double std_cpu_time = compute_elapsed(&start, &end);

     cout << " radix 64bit sort time " << std_cpu_time / 1000000 << endl;

     // verification
     uint64_t *tmp_larray = new uint64_t[SIZE64];
     cuda(Memcpy(tmp_larray, d_larray, SIZE64 * sizeof(uint64_t),
                 cudaMemcpyDeviceToHost));

     for (int i = 0; i < SIZE64; i++)
         assert(tmp_larray[i] == larray[i]);

     delete[] tmp_larray;
     cudaFree(d_larray);
     free(array);
 }
