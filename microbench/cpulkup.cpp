#include <iostream>
#include <cassert>
#include <cstdlib>
#include <sys/time.h>

using namespace std;

int sz = 500000000;

int main(int argc, char *argv[])
{
        int *a = new int[sz];

        for (int i = 0; i < sz; i++)
                a[i] = i;

        struct timeval stime, etime;

        cout << "init pos vec\n";
        int *pos = new int[sz];
        for (int i = 0; i < sz; i++) {
                pos[i] = rand() % sz; 
        }

        int *res = new int[sz];

#if DO_PREFETCH
        int v3 = 0;
        int v4 = 0;
        cout << "start test\n";
        gettimeofday(&stime, NULL);

        const int GRP = 20;
        int idx[GRP];
        for (int i = 0; i < sz; i+=GRP) {
                for (int &i : idx)
                        i = 0;

                for (int j = 0; j < GRP; j++) {
                        idx[j] = pos[j + i];
                        __builtin_prefetch(&pos[idx[j]], 0, 1);
                }

                for (int j = 0; j < GRP; j++) {
                        idx[j] = pos[idx[j]];
                        __builtin_prefetch(&a[idx[j]], 0, 1);
                }

                for (int j = 0; j < GRP; j++) {
                        int val = a[idx[j]];
                        res[i + j] = val;
                        if (val % 2)
                                v3 += val;
                        else
                                v4 += val;
                }
        }    

        gettimeofday(&etime, NULL);
        cout << "done test\n";

        int diff = (etime.tv_sec - stime.tv_sec) * 1000000;
        diff += etime.tv_usec - stime.tv_usec;
        cout << (double)diff/1000000 << " secs" << endl;

        cout << v3 << " , " <<  v4 << endl;
#else
        int v1 = 0;
        int v2 = 0;
        cout << "start test\n";
        gettimeofday(&stime, NULL);
        for (int i = 0; i < sz; i++) {
                int idx = pos[i];
                idx = pos[idx];
                if (a[idx] % 2)
                        v1 += a[idx];
                else
                        v2 += a[idx];

                res[i] = a[idx];
        }

        gettimeofday(&etime, NULL);
        cout << "done test\n";

        cout << v1 << " , " <<  v2 << endl;
        int diff = (etime.tv_sec - stime.tv_sec) * 1000000;
        diff += etime.tv_usec - stime.tv_usec;
        cout << (double)diff/1000000 << " secs" << endl;


#endif

        delete[] pos;
        delete[] a;
        return 0;
}
