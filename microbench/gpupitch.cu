#include <cassert>
#include <iostream>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sys/time.h>
#include "../radix_sort.cpp"

static void 
cuda_assert(const cudaError_t code, const char* const file, const int line, const bool abort)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"cuda_assert: %s %s %d\n",cudaGetErrorString(code),file,line);

        if (abort)
        {
            cudaDeviceReset();          
            exit(code);
        }
    }
}

#define cuda(...) { cuda_assert((cuda##__VA_ARGS__), __FILE__, __LINE__, true); }

using namespace std;

int compute_elapsed(struct timeval *start, struct timeval *end)
{
    return ((end->tv_sec * 1000000 + end->tv_usec)
            - (start->tv_sec * 1000000 + start->tv_usec));
} 

int main() {
    unsigned SIZE = 16;
    unsigned *array = new unsigned[SIZE];
    for (int i = 0;i < SIZE; i++){
        array[i] = i;
    }

    uint32_t *larray = new uint32_t[2 * SIZE];
    srand(time(NULL));
    for (int i = 0; i < 2 * SIZE; i++){
        //larray[i] = i % 2 ? 0 : i/2;
        larray[i] = 0;
    }

    // copy
    unsigned *d_array;
    cuda(Malloc((void **)&d_array, SIZE * sizeof(unsigned)));
    cuda(Memcpy(d_array, array, SIZE * sizeof(unsigned),
                cudaMemcpyHostToDevice));

    //copy back
    cuda(Memcpy2D(larray, sizeof(uint64_t), d_array, sizeof(unsigned),
                sizeof(unsigned), SIZE, cudaMemcpyDeviceToHost));

    for (int i = 0; i < 2 * SIZE; i+=2) {
        uint64_t tmp = *(uint64_t *)(larray + i);
        cout << array[i/2] << " : " << tmp << " : " << larray[i] << "," <<
            larray[i + 1] << endl;
    }

    cudaFree(d_array);
    delete[] array;
    delete[] larray;
}
