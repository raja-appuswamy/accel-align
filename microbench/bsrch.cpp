 #include <ctime>
 #include <cstdio>
 #include <cstdlib>

void gpsearch(int *array, int nelems, int *key, int *value) {
    struct state {
        int value;
        int low;
    };

    struct state states[16];
    int sz = nelems;
    for (int j = 0; j < 16; j++)
        states[j].value = states[j].low = 0;

    while (sz / 2 > 0) {
        int half = sz / 2;
        for (int j = 0; j < 16; j++) {
            int probe = states[j].low + half;
            __builtin_prefetch(&array[probe], 0, 1);
        }

        for (int j = 0; j < 16; j++) {
            int probe = states[j].low + half;
            int v = array[probe];
            if (v <= key[j])
                states[j].low = probe;
        }

        sz -= half;
    }

    for (int j = 0; j < 16; j++) {
        if (array[states[j].low] == key[j])
            value[j] = 1;
        else 
            value[j] = 0;
    }
}

 int binarySearch(int *array, int number_of_elements, int key) {
         int low = 0, high = number_of_elements-1, mid;
         while(low <= high) {
                 mid = (low + high)/2;
 //           #ifdef DO_PREFETCH
            // low path
 //           __builtin_prefetch (&array[(mid + 1 + high)/2], 0, 1);
            // high path
 //           __builtin_prefetch (&array[(low + mid - 1)/2], 0, 1);
 //           #endif

                 if(array[mid] < key)
                         low = mid + 1; 
                 else if(array[mid] == key)
                         return 1;
                 else if(array[mid] > key)
                         high = mid-1;
         }
         return 0;
 }

 int main() {
     int SIZE = 1024*1024*512;
     int *array =  new int[SIZE];
     for (int i=0;i<SIZE;i++){
         array[i] = i;
     }
     int NUM_LOOKUPS = 1024*1024*100;
     srand(time(NULL));
     int *lookups = new int[NUM_LOOKUPS];
     for (int i=0;i<NUM_LOOKUPS;i++){
         if (i % 2)
             lookups[i] = rand() % SIZE;
         else
             lookups[i] = SIZE;
     }
#ifdef DO_PREFETCH
     int results[16], total = 0;
     for (int i=0;i<NUM_LOOKUPS;i+=16){
         for (int j = 0; j < 16; j++)
             results[j] = 0;

         gpsearch(array, SIZE, &lookups[i], results);
         for (int j = 0; j < 16; j++)
             total += results[j];
     }
     printf("%d\n", total);
#else
     int result = 0;
     for (int i=0;i<NUM_LOOKUPS;i++){
       result += binarySearch(array, SIZE, lookups[i]);
     }
     printf("%d\n", result);
#endif

     delete[] array;
     delete[] lookups;
 }
