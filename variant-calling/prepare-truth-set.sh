# This is to prepare the variants of na12878 from GiaB as true set

######### prepare exome S0293689 ######### 
# get the exome S0293689 in exome_folder
cd $exome_folder
unzip S0293689.zip 
awk 'NR >= 3 && NR <= 195907' S0293689_AllTracks.bed > S0293689_region.bed

######### filter giab variants by bed file #############
# wget files from ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/NA12878_HG001/NISTv2.19/
#tabix -p vcf $giab_folder/NISTIntegratedCalls_14datasets_131103_allcall_UGHapMerge_HetHomVarPASS_VQSRv2.19_2mindatasets_5minYesNoRatio_all_nouncert_excludesimplerep_excludesegdups_excludedecoy_excludeRepSeqSTRs_noCNVs.vcf.gz

#high conf bed
tabix -h -R $giab_folder/union13callableMQonlymerged_addcert_nouncert_excludesimplerep_excludesegdups_excludedecoy_excludeRepSeqSTRs_noCNVs_v2.19_2mindatasets_5minYesNoRatio.bed \
$giab_folder/NISTIntegratedCalls_14datasets_131103_allcall_UGHapMerge_HetHomVarPASS_VQSRv2.19_2mindatasets_5minYesNoRatio_all_nouncert_excludesimplerep_excludesegdups_excludedecoy_excludeRepSeqSTRs_noCNVs.vcf.gz > $out_folder/highconf.vcf

#sort highconf and add "chr" prefix
head -n 140 $out_folder/highconf.vcf > $out_folder/highconf-sorted.vcf
tail +141 $out_folder/highconf.vcf | sort -k1,1V -k2,2n -k3,3n | sed -e 's/^/chr/' >> $out_folder/highconf-sorted.vcf
bgzip -c $out_folder/highconf-sorted.vcf > $out_folder/highconf-sorted.vcf.gz \
&& tabix -p vcf $out_folder/highconf-sorted.vcf.gz

#exome bed
tabix -h -R $exome_folder/S0293689_region.bed \
$out_folder/highconf-sorted.vcf.gz \
| bgzip -c > $out_folder/sureSelect.vcf.gz \
&& tabix -p vcf $out_folder/sureSelect.vcf.gz

#variant normalization
path-to-vcflib/bin/vcfallelicprimitives \
$out_folder/sureSelect.vcf.gz > $out_folder/normalized.vcf

bgzip -c $out_folder/normalized.vcf > $out_folder/normalized.vcf.gz \
&& tabix -p vcf $out_folder/normalized.vcf.gz

#variant_Sepration_Indel_SNV
path-to-vcftools/src/cpp/vcftools \
--gzvcf $out_folder/normalized.vcf.gz \
--remove-indels --recode --recode-INFO-all \
--out $out_folder/truth_snp

path-to-vcftools/src/cpp/vcftools \
--gzvcf $out_folder/normalized.vcf.gz \
--keep-only-indels  --recode --recode-INFO-all \
--out $out_folder/truth_indels

bgzip -c $out_folder/truth_snp.recode.vcf \
> $out_folder/truth_snp.recode.vcf.gz \
&& tabix -p vcf $out_folder/truth_snp.recode.vcf.gz

bgzip -c $out_folder/truth_indels.recode.vcf\
> $out_folder/truth_indels.recode.vcf.gz \
&& tabix -p vcf $out_folder/truth_indels.recode.vcf.gz