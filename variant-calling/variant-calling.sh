input=path-to-input
output_folder=path-to-output
aligner=accalign

# prefetch SRR098401
sratoolkit.2.10.5-ubuntu64/bin/prefetch SRR098401

# get fastq
sratoolkit.2.10.5-ubuntu64/bin/fastq-dump --split-files -O $input SRR098401.sra

# quality control
time cutadapt -j 4 --pair-filter=any \
--quality-cutoff 16,16 --error-rate 0.1 \
--match-read-wildcards \
--adapter PE2_rc=AGATCGGAAGAGCACACGTCTGAAC \
--adapter FlowCell2_rc=TCGTATGCCGTCTTCTGCTTGAAAA \
--adapter PCRPrimer2_rc=AGATCGGAAGAGCGGTTCAGCAGGA \
--times 2 --minimum-length 50 \
-o $input/qualified_1_tmp.fastq -p $input/qualified_2_tmp.fastq \
$input/SRR098401_1.fastq $input/SRR098401_2.fastq

time cutadapt -j 4 --pair-filter=any \
--quality-cutoff 16,16 --error-rate 0.1 \
--match-read-wildcards \
--adapter RE1_rc=AGATCGGAAGAGCGTCGTGTAGGGA \
--adapter FlowCell1_rc=GTGTAGATCTCGGTGGTCGCCGTAT \
--adapter PCRPrimer1_rc=AGATCGGAAGAGCGTCGTGTAGGGA \
--times 2 --minimum-length 50 \
-o $input/qualified_1.fastq -p $input/qualified_2.fastq \
$input/qualified_1_tmp.fastq $input/qualified_2_tmp.fastq 

# align qualified reads 
accel-align/accalign-cpu -l 32  -t 4 -o $output_folder/$aligner.mason.sam \
ref.fna $input/qualified_1.fastq $input/qualified_2.fastq

# convert sam to bam
samtools view -bS $output_folder/$aligner.mason.sam -o $output_folder/$aligner.mason.bam

# Sort_Bam
java -Xmx10g -jar picard.jar SortSam \
VALIDATION_STRINGENCY=SILENT I=$output_folder/$aligner.mason.bam \
O=$output_folder/$aligner.sort.mason.bam SORT_ORDER=coordinate

# Pcr_Duplicates 
time java -Xmx10g -jar picard.jar MarkDuplicates \
VALIDATION_STRINGENCY=SILENT I=$output_folder/$aligner.sort.mason.bam \
O=$output_folder/$aligner.pcr.mason.bam REMOVE_DUPLICATES=true \
M=$output_folder/$aligner.pcr.mason.metrics

# ID_Addition 
time java -Xmx10g \
-jar picard.jar AddOrReplaceReadGroups \
VALIDATION_STRINGENCY=SILENT I=$output_folder/$aligner.pcr.mason.bam \
O=$output_folder/$aligner.rg.mason.bam SO=coordinate \
RGID=$r1 RGLB=$r1 RGPL=illumina \
RGPU=$r1 RGSM=$r1 CREATE_INDEX=true

# Variant calling: GATK 
gatk --java-options "-Xmx10g" HaplotypeCaller -R ref.fna \
-I $output_folder/$aligner.rg.mason.bam -O $output_folder/$aligner.gatk.vcf
