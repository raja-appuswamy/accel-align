# This is to normalize and get the statistic of the variants detected by GATK

mkdir -p $out_folder

bgzip -c $input_folder/$aligner.gatk.vcf > $out_folder/$aligner.gatk.vcf.gz \
&& tabix -p vcf $out_folder/$aligner.gatk.vcf.gz

#high conf bed
tabix -h -R $giab_folder/chr.bed \
$out_folder/$aligner.gatk.vcf.gz \
| bgzip -c > $out_folder/highconf.vcf.gz \
&& tabix -p vcf $out_folder/highconf.vcf.gz

#exome bed
tabix -h -R $exome_folder/S0293689_region.bed \
$out_folder/highconf.vcf.gz \
| bgzip -c > $out_folder/sureSelect.vcf.gz \
&& tabix -p vcf $out_folder/sureSelect.vcf.gz

#variant normalization
path-to-vcflib/bin/vcfallelicprimitives \
$out_folder/sureSelect.vcf.gz \
| bgzip -c > $out_folder/normalized.vcf.gz \
&& tabix -p vcf $out_folder/normalized.vcf.gz

#stats of normalization
path-to-bcftools-1.10.2/bcftools stats \
$out_folder/normalized.vcf.gz > $out_folder/bcftools-stats-normalized.out

#variant_Sepration_Indel_SNV
path-to-vcftools/src/cpp/vcftools \
--gzvcf $out_folder/normalized.vcf.gz \
--remove-indels --recode --recode-INFO-all \
--out $out_folder/$aligner.gatk_snp

path-to-vcftools/src/cpp/vcftools \
--gzvcf $out_folder/normalized.vcf.gz \
--keep-only-indels  --recode --recode-INFO-all \
--out $out_folder/$aligner.gatk_indels

bgzip -c $out_folder/$aligner.gatk_snp.recode.vcf \
> $out_folder/$aligner.gatk_snp.recode.vcf.gz \
&& tabix -p vcf $out_folder/$aligner.gatk_snp.recode.vcf.gz

bgzip -c $out_folder/$aligner.gatk_indels.recode.vcf\
> $out_folder/$aligner.gatk_indels.recode.vcf.gz \
&& tabix -p vcf $out_folder/$aligner.gatk_indels.recode.vcf.gz

######### compare vcf for F-score#############
path-to-vcftools/src/perl/vcf-compare \
$giab_out_folder/truth_snp.recode.vcf.gz $out_folder/$aligner.gatk_snp.recode.vcf.gz \
> $out_folder/vcf-compare-snp.out

path-to-vcftools/src/perl/vcf-compare \
$giab_out_folder/truth_indels.recode.vcf.gz $out_folder/$aligner.gatk_indels.recode.vcf.gz \
> $out_folder/vcf-compare-indel.out