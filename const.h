#pragma once

#define NBITS_PER_ALPHA 2
#define MOD ((1UL<<29)-1)
#define BDIM 1024
#define MAX_OCC 450
#define MOD ((1UL<<29)-1)
#define MAX_THREADS 1
#define CPU_BUFFER 10000
#define GPU_BUFFER 15000
#define MAX_INDEL 32
#define NGPU_STATS 2
#define GPU_LOOKUP_STATS 1
#define GPU_SORT_STATS 1
#define KB 1024UL
#define MB (KB * KB)
#define GB (MB * KB)
#define PIGEONHOLE_FILTER 0
//#define DEFAULT_ALIGNER 1

#define QID_MASK 0xFFFF000000000000
#define QID_SHIFT 48
#define HAMMING_MASK 0x0000FFFF00000000
#define HAMMING_SHIFT 32
#define CPOS_MASK 0x00000000FFFFFFFF
#define BITS_PER_NT 3
#define NT_PER_WORD 21
#define NHITS_PER_BLOCK_EMBED 256

// CORE IDs
#define NCORES 12
#define NHTHREADS_PER_CORE 2
#define ROOT_CORE_ID 22
#define GPU_MAP_BLOCK_CORE_ID 0
#define BASE_GPU_ALIGN_CORE_ID 2
#define NGPU_ALIGN_THREADS 1
#define BASE_CPU_ALIGN_CORE_ID (BASE_GPU_ALIGN_CORE_ID + NGPU_ALIGN_THREADS * NHTHREADS_PER_CORE)

//modes
#define SHORT_READ_MODE 0
#define LONG_READ_MODE 1

//Embedding related
#define EMBED_PAD 4
#define NUM_STR 1 //r
#define NUM_CHAR 5 // num of chars in strings
#define MAX_ELEN 1500 //the maximum length of random string
#define RBITS_PER_STRING (MAX_ELEN * NUM_CHAR)
#define TOTAL_RBITS (RBITS_PER_STRING * NUM_STR)
#define BITPOS(STR_ID, OFFSET, CHAR_ID) (STR_ID * RBITS_PER_STRING +\
        OFFSET * NUM_CHAR + CHAR_ID)
#define EMBED_FACTOR 2


// mapq
#define MIS_PENALTY -1

#define MAX_LEN 512