#include <stdint.h>

void radix_sort(unsigned *begin, unsigned *end)
{
    unsigned *begin1 = new unsigned[end - begin];
    unsigned *end1 = begin1 + (end - begin);
    unsigned *temp;
    unsigned count[4][0x100] = {0};
    for (unsigned *p = begin; p != end; p++) {
        count[0][(*p) & 0xFF]++;
        count[1][(*p >> 8) & 0xFF]++;
        count[2][(*p >> 16) & 0xFF]++;
        count[3][(*p >> 24) & 0xFF]++;
    }

    for(unsigned shift = 0; shift < 32; shift += 8) {
        unsigned *bucket[0x100], *q = begin1;
        int radix = shift / 8;
        for(unsigned i = 0; i < 0x100; q += count[radix][i++])
            bucket[i]=q;

        for(unsigned *p = begin; p != end; p++)
            *bucket[(*p>>shift)&0xFF]++ = *p;

        temp = begin;
        begin = begin1;
        begin1 = temp;

        temp = end;
        end = end1;
        end1 = temp;
    }

    delete[] begin1;
}

void radix_sort64(uint64_t *begin, uint64_t *end)
{
    uint64_t *begin1=new uint64_t [end-begin], *end1=begin1+(end-begin), *temp;
    for(unsigned shift=0; shift<64; shift+=8){
        unsigned count[0x100]={};
        for(uint64_t *p=begin; p!=end; p++)
            count[(*p>>shift)&0xFF]++;

        uint64_t *bucket[0x100], *q=begin1;
        for(unsigned i=0; i<0x100; q+=count[i++])
            bucket[i]=q;

        for(uint64_t *p=begin; p!=end; p++)
            *bucket[(*p>>shift)&0xFF]++=*p;

        temp=begin; begin=begin1; begin1=temp;
        temp=end; end=end1; end1=temp;
    }
    delete[] begin1;
}
