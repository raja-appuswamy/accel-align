#pragma once
#include "header.h"

using namespace std;

class Embedding
{
    public:
        ~Embedding();
        Embedding();
        Embedding(const char *fname);
        int embedstr(const char ** oridata, unsigned rlen, int threshold, int id,
                int strid, char *embeddedQ);
        void flush(const char *fname);
        void embeddata(vector<Region> &candidate_regions, const char **input,
                unsigned ninput, unsigned rlen, int &best_threshold, int
                &next_threshold, bool max_rnd,
                int &best_idx, int &next_idx);

        //unsigned char **hash_eb;
        std::bitset<TOTAL_RBITS> hash_eb;
        unsigned char **d_hasheb; // pointer to hasheb on gpu
        float embed_time;
        int efactor;

    private:

        int cgk2_embed(const char ** oridata, unsigned rlen, int threshold, int id,
                int strid, char *embeddedQ);
        int cgk_embed(const char ** oridata, unsigned rlen, int threshold, int id,
                int strid, char *embeddedQ);

};
