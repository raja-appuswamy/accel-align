#!/bin/bash
LEFT=$1
RIGHT=$2
cat $LEFT | grep '^chr' | cut -f4,5  | sed 's/\t/ /' | awk '$2 != 0 {print $0}' | cut -d' ' -f1 > /tmp/left.out
cat $RIGHT | grep '^chr' | cut -f4,5  | sed 's/\t/ /' | awk '$2 != 0 {print $0}' | cut -d' ' -f1 > /tmp/right.out
diff -q /tmp/left.out /tmp/right.out
#rm /tmp/left.out /tmp/right.out
