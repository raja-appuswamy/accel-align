import sys

if len(sys.argv) < 4:
    print("python diff_coords.py <mason.align.sam> <bwa.sorted.sam>"
            " <acc.sorted.sam>")
    exit(0)

mason_file = sys.argv[1]
bwa_file = sys.argv[2]
acc_file = sys.argv[3]
#assert "bwa" in left

mfile = open(mason_file, "r")
bfile = open(bwa_file, "r")
afile = open(acc_file, "r")

mline = mfile.readline()
while not mline.startswith("simulated."):
    mline = mfile.readline()

bline = bfile.readline()
while not bline.startswith("simulated."):
    bline = bfile.readline()

aline = afile.readline()
while not aline.startswith("simulated."):
    aline = afile.readline()

nmatch_bwa = 0
ndiff_total_bwa = 0
ndiff_bwa_zmapq = 0
ndiff_bwa_nzmapq = 0
nmatch_acc = 0
ndiff_total_acc = 0
ndiff_acc_zmapq = 0
ndiff_zmapq_in_xa = ndiff_zmapq_not_in_xa = 0
ndiff_acc_nzmapq = 0
ndiff_nzmapq_in_xa = ndiff_nzmapq_not_in_xa = 0
ntotal_reads = 0
nunaligned_bwa = 0
nunaligned_acc = 0
while mline:
    bfields = bline.split("\t")
    afields = aline.split("\t")
    mfields = mline.split("\t")
    bstrand = int(bfields[1])
    astrand = int(afields[1])

    if (mfields[0] != bfields[0]):
        print (mfields[0] + "," + bfields[0])
        assert 0
        
    if (mfields[0] != afields[0]):
        print (mfields[0] + "," + afields[0])
        assert 0

    bwa_AS_field = ""
    if len(bfields) == 16:
        bwa_AS_field = bfields[15]

    if (bstrand == 4):
        nunaligned_bwa += 1
    if (astrand == 4):
        print "============mason============="
        print mline.rstrip()
        print "------------bwa---------------"
        print bline.rstrip()
        print "XXXXXXXXXXXXaccXXXXXXXXXXXXXXX"
        print aline.rstrip()
        print "=============================="
        nunaligned_acc += 1

    bcoord = 0
    bmapq = 150
    mcoord = int(mfields[3])
    if (bstrand != 4):
        bcoord = int(bfields[3])
        bmapq = int(bfields[4])
        if ((bcoord > mcoord and bcoord - mcoord > 3) or (bcoord < mcoord and mcoord
                        - bcoord > 3)):
            ndiff_total_bwa += 1
            if (bmapq != 0):
                ndiff_bwa_nzmapq += 1
            else:
                ndiff_bwa_zmapq += 1
        else:
            nmatch_bwa += 1

    if (astrand != 4):
        acoord = int(afields[3])
        mcoord = int(mfields[3])
        bcoord = int(bfields[3])

        # do we differ from mason truth
        if ((acoord > mcoord and acoord - mcoord > 3) or (acoord < mcoord and mcoord
                        - acoord > 3)):
            ndiff_total_acc += 1
            if (bmapq != 0):
                ndiff_acc_nzmapq += 1
#                print "============mason============="
#                print mline.rstrip()
#                print "------------bwa---------------"
#                print bline.rstrip()
#                print "------------acc---------------"
#                print aline.rstrip()
#                print "=============================="

                if str(acoord) not in bwa_AS_field:
                    ndiff_nzmapq_in_xa += 1
                else:
                    ndiff_nzmapq_not_in_xa += 1
            else:
                ndiff_acc_zmapq += 1
                if str(acoord) not in bwa_AS_field:
                    ndiff_zmapq_in_xa += 1
                else:
                    ndiff_zmapq_not_in_xa += 1
        else:
            nmatch_acc += 1

    mline = mfile.readline()
    bline = bfile.readline()
    aline = afile.readline()
    ntotal_reads += 1

assert((nmatch_bwa + ndiff_bwa_zmapq + ndiff_bwa_nzmapq + nunaligned_bwa) == ntotal_reads)
assert((nmatch_acc + ndiff_acc_zmapq + ndiff_acc_nzmapq +  nunaligned_acc) == ntotal_reads)

print "bwa unaligned=" + str(nunaligned_bwa) + ",match=" + str(nmatch_bwa) +\
          ",mismatch=" + str(ndiff_total_bwa) +\
          ",total=" + str(ndiff_total_bwa + nunaligned_bwa + nmatch_bwa)
print "acc unaligned=" + str(nunaligned_acc) + ",match=" + str(nmatch_acc) +\
          ",mismatch = " + str(ndiff_total_acc) +\
          ",total=" + str(ndiff_total_acc + nunaligned_acc + nmatch_acc)
print "bwa ndiff_zmapq=" + str(ndiff_bwa_zmapq) +\
    ",ndiff_nzmapq=" + str(ndiff_bwa_nzmapq)
print "acc ndiff_zmapq=" + str(ndiff_acc_zmapq) +\
    ",ndiff_nzmapq=" + str(ndiff_acc_nzmapq)
print "acc ndiff_zmapq_in_xa=" + str(ndiff_zmapq_in_xa) +\
    ",ndiff_zmapq_not_in_as=" + str(ndiff_zmapq_not_in_xa)
print "acc ndiff_nzmapq_in_xa=" + str(ndiff_nzmapq_in_xa) +\
    ",ndiff_nzmapq_not_in_as=" + str(ndiff_nzmapq_not_in_xa)

