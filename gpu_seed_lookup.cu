#include "header.h"
#include <cub/cub.cuh>

#define CGK2_EMBED 1

extern GPUMemMgr *g_memmgr;
extern struct gpu_stats gstats[50];
extern unsigned mod;
extern uint64_t mask;
extern int kmer_step, kmer_len;


void cpu_seed_lookup(uint32_t *keyv, size_t *d_hash, int total_nseeds,
                        uint32_t *start_pos, uint32_t *end_pos, uint32_t *npos)
{
    for (int idx = 0; idx < total_nseeds; idx++) {
        size_t hash = d_hash[idx];
        start_pos[idx] = keyv[hash];
        end_pos[idx] = keyv[hash + 1];
        npos[idx] = end_pos[idx] - start_pos[idx];
    }
}

__global__
void device_seed_lookup(uint32_t *keyv, size_t *d_hash, int total_nseeds,
        uint32_t *start_pos, uint32_t *end_pos, uint32_t *npos)
{

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx  < total_nseeds) {
        size_t hash = d_hash[idx];
        start_pos[idx] = keyv[hash];
        end_pos[idx] = keyv[hash + 1];
        npos[idx] = end_pos[idx] - start_pos[idx];
    }
}

__global__
void device_seed_lookup_unrolled(uint32_t *keyv, size_t *d_hash, int total_nseeds,
        uint32_t *start_pos, uint32_t *end_pos, uint32_t *npos)
{

    int idx = blockDim.x * blockIdx.x * 2 + threadIdx.x;

    if (idx +  blockDim.x < total_nseeds) {
        size_t hash1 = d_hash[idx];
        size_t hash2 = d_hash[idx + blockDim.x];

        start_pos[idx] = keyv[hash1];
        start_pos[idx + blockDim.x] = keyv[hash2];

        end_pos[idx] = keyv[hash1 + 1];
        end_pos[idx + blockDim.x] = keyv[hash2 + 1];

        npos[idx] = end_pos[idx] - start_pos[idx];
        npos[idx + blockDim.x] = end_pos[idx + blockDim.x] -start_pos[idx + blockDim.x];

    }

    if (idx <  total_nseeds && idx +  blockDim.x >=  total_nseeds) {
        size_t hash1 = d_hash[idx];
        start_pos[idx] = keyv[hash1];
        end_pos[idx] = keyv[hash1 + 1];
        npos[idx] = end_pos[idx] - start_pos[idx];
    }
}

__global__
void device_sum_hits(int nreads,
        uint32_t *start_pos, uint32_t *end_pos,
        unsigned nseeds_per_read, unsigned nseeds_per_strand,
        int *nfwd_hits, int *nrev_hits)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // TODO: BAD BAD access pattern to start/end index. In CPU case it
    // makes sense to have data grouped liked this. we need to change
    // this for gpu case to avoid non-coalesced reads

    if (idx < nreads) {
        nfwd_hits[idx] = 0;
        nrev_hits[idx] = 0;

        for (int i = 0; i < nseeds_per_strand; i++) {
            uint32_t count = end_pos[idx * nseeds_per_read + i] - start_pos[idx *
                nseeds_per_read + i];

            nfwd_hits[idx] += count;
        }

        for (int i = nseeds_per_strand; i < nseeds_per_read; i++) {
            uint32_t count = end_pos[idx * nseeds_per_read + i] -
                start_pos[idx * nseeds_per_read + i];

            nrev_hits[idx] += count;
        }
    }
}

//XXX: fhits , rhits are unsigned, start/end pos at uint32. Inconsistencies
__global__
void device_find_candidates(int nreads, int *nfwd_hit_ps, int *nrev_hit_ps,
        int nseeds_per_strand, int nseeds_per_read, uint32_t *d_startpos,
        uint32_t *d_endpos, uint32_t *posv, uint64_t *fhits,
        uint64_t *rhits)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // TODO: BAD BAD access pattern to start/end index. In CPU case it
    // makes sense to have data grouped liked this. we need to change
    // this for gpu case to avoid non-coalesced reads

    if (idx < nreads) {
        int fhit_idx = nfwd_hit_ps[idx];

        // get candidates for forward match
        for (int i = 0; i < nseeds_per_strand; i++) {
            uint32_t soff = d_startpos[idx * nseeds_per_read + i];
            uint32_t eoff = d_endpos[idx * nseeds_per_read + i];

            // XXX: Warp divergence possible here. Need to see effect
            // XXX: Tons of misaligned reads and writes here
            for (uint32_t k = soff; k < eoff; k++, fhit_idx++) {
                fhits[fhit_idx] = posv[k] - i;
                fhits[fhit_idx] |= ((uint64_t)idx << 32);
            }
        }

        int rhit_idx = nrev_hit_ps[idx];

        // get candidates for reverse match
       for (int i = nseeds_per_strand; i < nseeds_per_read; i++) {
           uint32_t soff = d_startpos[idx * nseeds_per_read + i];
           uint32_t eoff = d_endpos[idx * nseeds_per_read + i];

           // XXX: Warp divergence possible here. Need to see effect
           // XXX: Tons of misaligned reads and writes here
           for (uint32_t k = soff; k < eoff; k++, rhit_idx++) {
               rhits[rhit_idx] = posv[k] - (i % nseeds_per_strand);
               rhits[rhit_idx] |= ((uint64_t)idx << 32);
           }
       }
    }
}

__global__
void opt_device_find_candidates(int nseeds_per_strand,
        unsigned rlen, int kmer_len, int kmer_step,
        uint32_t *d_startpos, uint32_t *d_endpos, uint32_t *d_pos_offsets,
        uint32_t *posv, uint64_t *hits, int total_hits)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    /* When we get here, the output hits contains offsets into start_pos or
     * end_pos that we should use to get the corresponding posv value. For
     * example if orig npos = [1,2,1,3,1,2], startpos=[10,100,1000,101,110,200],
     * endpos=[11,102,1001,104,111,202], then total_hits = 10. hits will be 
     * hits=[0,1,1,2,3,3,3,4,5,5], d_pos_offsets = [0,1,3,4,7,8]
     * Observe that hits actually contains the seedid. Assume 2 seeds per read.
     * Each hit gets a thread. So for thread 1,
     *          seeid = hits[idx] = 1.
     *          k = d_startpos[1] + 1 - 1 will be 100. my_read = 0.
     * For thread 2, 
     *          seedid = hits[2] = 1
     *          k = d_startpos[1] + 2 - 1 = 101. my_read = 0.
     * thread 3
     *          seedid = hits[3] = 2
     *          k = d_startpos[2] + 3 - d_pos_offsets[2] = 1000 + 3 - 3 = 1000
     *          my_read = 3 / 2 = 1
     */
    /* in pigeonhole, there are kmer_step seeds in each window and there are
     * rlen / kmer_window windows in total. For ex, assuming step = 4,
     * kmer_len=100, we will have kmers at 0,1,2,3 then 104,105,106,107, then
     * 208,209,210,211, and so on.
     */
    int kmer_window = kmer_len + kmer_step - 1;
    int nwindows = rlen / kmer_window;

    if (idx < total_hits) {
        int seed_id = hits[idx];
        int window_id = seed_id % nwindows;
        uint32_t window_base = window_id * kmer_window;
        int step_id = seed_id / nwindows % kmer_step;
        uint32_t k = d_startpos[seed_id] + idx - d_pos_offsets[seed_id];
        hits[idx] = posv[k] > (window_base + step_id) ? (posv[k] - window_base - step_id) : 0;
        int my_read = seed_id / nseeds_per_strand;
        hits[idx] |= ((uint64_t)my_read << 32);
    }
}

__global__ void reduceNeighbored (uint32_t *g_idata, uint32_t *g_odata, int n)
{
    // set thread ID
    int tid = threadIdx.x;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    // convert global data pointer to the local pointer of this block
    uint32_t *idata = g_idata + blockIdx.x * blockDim.x;

    // boundary check
    if (idx >= n) return;

    // in-place reduction in global memory
    for (int stride = 1; stride < blockDim.x; stride *= 2)
    {
        // convert tid into local array index
        int index = 2 * stride * tid;

        if (index < blockDim.x)
        {
            idata[index] += idata[index + stride];
        }

        // synchronize within threadblock
        __syncthreads();
    }

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = idata[0];
}

static void opt_gpu_find_candidates(
        GPUMemMgr &my_mgr,
        int nreads, unsigned rlen, int nseeds_per_strand,
        uint32_t *d_startpos, uint32_t *d_endpos, uint32_t *d_npos,
        uint32_t *posv, uint64_t **d_hits, int &total_hits)
    
{
    int nseeds_total = nseeds_per_strand * nreads;
    struct timeval tstart, tend;

    using namespace cub;
    void     *d_temp_storage = NULL;
    size_t   temp_storage_bytes = 0;

    // do ex-scan over npos to find offset for each seed in the output
    std::cerr << "find_candidates: setting up offset\n";
    gettimeofday(&tstart, NULL);
    size_t offsz = nseeds_total * sizeof(uint32_t);
    uint32_t *d_pos_offsets = reinterpret_cast<uint32_t *>(my_mgr.alloc(offsz));
    DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, d_npos, d_pos_offsets, nseeds_total);
    d_temp_storage = reinterpret_cast<uint32_t *>(my_mgr.alloc(temp_storage_bytes));
    DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, d_npos, d_pos_offsets, nseeds_total);
    my_mgr.free(d_temp_storage);
    gettimeofday(&tend, NULL);
    gstats[6].gpu_time += compute_elapsed(&tstart, &tend);

    // find total number of hits
    std::cerr << "find total number of hits \n";
    gettimeofday(&tstart, NULL);
    int *d_nhits = reinterpret_cast<int *>(my_mgr.alloc(sizeof(int)));
    d_temp_storage = NULL;
    DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_npos, d_nhits, nseeds_total);
    d_temp_storage = reinterpret_cast<uint32_t *>(my_mgr.alloc(temp_storage_bytes));
    DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_npos, d_nhits, nseeds_total);
    int nhits;
    cudaMemcpy(&nhits, d_nhits, sizeof(int), cudaMemcpyDeviceToHost);
    my_mgr.free(d_temp_storage);
    my_mgr.free(d_nhits);
    gettimeofday(&tend, NULL);
    gstats[7].gpu_time += compute_elapsed(&tstart, &tend);

    gettimeofday(&tstart, NULL);
    total_hits = nhits;
    //std::cerr << "find_candidates: found " << nhits << " hits after reduction.";

     //allocate gpu space for hits
    size_t out_sz = nhits * sizeof(uint64_t);
    uint64_t *_d_hits = reinterpret_cast<uint64_t *>(my_mgr.alloc(out_sz));
    assert(_d_hits);
    cudaMemset(_d_hits, 0, out_sz);

    // now scatter counts to corresponding output positions
    std::cerr << "find_candidates: scattering output\n";
    thrust::device_ptr<uint64_t> d_hitptr(_d_hits);
    thrust::device_ptr<uint32_t> d_nposptr(d_npos);
    thrust::device_ptr<uint32_t> d_offptr(d_pos_offsets);
    thrust::counting_iterator<int> first(0);
    thrust::scatter_if(first, first+nseeds_total,
            d_offptr, d_nposptr, d_hitptr);
    gettimeofday(&tend, NULL);
    gstats[8].gpu_time += compute_elapsed(&tstart, &tend);

    // now fill in the holes with scan over indices
    std::cerr << "fill in the holes with scan over indices\n";
    gettimeofday(&tstart, NULL);
    d_temp_storage = NULL;
    DeviceScan::InclusiveScan(d_temp_storage, temp_storage_bytes, _d_hits, _d_hits, Max(), nhits);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    DeviceScan::InclusiveScan(d_temp_storage, temp_storage_bytes, _d_hits, _d_hits, Max(), nhits);
    my_mgr.free(d_temp_storage);
    gettimeofday(&tend, NULL);
    gstats[9].gpu_time += compute_elapsed(&tstart, &tend);

    // go get hits
    gettimeofday(&tstart, NULL);
    std::cerr << "find_candidates: running kernel on " << nhits << " hits\n";
    dim3 block(128);
    dim3 grid((nhits + block.x -1) / block.x);
    opt_device_find_candidates<<<grid, block>>>(nseeds_per_strand,
            rlen, kmer_len, kmer_step,
            d_startpos, d_endpos, d_pos_offsets,
            posv, _d_hits, nhits);
    cudaDeviceSynchronize();
    my_mgr.free(d_pos_offsets);
    gettimeofday(&tend, NULL);
    gstats[10].gpu_time += compute_elapsed(&tstart, &tend);

    *d_hits = _d_hits;
}

__attribute__((unused)) static void gpu_find_candidates(
        GPUMemMgr &my_mgr,
        int nreads, int nseeds_per_strand, int nseeds_per_read,
        int *d_nfwdhits, int *d_nrevhits,
        uint32_t *d_startpos, uint32_t *d_endpos,
        uint32_t *posv,
        uint64_t **d_fhits, int total_fhits,
        uint64_t **d_rhits, unsigned total_rhits)
{
    // do the prefix sum for forward and reverse hits so that we know where each
    // hits for each seed are supposed to start in final output
    thrust::device_ptr<int> d_fptr(d_nfwdhits);
    thrust::device_ptr<int> d_rptr(d_nrevhits);
    thrust::exclusive_scan(d_fptr, d_fptr + nreads, d_fptr);
    cuda(DeviceSynchronize());
    thrust::exclusive_scan(d_rptr, d_rptr + nreads, d_rptr);
    cuda(DeviceSynchronize());

    //allocate gpu space for fhits and rhits
    size_t fsz = total_fhits * sizeof(uint64_t);
    size_t rsz = total_rhits * sizeof(uint64_t);
    *d_fhits = reinterpret_cast<uint64_t *>(my_mgr.alloc(fsz));
    assert(*d_fhits);
    *d_rhits = reinterpret_cast<uint64_t *>(my_mgr.alloc(rsz));
    assert(*d_rhits);
    //cuda(Malloc((void **)d_fhits, fsz));
    //cuda(Malloc((void **)d_rhits, rsz));

    // go get hits
    dim3 block(128);
    dim3 grid((nreads + block.x -1) / block.x);
    device_find_candidates<<<grid, block>>>(nreads, d_nfwdhits, d_nrevhits,
            nseeds_per_strand, nseeds_per_read, d_startpos, d_endpos, posv,
            *d_fhits, *d_rhits);

    cuda(DeviceSynchronize());
}

// XXX: Ugly. function takes too many params. ideally we want to pass back all
// gpu-related state and break this function into multiple others
void gpu_lookup(AccAlign *f, int gpu_id,
        size_t *d_fseed_hash, size_t *d_rseed_hash, int nseeds,
        unsigned nreads, unsigned rlen,
        int &total_fhits, int &total_rhits,
        uint64_t **d_fhits, uint64_t **d_rhits){

    struct timeval tstart, tend;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    gettimeofday(&tstart, NULL);

    // alloc device space for start, end pos, diff
    uint32_t *d_startpos, *d_endpos, *d_npos;
    size_t sz = nseeds * sizeof(size_t);
    //std::cerr << "Allocating " << sz * 2 / MB << " MB for lookup on GPU" << std::endl;
    d_startpos = reinterpret_cast<uint32_t *>(my_mgr.alloc(sz));
    d_endpos = reinterpret_cast<uint32_t *>(my_mgr.alloc(sz));
    d_npos = reinterpret_cast<uint32_t *>(my_mgr.alloc(sz));

    std::cerr << "Looking up " << nseeds << " seeds from " << nreads << " reads" << std::endl;

    // perform the lookup
    //uint32_t startpos[nseeds], endpos[nseeds], npos[nseeds];
    //gettimeofday(&tstart, NULL);
    //cpu_seed_lookup(f->keyv, h_seed_hash,
    //        nseeds, startpos, endpos, npos);
    //gettimeofday(&tend, NULL);
    //gstats[2].gpu_time += compute_elapsed(&tstart, &tend);

    dim3 block(128);
    dim3 grid((nseeds/2 + block.x -1) / block.x);
    int nseeds_per_strand = nseeds / (2 * nreads);
    int rbase_off = nseeds_per_strand * nreads;

    //gettimeofday(&tstart, NULL);
    //device_seed_lookup<<<grid.x, block>>>(f->d_keyv[gpu_id], d_seed_hash,
    //        nseeds, d_startpos, d_endpos, d_npos);
    //cuda(DeviceSynchronize());
    //gettimeofday(&tend, NULL);
    //gstats[3].gpu_time += compute_elapsed(&tstart, &tend);

    gettimeofday(&tstart, NULL);
    device_seed_lookup_unrolled<<<(grid.x+1)/2, block>>>(f->d_keyv[gpu_id], d_fseed_hash,
            nseeds/2, d_startpos, d_endpos, d_npos);
    device_seed_lookup_unrolled<<<(grid.x+1)/2, block>>>(f->d_keyv[gpu_id], d_rseed_hash,
            nseeds/2, d_startpos+rbase_off, d_endpos+rbase_off, d_npos+rbase_off);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[4].gpu_time += compute_elapsed(&tstart, &tend);
    //std::cerr << "device lookup took " << (float)compute_elapsed(&tstart, &tend) /
    //    1000000 << std::endl;

#if 0
    /* now compute fwd, rev hits. we want forward and reverse hits. so per read,
     * we want two summary statistics. we use the npos values from earlier and
     * reduce using thrust. we make a sequence with one key per strand per read.
     * Then we do the reduction. So we get summary stats per strand then.
     */
    gettimeofday(&tstart, NULL);

    // generate sequence
    thrust::device_vector<int> d_index(total_nseeds);
    thrust::transform(thrust::make_counting_iterator(0),
                        thrust::make_counting_iterator(total_nseeds),
                        thrust::make_constant_iterator(nseeds_per_strand),
                        d_index.begin(),
                        thrust::divides<int>() );

    // allocate memory for results and build device pointers
    int *d_allhits =
        reinterpret_cast<int *>(my_mgr.alloc(nreads * 2 * sizeof(int)));
    int *d_allkeys =
        reinterpret_cast<int *>(my_mgr.alloc(nreads * 2 * sizeof(int)));
    thrust::device_ptr<uint32_t> d_npos_ptr(d_npos);
    thrust::device_ptr<int> d_allhits_ptr(d_allhits);
    thrust::device_ptr<int> d_allkeys_ptr(d_allkeys);
    thrust::reduce_by_key(d_index.begin(), d_index.end(), d_npos_ptr,
            d_allkeys_ptr, d_allhits_ptr);

    /* XXX; copy from single reduction to separate fwd, rev
     * Easy optimizaiton target for later
     */
    sz = nreads * sizeof(int);
    *d_nfwdhits = reinterpret_cast<int *>(my_mgr.alloc(sz));
    *d_nrevhits = reinterpret_cast<int *>(my_mgr.alloc(sz));
    cuda(Memcpy2D(*d_nfwdhits, sizeof(int), d_allhits, 2 * sizeof(int),
            sizeof(int), nreads, cudaMemcpyDeviceToDevice));
    cuda(Memcpy2D(*d_nrevhits, sizeof(int), d_allhits + 1, 2 * sizeof(int),
            sizeof(int), nreads, cudaMemcpyDeviceToDevice));
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);

    std::cerr << "sum hits took " << (float)compute_elapsed(&tstart, &tend) /
        1000000 << std::endl;;

    // do the reduction
    gettimeofday(&tstart, NULL);
    thrust::device_ptr<int> d_fptr(*d_nfwdhits);
    thrust::device_ptr<int> d_rptr(*d_nrevhits);
    *total_fhits = thrust::reduce(d_fptr, d_fptr + nreads);
    cuda(DeviceSynchronize());
    *total_rhits = thrust::reduce(d_rptr, d_rptr + nreads);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    std::cerr << "reduce took " << (float)compute_elapsed(&tstart, &tend) /
        1000000 << std::endl;

    // now gather the candidates
    std::cerr << "Collecting candidates locs on gpu. totalF/R hits: " <<
        *total_fhits << "/" << *total_rhits << std::endl;
    gettimeofday(&tstart, NULL);
    gpu_find_candidates(my_mgr,
            nreads, nseeds_per_strand, nseeds_per_read,
            *d_nfwdhits, *d_nrevhits,
            d_startpos, d_endpos,
            f->d_posv[gpu_id],
            d_fhits, *total_fhits,
            d_rhits, *total_rhits);
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    std::cerr << "find candidates took " << (float)compute_elapsed(&tstart, &tend) /
        1000000 << std::endl;
#endif

    gettimeofday(&tstart, NULL);
    std::cerr << "Find candidates on forward\n";
    opt_gpu_find_candidates(my_mgr, nreads, rlen, nseeds_per_strand,
            d_startpos, d_endpos,
            d_npos, f->d_posv[gpu_id], d_fhits, total_fhits);
    gettimeofday(&tend, NULL);
    gstats[5].gpu_time += compute_elapsed(&tstart, &tend);

    gettimeofday(&tstart, NULL);
    std::cerr << "Find candidates on reverse\n";
    opt_gpu_find_candidates(my_mgr, nreads, rlen, nseeds_per_strand, d_startpos + rbase_off,
            d_endpos + rbase_off, d_npos + rbase_off, f->d_posv[gpu_id],
            d_rhits, total_rhits);
    gettimeofday(&tend, NULL);
    gstats[11].gpu_time += compute_elapsed(&tstart, &tend);
    //std::cerr << "find candidates took " << (float)compute_elapsed(&tstart, &tend) /
    //    1000000 << std::endl;

    // Free mem.
    my_mgr.free(d_startpos);
    my_mgr.free(d_endpos);
    my_mgr.free(d_npos);
    //my_mgr.free(d_allkeys);
    //my_mgr.free(d_allhits);
    my_mgr.free(d_fseed_hash);
    my_mgr.free(d_rseed_hash);
}

__global__
void device_seed_hash(int nseeds, int nwindows, unsigned rlen, char *d_input, size_t *d_seed_hash,
        uint64_t mask, int kmer_step, int kmer_len, int kmer_window){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx  < nseeds) {
        int i = idx / nwindows / kmer_step;  //which read
        int j = idx % nwindows * kmer_window;  //window # in the read
        int step = idx / nwindows % kmer_step; //step

        uint64_t h = 0;
        for (int k = j + step; k < j + kmer_len + step; k++)
            h = (h << 2) + d_input[i * rlen + k];

        d_seed_hash[idx] = (h & mask) % MOD;
    }
}

__global__
void device_embed(int num_str, int num_char, int embed_pad,
                            unsigned char *d_hasheb,
                            unsigned ninput, unsigned nin_chars, char *d_input,
                            unsigned noutput, unsigned nout_chars, char *d_output, unsigned rlen)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < ninput) {
        int ibase_offset = idx * nin_chars;

        for (int i = 0; i < num_str; i++) {
            unsigned char *thash_eb = d_hasheb + i * num_char * nout_chars;

            #ifdef CGK2_EMBED
            int j = 0;
            for (unsigned k = 0; k < rlen; k++) {
                uint8_t s = d_input[k + ibase_offset];
                char bit = thash_eb[j * num_char + s];
                if (!bit) {
                    d_output[i * ninput * nout_chars + idx * nout_chars + j] = s;
                    j++;
                } else {
                    d_output[i * ninput * nout_chars + idx * nout_chars + j] = s;
                    d_output[i * ninput * nout_chars + idx * nout_chars + j + 1] = s;
                    j += 2;
                }
            }
            //append the rest with EMBED_PAD
            //because the embedded candidate may be longer than j and need to count nmismatch with embedded read
            for(; j < nout_chars; j++){
                d_output[i * ninput * nout_chars + idx * nout_chars + j] = embed_pad;
            }
            #else
            unsigned in_pos = 0;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < nin_chars ? d_input[in_pos + ibase_offset] : EMBED_PAD;
                d_output[i * ninput * nout_chars + idx * nout_chars + j] = s;
                char hash_bit = thash_eb[j * num_char + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);
            }
            #endif
        }
    }
}

void gpu_seedhash_and_embedread(int gpu_id, char *d_input, int nreads, unsigned rlen, int nseeds, int nwindows, int kmer_window,
        size_t **d_seed_hash, Embedding *e, char **d_Qembed)
{
    struct timeval tstart, tend;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    // alloc space for seeds'hash
    size_t *_d_seed_hash = reinterpret_cast<size_t *>(my_mgr.alloc(nseeds * sizeof(size_t)));
    assert(_d_seed_hash);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 block(128);
    dim3 grid((nseeds + block.x -1) / block.x);
    device_seed_hash<<<grid.x, block>>>(nseeds, nwindows, rlen, d_input, _d_seed_hash, mask, kmer_step, kmer_len, kmer_window);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[20].gpu_time += compute_elapsed(&tstart, &tend);

    *d_seed_hash = _d_seed_hash;

    // alloc space for embeded reads output
    unsigned noutput = nreads * NUM_STR;
    unsigned nout_chars = rlen * EMBED_FACTOR;
    unsigned nout_bytes = noutput * nout_chars;
    char *_d_Qembed = reinterpret_cast<char *>(my_mgr.alloc(nout_bytes));
    assert(_d_Qembed);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 grid2((nreads + block.x -1) / block.x);
    device_embed<<<grid2.x, block>>>(NUM_STR, NUM_CHAR, EMBED_PAD, e->d_hasheb[gpu_id],
            nreads, rlen, d_input, noutput, nout_chars, _d_Qembed, rlen);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[21].gpu_time += compute_elapsed(&tstart, &tend);

    *d_Qembed = _d_Qembed;
}

__global__
void device_parse(char *d_reads, uint8_t *d_code, char *d_rcsymbol, unsigned nreads, int rlen, char *d_fwd, char *d_rev, char *d_rev_str){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int base = idx * rlen;

    if (idx < nreads) {
        for (int i = 0; i < rlen; i++){
            uint8_t c = *(d_code + d_reads[i + base]);
            d_fwd[base + i] = c;
            d_rev[base + rlen - 1 - i] = c == 4 ? c : 3 - c;
            d_rev_str[base + rlen - 1 - i] = d_rcsymbol[c];
        }
    }
}

void gpu_parse(int gpu_id, vector<const char *> &reads, uint8_t *code, char *rcsymbol, int rlen,
        char **d_fParsed, char **d_rParsed, char **d_rParsed_str){

    struct timeval tstart, tend;
    unsigned nreads = reads.size();
    unsigned nin_bytes = nreads * rlen;

    // formatting reads
    gettimeofday(&tstart, NULL);
    char *h_input = new char[nin_bytes];
    for (unsigned i = 0; i < nreads; i++)
        memcpy(h_input + i * rlen, reads[i], rlen);
    gettimeofday(&tend, NULL);
    gstats[19].gpu_time += compute_elapsed(&tstart, &tend);

    // copy reads to device
    gettimeofday(&tstart, NULL);
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    char *d_input = reinterpret_cast<char *>(my_mgr.alloc(nin_bytes));
    assert(d_input);
    cuda(Memcpy(d_input, h_input, nin_bytes, cudaMemcpyHostToDevice));
    gettimeofday(&tend, NULL);
    gstats[20].op_pcie_time += compute_elapsed(&tstart, &tend);

    // copy code to device
    uint8_t *d_code = reinterpret_cast<uint8_t *>(my_mgr.alloc(256 * sizeof(uint8_t)));
    assert(d_code);
    cuda(Memcpy(d_code, code, 256 * sizeof(uint8_t), cudaMemcpyHostToDevice));
    gettimeofday(&tend, NULL);
    gstats[20].op_pcie_time += compute_elapsed(&tstart, &tend);
    //copy rcsymbol to device
    char *d_rcsymbol = reinterpret_cast<char *>(my_mgr.alloc(6));
    assert(d_rcsymbol);
    cuda(Memcpy(d_rcsymbol, rcsymbol, 6, cudaMemcpyHostToDevice));

    // alloc space for output
    char *d_fwd = reinterpret_cast<char *>(my_mgr.alloc(nin_bytes));
    assert(d_fwd);
    char *d_rev = reinterpret_cast<char *>(my_mgr.alloc(nin_bytes));
    assert(d_rev);
    char *d_rev_str = reinterpret_cast<char *>(my_mgr.alloc(nin_bytes));
    assert(d_rev_str);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 block(1024);
    dim3 grid((nreads + block.x -1) / block.x);
    device_parse<<<grid.x, block>>>(d_input, d_code, d_rcsymbol, nreads, rlen, d_fwd, d_rev, d_rev_str);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[20].gpu_time += compute_elapsed(&tstart, &tend);

    *d_fParsed = d_fwd;
    *d_rParsed = d_rev;
    *d_rParsed_str = d_rev_str;

    my_mgr.free(d_input);
    my_mgr.free(d_code);
    my_mgr.free(d_rcsymbol);
}
