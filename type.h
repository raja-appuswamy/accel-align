#pragma once

#include "const.h"

struct Region{
    uint32_t beg;
    uint32_t end;
    uint32_t pos;
    uint16_t cov;
    uint16_t embed_dist;
    char str;
    bool is_exact;
    bool is_aligned;
    int score;
    bool operator()(Region &X, Region &Y) {
        if (X.embed_dist == Y.embed_dist)
            return X.cov > Y.cov;
        else
            return X.embed_dist < Y.embed_dist;
    }
};

struct Read{
    char name[MAX_LEN], qua[MAX_LEN], seq[MAX_LEN],  fwd[MAX_LEN], rev[MAX_LEN], rev_str[MAX_LEN], cigar[MAX_LEN];
    int tid, as;
    uint32_t pos;
    char strand;
    short mapq;
    Region best_region;

    friend gzFile& operator>>(gzFile &in, Read &r);
};

struct gpu_stats {
    long long op_pcie_time, gpu_time;
};

class Reference {
    public:
        void load_index(const char *F);
        Reference(const char *F);

        std::string ref;
        uint64_t *bit_ref;
        std::vector<std::string> name;
        std::vector<uint32_t> offset, hash_keys;
        uint32_t *keyv, *posv;
        uint32_t nposv, nkeyv;

        // Holding data in GPU memory
        uint32_t **d_keyv, **d_posv;
        uint64_t **d_ref;

        ~Reference();
};

typedef std::tuple<Read *, int> ReadCnt;