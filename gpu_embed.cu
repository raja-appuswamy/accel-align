#include "header.h"
#include <cub/cub.cuh>

#define CGK2_EMBED 1

extern GPUMemMgr *g_memmgr;
extern struct gpu_stats gstats[50];
using namespace cub;

struct get_read_id
{
    __host__ __device__
    uint64_t operator()(const uint64_t &r) const
    {
        return (r & 0xFFFFFFFF00000000) >>32;
    }
};

void gpu_hit_offset(int gpu_id, uint64_t *d_uniq_hits, int nunique_hits, int *nreads, uint64_t **d_hit_offset_per_read){
    struct timeval tstart, tend;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    void     *d_temp_storage = NULL;
    size_t   temp_storage_bytes = 0;

    //transform: extract read id
    gettimeofday(&tstart, NULL);
    uint64_t *d_hit_rid = reinterpret_cast<uint64_t *>(my_mgr.alloc(nunique_hits * sizeof(uint64_t)));
    assert(d_hit_rid);
    thrust::device_ptr<uint64_t> d_uiptr(d_uniq_hits);
    thrust::device_ptr<uint64_t> d_hit_rid_ptr(d_hit_rid);
    thrust::transform(d_uiptr, d_uiptr + nunique_hits, d_hit_rid_ptr, get_read_id());

    //Unique: get the number of reads which has at least one hit
    // this is to save the corner case: some read may not have hit in fwd or rev
    gettimeofday(&tstart, NULL);
    int *d_nreads_has_hits = reinterpret_cast<int *>(my_mgr.alloc(sizeof(int)));
    assert(d_nreads_has_hits);
    uint64_t *d_out = reinterpret_cast<uint64_t *>(my_mgr.alloc(nunique_hits * sizeof(uint64_t)));
    assert(d_out);
    cub::DeviceSelect::Unique(d_temp_storage, temp_storage_bytes, d_hit_rid, d_out, d_nreads_has_hits, nunique_hits);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cub::DeviceSelect::Unique(d_temp_storage, temp_storage_bytes, d_hit_rid, d_out, d_nreads_has_hits, nunique_hits);
    int nreads_has_hits;
    cudaMemcpy(&nreads_has_hits, d_nreads_has_hits, sizeof(int), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    my_mgr.free(d_out);
    my_mgr.free(d_temp_storage);
    my_mgr.free(d_nreads_has_hits);
    gettimeofday(&tend, NULL);
    gstats[32].gpu_time += compute_elapsed(&tstart, &tend);

    *nreads = nreads_has_hits;

    //reduce_by_key: count the number of unique hits for each read
    uint64_t *d_nuniq_hit_per_read = reinterpret_cast<uint64_t *>(my_mgr.alloc(nreads_has_hits * sizeof(uint64_t)));
    assert(d_nuniq_hit_per_read);
    thrust::device_ptr<uint64_t> d_nuniq_hit_per_read_ptr(d_nuniq_hit_per_read);
    thrust::reduce_by_key(d_hit_rid_ptr, d_hit_rid_ptr + nunique_hits, thrust::constant_iterator<uint64_t>(1),
                          d_hit_rid_ptr, d_nuniq_hit_per_read_ptr);
    gettimeofday(&tend, NULL);
    gstats[33].gpu_time += compute_elapsed(&tstart, &tend);

    //InclusiveSum: get the offset of every reads' hits (d_nuniq_hit_per_read)
    gettimeofday(&tstart, NULL);
    d_temp_storage = NULL;
    uint64_t *d_cnt_offset = reinterpret_cast<uint64_t *>(my_mgr.alloc((nreads_has_hits + 1) * sizeof(uint64_t)));
    assert(d_cnt_offset);
    uint64_t zero = 0;
    cudaMemcpy(d_cnt_offset, &zero, sizeof(uint64_t), cudaMemcpyHostToDevice);
    cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, d_nuniq_hit_per_read, d_cnt_offset+1, nreads_has_hits);
    d_temp_storage = reinterpret_cast<uint64_t *>(my_mgr.alloc(temp_storage_bytes));
    cub::DeviceScan::InclusiveSum(d_temp_storage, temp_storage_bytes, d_nuniq_hit_per_read, d_cnt_offset+1, nreads_has_hits);
    my_mgr.free(d_temp_storage);
    cudaDeviceSynchronize();
    gettimeofday(&tend, NULL);
    gstats[34].gpu_time += compute_elapsed(&tstart, &tend);

    *d_hit_offset_per_read = d_cnt_offset;
    my_mgr.free(d_uniq_hits);
    my_mgr.free(d_hit_rid);
    my_mgr.free(d_nuniq_hit_per_read);
}

__global__
void device_get_minhamming(int nreads, int max_hamming, int unique_hits, uint64_t *d_hamming_sorted, uint64_t *d_hit_offset_per_read,
        uint64_t *_d_min_hamming){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < nreads) {
        int min_index = d_hit_offset_per_read[idx];
        _d_min_hamming[idx] = d_hamming_sorted[min_index];

        if ( (min_index + 1 < unique_hits) && (min_index + 1 < d_hit_offset_per_read[idx+1]) ){
            _d_min_hamming[nreads + idx] = d_hamming_sorted[min_index+1];
        } else {
            //if not have the second best, get the rid and append with maxhamming + 1
            _d_min_hamming[nreads + idx] = (d_hamming_sorted[min_index] & 0xFFFF000000000000) | ((uint64_t) (max_hamming + 1) << 32);
        }
        assert((_d_min_hamming[idx]& 0xFFFF000000000000) == (_d_min_hamming[nreads + idx]& 0xFFFF000000000000));
        assert((_d_min_hamming[idx]& 0x0000FFFF00000000) <= (_d_min_hamming[nreads + idx]& 0x0000FFFF00000000));
    }
}

void gpu_get_min_hamming(int gpu_id, int unique_hits, int max_hamming, uint64_t *d_hamming, uint64_t *d_hit_offset_per_read, int nreads, uint64_t **d_min_hamming){
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];

    //TODO: try segmented sort
    gpu_sort(gpu_id, d_hamming, unique_hits);

    uint64_t *_d_min_hamming = reinterpret_cast<uint64_t *>(my_mgr.alloc(2 * nreads * sizeof(uint64_t)));
    assert(_d_min_hamming);
    dim3 block(128);
    dim3 grid((nreads + block.x -1) / block.x);
    device_get_minhamming<<<grid, block>>>(nreads, max_hamming, unique_hits, d_hamming, d_hit_offset_per_read, _d_min_hamming);
    cudaDeviceSynchronize();

    *d_min_hamming = _d_min_hamming;

    my_mgr.free(d_hamming);
    my_mgr.free(d_hit_offset_per_read);
}

__global__
void device_compute_hamming(int num_str, int num_char,
        int nhits, uint64_t *d_hits,
        unsigned nembed_chr, char *d_cembed,
        char *d_qembed, unsigned nreads, 
        uint64_t *d_hamming)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if (idx < nhits) {
        uint64_t hit_val = d_hits[idx];
        uint32_t q_id = hit_val >> 32;
        uint32_t pos = hit_val & 0xffffffff;
        short hamming = nembed_chr;
        for (unsigned i = 0; i < num_str; i++) {
            int lhamming = 0;
            uint32_t rbase_off = i * nreads * nembed_chr + q_id;
            uint32_t cbase_off = i * nhits * nembed_chr + blockDim.x * blockIdx.x *
                nembed_chr + threadIdx.x;
            for (unsigned j = 0; j < nembed_chr; j++) {
                char cchr = d_cembed[cbase_off + j * blockDim.x];
                char rchr = d_qembed[rbase_off + j * nreads];
                lhamming = lhamming + (cchr == rchr ? 0 : 1);
            }

            hamming = lhamming < hamming ? lhamming : hamming;
        }

        // q_id in first 16 bits, then hamming distance, then position.
        d_hamming[idx] = (((uint64_t)q_id) << QID_SHIFT |
             ((uint64_t) hamming << HAMMING_SHIFT) | pos);
    }
}

__global__
void opt_device_gather_refs_sm(uint64_t *d_hits, int nhits, int nref_words,
        int ninput, int rlen,
        uint64_t *d_refs, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int tid = threadIdx.x;

    // load input into shared memory
    extern __shared__ uint64_t sm_ref_buf[];
    uint64_t *sm_hits = sm_ref_buf;
    uint64_t *sm_refs = sm_hits + 1;

    if (idx < ninput) {
        uint32_t hit_off = idx / rlen;

        if (tid == 0) {
            sm_hits[0] = d_hits[hit_off] & 0xFFFFFFFF;
        }
    }
    __syncthreads();

    uint32_t base_word = sm_hits[0] / NT_PER_WORD;
    if (idx < ninput) {
        if (tid < nref_words) {
            sm_refs[tid] = d_refs[base_word + tid];
        }
    }
    __syncthreads();

    if (idx < ninput) {
        int pos_off = idx % rlen;
        uint32_t pos = sm_hits[0] + pos_off;
        int cur_word = pos / NT_PER_WORD;
        uint64_t ref_val = sm_refs[cur_word - base_word];

        // now set the output
        int shift_off = NT_PER_WORD - (pos % NT_PER_WORD) - 1;
        // 3 should be replaced by EMBED_FACTOR?
        uint64_t bit_mask = 0x7ULL << (shift_off * 3);
        d_output[idx] = (ref_val & bit_mask) >> (shift_off * 3);
    }
}

__global__
void device_uncompressed_gather_refs(uint64_t *d_hits, int nhits,
        int rlen, char *d_refs, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int tid = threadIdx.x;
    extern __shared__ uint32_t sm_ref_pos[];
    
    // load the hits for this thread block
    assert(blockDim.x >= rlen);
    if (idx < nhits) {
        sm_ref_pos[tid] = d_hits[idx] & 0xFFFFFFFF;
    }
    __syncthreads();

    // now loop over hits one at a time and fetch the read
    uint32_t base_hit = blockDim.x * blockIdx.x;
    size_t obase_offset = base_hit * rlen;
    for (int i = 0; i < blockDim.x; i++) {
        if (base_hit + i < nhits) {
            uint32_t ref_pos = sm_ref_pos[i];
            size_t out_offset = obase_offset + i * rlen;
            if (tid < rlen) {
                d_output[out_offset + tid] = d_refs[ref_pos + tid];
            }
        }
    }
}

__global__
void opt_device_gather_refs(uint64_t *d_hits, int nhits, int ninput, int rlen,
        uint64_t *d_refs, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < ninput) {
        // lookup the reference value
        uint32_t hit_off = idx / rlen;
        uint32_t pos = d_hits[hit_off] & 0xFFFFFFFF;
        int pos_off = idx % rlen;
        pos += pos_off;
        unsigned word = pos / NT_PER_WORD;
        uint64_t ref_val = d_refs[word];

        // now set the output
        int off = NT_PER_WORD - (pos % NT_PER_WORD) - 1;
        uint64_t bit_mask = 0x7ULL << (off * 3);
        d_output[idx] = (ref_val & bit_mask) >> (off * 3);
    }
}

__global__
void device_embed_unaligned(int num_str, int num_char,
        unsigned char *d_hasheb,
        unsigned ninput, unsigned nin_chars, char *d_input,
        unsigned noutput, unsigned nout_chars, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < ninput) {
        int ibase_offset = idx * nin_chars;

        for (int i = 0; i < num_str; i++) {
            unsigned in_pos = 0;
            unsigned char *thash_eb = d_hasheb + i * num_char * nout_chars;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < nin_chars ? d_input[in_pos + ibase_offset] : EMBED_PAD;
                //d_output[i * ninput * nout_chars + j * ninput + idx] = s;
                d_output[i * ninput * nout_chars + idx * nout_chars + j] = s;
                char hash_bit = thash_eb[j * num_char + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);
            }
        }
    }
}

__global__
void device_all_merged(int num_str, int num_char,
        unsigned char *d_hasheb, int nentries_hasheb,
        uint64_t *d_hits, unsigned ninput, unsigned nin_chars, char *d_refs,
        unsigned noutput, unsigned nout_chars,
        char *d_qembed, unsigned nreads, uint64_t *d_hamming)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // load hasheb into shared memory
    extern __shared__ uint32_t sm_data[];
    uint32_t *sm_hasheb = sm_data;
    int nhash_blocks = nentries_hasheb / blockDim.x + 1;
    int tid = threadIdx.x;
    for (int i = 0; i < nhash_blocks; i++) {
        if (tid + i * blockDim.x < nentries_hasheb)
            sm_hasheb[tid + i * blockDim.x] = d_hasheb[tid + i * blockDim.x];
    }
    __syncthreads();

    // load the hits corresponding to this threadblock. Assumes that we have one
    // thread per hit.
    uint32_t *sm_qids = &sm_hasheb[nentries_hasheb];
    uint32_t *sm_pos = &sm_qids[blockDim.x];
    if (idx < ninput) {
        uint64_t hit_val = d_hits[idx];
        sm_qids[tid] = hit_val >> 32;
        sm_pos[tid] = hit_val & 0xFFFFFFFF;
        //sm_hits[tid] = d_hits[idx];
    }
    __syncthreads();

    // now loop over hits one at a time and fetch the read. Prerequisite is
    // that the threadblock must have atleast as many threads as the length of
    // the read so that we can collaboratively read the input.
    uint32_t *sm_input = &sm_pos[blockDim.x];
    uint32_t base_hit = blockIdx.x * blockDim.x;
    for (int i = 0; i < blockDim.x; i++) {
        if (base_hit + i < ninput) {
            uint32_t ref_pos = sm_pos[i];
            uint32_t obase_offset = i * nin_chars;
            int nin_blocks = nin_chars / blockDim.x + 1;
            for (int j = 0; j < nin_blocks; j++) {
                if (tid + j * blockDim.x < nin_chars)
                    sm_input[obase_offset + tid + j * blockDim.x] =
                        d_refs[ref_pos + tid + j * blockDim.x];
            }
        }
    }
   __syncthreads();

   if (idx >= ninput)
       return;

   uint32_t ibase_offset = tid * nin_chars;
   uint32_t q_id = sm_qids[tid];
   uint32_t pos = sm_pos[tid];
   short hamming = nout_chars;

   for (int i = 0; i < num_str; i++) {
       uint32_t in_pos = 0;
       uint32_t *thash_eb = sm_hasheb + i * num_char * nout_chars;
       uint32_t rbase_off = i * nreads * nout_chars + q_id * nout_chars;
       int lhamming = 0;

       // we divide hits into groups of blockDim.x. each output will also
       // have blockDim.x hits grouped in one unit.
       //uint32_t obase_offset = i * ninput * nout_chars + 
       //    blockIdx.x * blockDim.x * nout_chars + threadIdx.x;
       for (int j = 0; j < nout_chars; j++) {
           uint8_t s = in_pos < nin_chars ? ((uint8_t)sm_input[in_pos +
               ibase_offset]) : EMBED_PAD;
           //d_output[obase_offset + j * ninput_per_block] = s;
           char hash_bit = (char)thash_eb[j * num_char + s];
           in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);

           // now, also get reference and check
           char rchr = d_qembed[rbase_off + j];
           lhamming = lhamming + (s == rchr ? 0 : 1);
       }

       hamming = lhamming < hamming ? lhamming : hamming;
   }

   // q_id in first 16 bits, then hamming distance, then position.
   d_hamming[idx] = (((uint64_t)q_id) << QID_SHIFT |
           ((uint64_t) hamming << HAMMING_SHIFT) | pos);
}

__global__
void device_merged_embed_wthreshold(unsigned char *d_hasheb, int nbytes_hasheb,
                                    uint64_t *d_hits, unsigned nhits, unsigned rlen, char *d_input,
                                    unsigned noutput, unsigned nout_chars,
                                    char *d_qembed, unsigned nreads, uint64_t *d_hcov_hamming,
                                    uint64_t *d_hamming)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // load hasheb into shared memory
    extern __shared__ unsigned char sm_buf[];
    unsigned char *sm_hasheb = sm_buf;
    int nhash_blocks = nbytes_hasheb / blockDim.x + 1;
    int tid = threadIdx.x;
    for (int i = 0; i < nhash_blocks; i++) {
        if (tid + i * blockDim.x < nbytes_hasheb)
            sm_hasheb[tid + i * blockDim.x] = d_hasheb[tid + i * blockDim.x];
    }
    __syncthreads();

    // load the input corresponding to this thread block. Each thread block has
    // to read blockIdx.x * rlen (or rlen) charcters of input. This will
    // amount to blockDim.x inputs starting from from blockDIm.x * blockIdx.x.
    // have each thread cooperatively load blockDim.x * rlen characters. Each
    // thread first loads first blockDim.x chars, then next set and so on. So
    // there are rlen repetitions.
    unsigned char *sm_input = sm_buf + nbytes_hasheb;
    uint32_t input_offset = blockDim.x * blockIdx.x;
    uint32_t byte_offset = input_offset * rlen;
//    if (idx < nhits) {
//        for (int i = 0; i < rlen; i++) {
//            sm_input[i * blockDim.x + tid] = d_input[byte_offset + i * blockDim.x + tid];
//            //sm_input[i * ninput_per_block + tid] = d_input[idx * rlen + i];
//        }
//    }

    for (int i = 0; i < rlen; i++) {
        if (byte_offset + i * blockDim.x + tid < nhits * rlen) {
            sm_input[i * blockDim.x + tid] = d_input[byte_offset + i * blockDim.x + tid];
        }
    }

    __syncthreads();

    if (idx < nhits) {
        //int ibase_offset = idx;
        uint32_t ibase_offset = tid * rlen;
        uint64_t hit_val = d_hits[idx];
        uint32_t q_id = hit_val >> 32;
        uint32_t pos = hit_val & 0xffffffff;
        short hamming = nout_chars;
        uint64_t hcov_val = d_hcov_hamming[q_id];
        int hcov_qid = (hcov_val & QID_MASK) >> QID_SHIFT;
	    assert(hcov_qid == q_id);
        int ham_threshold = (hcov_val & HAMMING_MASK) >> HAMMING_SHIFT;
//        uint32_t hcov_pos = hcov_val & 0xffffffff;
        assert(ham_threshold <= nout_chars);

//        if (hcov_pos == pos){
//            hamming = ham_threshold;
//            goto end;
//        }

        for (int i = 0; i < NUM_STR; i++) {
            unsigned char *thash_eb = sm_hasheb + i * NUM_CHAR * nout_chars;
            uint32_t rbase_off = i * nreads * nout_chars + q_id * nout_chars;
            int lhamming = 0;

            // we divide hits into groups of blockDim.x. each output will also
            // have blockDim.x hits grouped in one unit.
            //uint32_t obase_offset = i * nhits * nout_chars +
            //    blockIdx.x * blockDim.x * nout_chars + threadIdx.x;

            #ifdef CGK2_EMBED
            int j = 0;
            for (unsigned k = 0; k < rlen; k++) {
                uint8_t s = sm_input[k + ibase_offset];
                char hash_bit = thash_eb[j * NUM_CHAR + s];
                if (!hash_bit) {
                    lhamming += (d_qembed[rbase_off + j] == s ? 0 : 1);
                    j++;
                } else {
                    lhamming += (d_qembed[rbase_off + j] == s ? 0 : 1);
                    lhamming += (d_qembed[rbase_off + j + 1] == s ? 0 : 1);
                    j += 2;
                }

//                if (lhamming >= ham_threshold) {
//                    lhamming = nout_chars;
//                    break;
//                }
            }
            #else
            uint32_t in_pos = 0;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < rlen ? sm_input[in_pos + ibase_offset] : EMBED_PAD;
                //d_output[obase_offset + j * ninput_per_block] = s;
                char hash_bit = thash_eb[j * NUM_CHAR + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);

                // now, also get reference and check
                char rchr = d_qembed[rbase_off + j];
                lhamming = lhamming + (s == rchr ? 0 : 1);
//                if (lhamming >= ham_threshold) {
//                    lhamming = nout_chars;
//                    break;
//                }
            }
            #endif


            hamming = lhamming < hamming ? lhamming : hamming;
        }

//        end:
#if DBGPRINT
        printf("Read %d at pos %"PRIu32" has nmismatch %d \n", q_id, pos, hamming);
#endif

        // q_id in first 16 bits, then hamming distance, then position.
        d_hamming[idx] = (((uint64_t)q_id) << QID_SHIFT | ((uint64_t) hamming << HAMMING_SHIFT) | pos);

    }
}



__global__
void device_merged_embed_hamming(unsigned char *d_hasheb, int nbytes_hasheb,
                                 uint64_t *d_hits, unsigned nhits, unsigned rlen, char *d_input,
                                 unsigned nout_chars, char *d_qembed, unsigned nreads, uint64_t *d_hamming)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // load hasheb into shared memory
    extern __shared__ unsigned char sm_buf[];
    unsigned char *sm_hasheb = sm_buf;
    int nhash_blocks = nbytes_hasheb / blockDim.x + 1;
    int tid = threadIdx.x;
    for (int i = 0; i < nhash_blocks; i++) {
        if (tid + i * blockDim.x < nbytes_hasheb)
            sm_hasheb[tid + i * blockDim.x] = d_hasheb[tid + i * blockDim.x];
    }
    __syncthreads();

    // load the input corresponding to this thread block.
    // Each thread block has to read blockIdx.x * rlen (or rlen) charcters of input.
    // This will amount to blockDim.x inputs starting from blockDIm.x * blockIdx.x.
    // have each thread cooperatively load blockDim.x * rlen characters.
    // Each thread first loads first blockDim.x chars, then next set and so on.
    // So there are rlen repetitions.
    unsigned char *sm_input = sm_buf + nbytes_hasheb;
    uint32_t input_offset = blockDim.x * blockIdx.x;
    uint32_t byte_offset = input_offset * rlen;
//    if (idx < nhits) {
//        for (int i = 0; i < rlen; i++) {
//            sm_input[i * blockDim.x + tid] = d_input[byte_offset + i * blockDim.x + tid];
//            //sm_input[i * ninput_per_block + tid] = d_input[idx * rlen + i];
//        }
//    }

    for (int i = 0; i < rlen; i++) {
        if (byte_offset + i * blockDim.x + tid < nhits * rlen) {
            sm_input[i * blockDim.x + tid] = d_input[byte_offset + i * blockDim.x + tid];
        }
    }

    __syncthreads();

    if (idx < nhits) {
        //int ibase_offset = idx;
        uint32_t ibase_offset = tid * rlen;
        uint64_t hit_val = d_hits[idx];
        uint32_t q_id = hit_val >> 32; // read id
        uint32_t pos = hit_val & 0xffffffff;
        short hamming = nout_chars; //3*rlen

        for (int i = 0; i < NUM_STR; i++) {
            unsigned char *thash_eb = sm_hasheb + i * NUM_CHAR * nout_chars;
            uint32_t rbase_off = i * nreads * nout_chars + q_id * nout_chars;
            int lhamming = 0;

            // we divide hits into groups of blockDim.x. each output will also
            // have blockDim.x hits grouped in one unit.
            //uint32_t obase_offset = i * nhits * nout_chars +
            //    blockIdx.x * blockDim.x * nout_chars + threadIdx.x;

            #ifdef CGK2_EMBED
            int j = 0;
            for (unsigned k = 0; k < rlen; k++) {
                uint8_t s = sm_input[k + ibase_offset];
                char hash_bit = thash_eb[j * NUM_CHAR + s];
                if (!hash_bit) {
                    lhamming += (d_qembed[rbase_off + j] == s ? 0 : 1);
                    j++;
                } else {
                    lhamming += (d_qembed[rbase_off + j] == s ? 0 : 1);
                    lhamming += (d_qembed[rbase_off + j + 1] == s ? 0 : 1);
                    j += 2;
                }
            }
            #else
            uint32_t in_pos = 0;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < rlen ? sm_input[in_pos + ibase_offset] : EMBED_PAD;
                //d_output[obase_offset + j * ninput_per_block] = s;
                char hash_bit = thash_eb[j * NUM_CHAR + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);

                // now, also get reference and check
                char rchr = d_qembed[rbase_off + j];
                lhamming = lhamming + (s == rchr ? 0 : 1);
            }
            #endif

            hamming = lhamming < hamming ? lhamming : hamming;
        }

#if DBGPRINT
        printf("Read %d at pos %"PRIu32" has nmismatch %d \n", q_id, pos, hamming);
#endif

        // q_id in first 16 bits, then hamming distance 16 bit , then position 32 bit.
        d_hamming[idx] = (((uint64_t)q_id) << QID_SHIFT | ((uint64_t) hamming << HAMMING_SHIFT) | pos);
    }
}

 __global__
void device_embed_aligned(int num_str, int num_char,
        unsigned char *d_hasheb, int nbytes_hasheb,
        unsigned ninput, unsigned nin_chars, char *d_input,
        unsigned noutput, unsigned nout_chars, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // load hasheb into shared memory
    extern __shared__ unsigned char sm_buf[];
    unsigned char *sm_hasheb = sm_buf;
    int nhash_blocks = nbytes_hasheb / blockDim.x + 1;
    int tid = threadIdx.x;
    for (int i = 0; i < nhash_blocks; i++) {
        if (tid + i * blockDim.x < nbytes_hasheb)
            sm_hasheb[tid + i * blockDim.x] = d_hasheb[tid + i * blockDim.x];
    }
    __syncthreads();

    // load the input corresponding to this thread block. Each thread block has
    // to read blockIdx.x * rlen (or nin_chars) charcters of input. This will
    // amount to blockDim.x inputs starting from from blockDIm.x * blockIdx.x.
    // have each thread cooperatively load blockDim.x * rlen characters. Each
    // thread first loads first blockDim.x chars, then next set and so on. So
    // there are rlen repetitions.
    unsigned char *sm_input = sm_buf + nbytes_hasheb;
    int ninput_per_block = blockDim.x;
    uint32_t input_offset = blockDim.x * blockIdx.x;
    uint32_t byte_offset = input_offset * nin_chars;
    if (idx < ninput) {
        for (int i = 0; i < nin_chars; i++) {
            sm_input[i * blockDim.x + tid] = d_input[byte_offset + i *
                blockDim.x + tid];
            //sm_input[i * ninput_per_block + tid] = d_input[idx * nin_chars + i];
        }
    }
    __syncthreads();

    //if (idx < ninput) {
    if (idx < ninput) {
        //int ibase_offset = idx;
        uint32_t ibase_offset = tid * nin_chars;

        for (int i = 0; i < num_str; i++) {
            unsigned in_pos = 0;
            unsigned char *thash_eb = sm_hasheb + i * num_char * nout_chars;

            // we divide hits into groups of blockDim.x. each output will also
            // have blockDim.x hits grouped in one unit.
            uint32_t obase_offset = i * ninput * nout_chars + 
                blockIdx.x * blockDim.x * nout_chars + threadIdx.x;
            for (int j = 0; j < nout_chars; j++) {
                //uint8_t s = in_pos / ninput < nin_chars ? d_input[in_pos +
                //    ibase_offset] : EMBED_PAD;
                uint8_t s = in_pos < nin_chars ? sm_input[in_pos +
                    ibase_offset] : EMBED_PAD;
                d_output[obase_offset + j * ninput_per_block] = s;
                char hash_bit = thash_eb[j * num_char + s];
                //in_pos = in_pos + ninput * (hash_bit >= 1 ? 0 : 1);
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);
            }
        }
    }
}

/* Function that does sequential memory access on both input and writing
 * out the output.
 */
__global__
void device_embed_unroll_aligned(int num_str, int num_char, unsigned char *d_hasheb,
        unsigned ninput, unsigned nin_chars, char *d_input,
        unsigned noutput, unsigned nout_chars, char *d_output)
{
    int idx = blockDim.x * blockIdx.x * 4 + threadIdx.x;

    if (idx +  3 * blockDim.x < ninput) {
        int ibase_offset1 = idx;
        int ibase_offset2 = (idx + blockDim.x);
        int ibase_offset3 = (idx + 2 * blockDim.x);
        int ibase_offset4 = (idx + 3 * blockDim.x);

        for (int i = 0; i < num_str; i++) {
            unsigned in_pos1 = 0, in_pos2 = 0, in_pos3 = 0, in_pos4 = 0;
            unsigned char *thash_eb = d_hasheb + i * num_char * nout_chars;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s1 = in_pos1 /ninput < nin_chars ? d_input[in_pos1 +
                    ibase_offset1] : EMBED_PAD;
                uint8_t s2 = in_pos2 /ninput < nin_chars ? d_input[in_pos2 +
                    ibase_offset2] : EMBED_PAD;
                uint8_t s3 = in_pos3 /ninput < nin_chars ? d_input[in_pos3 +
                    ibase_offset3] : EMBED_PAD;
                uint8_t s4 = in_pos4 /ninput < nin_chars ? d_input[in_pos4 +
                    ibase_offset4] : EMBED_PAD;

                d_output[i * ninput * nout_chars + j * ninput + idx] = s1;
                d_output[i * ninput * nout_chars + j * ninput + idx + blockDim.x] = s2;
                d_output[i * ninput * nout_chars + j * ninput + idx + 2 * blockDim.x] = s3;
                d_output[i * ninput * nout_chars + j * ninput + idx + 3 * blockDim.x] = s4;

                char hash_bit1 = thash_eb[j * num_char + s1];
                char hash_bit2 = thash_eb[j * num_char + s2];
                char hash_bit3 = thash_eb[j * num_char + s3];
                char hash_bit4 = thash_eb[j * num_char + s4];

                in_pos1 = in_pos1 + ninput * (hash_bit1 >= 1 ? 0 : 1);
                in_pos2 = in_pos2 + ninput * (hash_bit2 >= 1 ? 0 : 1);
                in_pos3 = in_pos3 + ninput * (hash_bit3 >= 1 ? 0 : 1);
                in_pos4 = in_pos4 + ninput * (hash_bit4 >= 1 ? 0 : 1);
            }
        }
    }
}

void gpu_embed_pos(uint64_t **d_ref, Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits, char **d_cembed)
{
    GPUMemMgr &my_mgr = g_memmgr[0];
    struct timeval tstart, tend;
    gettimeofday(&tstart, NULL);

    // allocate space for crefs first. This helps in compacting later when we
    // free off space alloced for offsets and index
    size_t ncref_chars = nhits * rlen;
    char *d_crefs = reinterpret_cast<char *>(my_mgr.alloc(ncref_chars));
    assert(d_crefs);
    cuda(Memset(d_crefs, 0, ncref_chars));
    cuda(DeviceSynchronize());

    //fetch the references
    //cerr << "gpu_embed_pos: gathering crefs\n";
    {
        dim3 block(rlen);
        dim3 grid((ncref_chars + block.x - 1) / block.x);
        //int nref_words = rlen / NT_PER_WORD + 1;
        //int total_sm = (1 /* for hit */ + nref_words) * sizeof(uint64_t);
        //opt_device_gather_refs_sm<<<grid.x, block, total_sm>>>(d_hits, nhits, nref_words, ncref_chars, rlen,
       //         d_ref[0], d_crefs);
        opt_device_gather_refs<<<grid.x, block>>>(d_hits, nhits, ncref_chars, rlen,
                d_ref[0], d_crefs);
        cuda(DeviceSynchronize());
    }

    // alloc space for output
    //cerr << "Allocing output for " << nhits << " hits on gpu" << endl;
    size_t noutput = nhits * NUM_STR;
    unsigned nout_chars = rlen * 3;
    size_t nout_bytes = noutput * nout_chars;
    char *d_output = reinterpret_cast<char *>(my_mgr.alloc(nout_bytes));
    assert(d_output);

    // now embed
    unsigned nhash_bytes = NUM_STR * NUM_CHAR * (MAX_ELEN + 1);
    {
        dim3 block(NHITS_PER_BLOCK_EMBED);
        dim3 grid((nhits + block.x - 1) / block.x);
        unsigned nin_bytes_per_block = rlen * block.x;
        int total_sm = nhash_bytes + nin_bytes_per_block;
        //cerr << "Trying to use " << total_sm << " bytes of SM\n";
        device_embed_aligned<<<grid.x, block, total_sm>>>(NUM_STR, NUM_CHAR,
                e->d_hasheb[0], nhash_bytes, nhits, rlen,
                d_crefs, noutput, nout_chars, d_output);
        cuda(DeviceSynchronize());
        gettimeofday(&tend, NULL);
    }
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running embedding kernel in " <<
    //    compute_elapsed(&tstart, &tend) << " secs " << endl;

    my_mgr.free(d_crefs);
    my_mgr.compact();

    *d_cembed = d_output;
}

void gpu_all_merged(uint64_t **d_ref, Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits, char *d_Qembed,
        unsigned nQembed, uint64_t **d_hamming)
{
    // allocate bytes for hamming
    GPUMemMgr &my_mgr = g_memmgr[0];
    struct timeval tstart, tend;
    int nhamming_bytes = nhits * sizeof(uint64_t);
    uint64_t *d_hamout = reinterpret_cast<uint64_t *>(my_mgr.alloc(nhamming_bytes));
    assert(d_hamout);    

    // now do embedding and hamming
    size_t noutput = nhits * NUM_STR;
    unsigned nout_chars = rlen * 3;
    unsigned nhash_chars = NUM_STR * NUM_CHAR * (MAX_ELEN + 1);
    size_t nhash_bytes = nhash_chars * sizeof(uint32_t);
    {
        dim3 block(64);
        dim3 grid((nhits + block.x - 1) / block.x);
        size_t nhit_bytes_per_block = block.x * sizeof(uint64_t);
        size_t nin_bytes_per_block = block.x * rlen * sizeof(uint32_t);
        size_t total_sm = nhash_bytes + nhit_bytes_per_block + nin_bytes_per_block;
        assert(total_sm < 48 * KB);
        cerr << "Trying to use " << total_sm << " bytes of SM\n";
        device_all_merged<<<grid.x, block, total_sm>>>(NUM_STR, NUM_CHAR,
                e->d_hasheb[0], nhash_chars, d_hits, nhits, rlen,
                (char *)d_ref[0], noutput, nout_chars, d_Qembed, nQembed,
                d_hamout);
        cuda(DeviceSynchronize());
        gettimeofday(&tend, NULL);
    }
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running embedding kernel in " <<
    //    compute_elapsed(&tstart, &tend) << " secs " << endl;

    my_mgr.compact();

    *d_hamming = d_hamout;
}

void gpu_merged_embed_hamming(int gpu_id, uint64_t **d_ref, Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits, char *d_Qembed, unsigned nQembed,
        uint64_t *d_hcov_hamming, uint64_t **d_hamming)
{
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    struct timeval tstart, tend;
    gettimeofday(&tstart, NULL);

    // allocate space for crefs first. This helps in compacting later when we
    // free off space alloced for offsets and index
    size_t ncref_chars = nhits * (size_t)rlen;
    char *d_crefs = reinterpret_cast<char *>(my_mgr.alloc(ncref_chars));
    assert(d_crefs);
    cuda(Memset(d_crefs, 0, ncref_chars));
    cuda(DeviceSynchronize());

    //fetch the references
    //cerr << "gpu_embed_pos: gathering crefs\n";
    {
#if ENABLE_REF_COMPRESSION
        dim3 block(rlen);
        dim3 grid((ncref_chars + block.x - 1) / block.x);
        opt_device_gather_refs<<<grid.x, block>>>(d_hits, nhits, ncref_chars, rlen,
                d_ref[0], d_crefs);
#else
        dim3 block(256);
        dim3 grid((nhits + block.x - 1) / block.x);
        int nbytes_sm = block.x * sizeof(uint32_t);
        // get d_crefs: the candidates in reference for each hit
        device_uncompressed_gather_refs<<<grid.x, block, nbytes_sm>>>(d_hits, nhits, rlen,
                (char *)d_ref[gpu_id], d_crefs);
#endif
        cuda(DeviceSynchronize());
    }

    // alloc space for output
    //cerr << "Allocing output for " << nhits << " hits on gpu" << endl;
    size_t noutput = nhits * NUM_STR;
    unsigned nout_chars = rlen * EMBED_FACTOR;

    // allocate bytes for hamming
    int nhamming_bytes = nhits * sizeof(uint64_t);
    uint64_t *d_hamout = reinterpret_cast<uint64_t *>(my_mgr.alloc(nhamming_bytes));
    assert(d_hamout);    

    // now do embedding and hamming
    unsigned nhash_bytes = NUM_STR * NUM_CHAR * (MAX_ELEN + 1);
    {
        dim3 block(NHITS_PER_BLOCK_EMBED);
        dim3 grid((nhits + block.x - 1) / block.x);
        unsigned nin_bytes_per_block = rlen * block.x;
        int total_sm = nhash_bytes + nin_bytes_per_block;
        //cerr << "Trying to use " << total_sm << " bytes of SM\n";
        if (d_hcov_hamming) {
            device_merged_embed_wthreshold<<<grid.x, block, total_sm>>>(
                    e->d_hasheb[gpu_id], nhash_bytes, d_hits, nhits, rlen,
                    d_crefs, noutput, nout_chars, d_Qembed, nQembed,
                    d_hcov_hamming, d_hamout);
        } else {
            device_merged_embed_hamming<<<grid.x, block, total_sm>>>(
                    e->d_hasheb[gpu_id], nhash_bytes, d_hits, nhits, rlen,
                    d_crefs, nout_chars, d_Qembed, nQembed,
                    d_hamout);
        }
        cuda(DeviceSynchronize());
    }
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running embedding kernel in " <<
    //    compute_elapsed(&tstart, &tend) << " secs " << endl;

    my_mgr.free(d_crefs);
    my_mgr.compact();

    *d_hamming = d_hamout;
}

void gpu_compute_hamming(Embedding *e, unsigned rlen,
        uint64_t *d_hits, int nhits,
        char *d_cembed, char *d_Qembed, unsigned nQembed, uint64_t **d_hamming)
{
    GPUMemMgr &my_mgr = g_memmgr[0];
    int nhamming_bytes = nhits * sizeof(uint64_t);
    uint64_t *d_tmp = reinterpret_cast<uint64_t *>(my_mgr.alloc(nhamming_bytes));
    assert(d_tmp);    

    struct timeval tstart, tend;
    //cerr << "Now running hamming kernel. " << endl;
    gettimeofday(&tstart, NULL);
    dim3 block(NHITS_PER_BLOCK_EMBED);
    dim3 grid((nhits + block.x - 1) / block.x);
    device_compute_hamming<<<grid.x, block>>>(NUM_STR, NUM_CHAR, nhits,
            d_hits, rlen * 3, d_cembed, d_Qembed, nQembed, d_tmp);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running hamming kernel. " << endl;

    *d_hamming = d_tmp;
}

void gpu_embed_reference(Embedding *e, unsigned rlen, int kmer_step,
        int nposv, uint64_t **d_ref, char **h_output)
{
    /* Preembed the entire reference and store it here. */
    //alloc space for output
    size_t nbytes_per_pos = rlen * 3;
    //int nwords_per_pos = rlen * 3 / NT_PER_WORD + 1;
    //*h_output = new uint64_t[nposv * nwords_perpos];
    char *h_rembed;
    cuda(HostAlloc((void **)&h_rembed, nbytes_per_pos * nposv, cudaHostAllocPortable));
    assert(h_rembed);

    /* We don't have enough memory on gpu to embed all at once. With 7GB, we can
     * do about 16M locations at 450 bytes/loc. So we loop X * 1M locations at a
     * time and embed the reference, where X is kmer_step size. This is done to
     * make sure that we are always aligned at a kmer starting point.
     */
    GPUMemMgr &my_mgr = g_memmgr[0];
    int batch_size = 1000000 * kmer_step;
    uint64_t *d_pos =
        reinterpret_cast<uint64_t *>(my_mgr.alloc(batch_size * sizeof(uint64_t)));
    thrust::device_ptr<uint64_t> d_pos_ptr(d_pos);
    char *d_rembed = NULL;
    for (int i = 0; i < nposv; i += batch_size) {
        int ninput = nposv - i > batch_size ? batch_size : nposv - i;
        size_t nbytes_total = nbytes_per_pos * ninput;
        thrust::sequence(d_pos_ptr, d_pos_ptr + ninput, (uint64_t)i * kmer_step,
                (uint64_t)kmer_step);
        gpu_embed_pos(d_ref, e, rlen, d_pos, ninput, &d_rembed);
        char *host_addr = h_rembed + (size_t) i * nbytes_per_pos;
        cerr << "Done " << i << " positions" << endl;
        cuda(Memcpy(host_addr, d_rembed, nbytes_total, cudaMemcpyDeviceToHost));
        my_mgr.free(d_rembed);
    }
    my_mgr.free(d_pos);

    *h_output = h_rembed;
}

/****************************************************************************
 *          Functions below are used only for benchmarking purposes         *
 ***************************************************************************/
__global__
void device_bench_hamming(int num_str, int num_char,
        unsigned noriginal, unsigned nembedded, unsigned nembed_chr,
        char *d_input, short *d_hamming)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x + 1;
    if (idx < nembedded) {
        short hamming = nembed_chr;
        for (unsigned i = 0; i < num_str; i++) {
            int lhamming = 0;
            int rbase_off = i * noriginal * nembed_chr;
            int cbase_off = rbase_off + idx;
            for (unsigned j = 0; j < nembed_chr; j++) {
                char cchr = d_input[cbase_off + j * noriginal];
                char rchr = d_input[rbase_off + j * noriginal];
                lhamming = lhamming + (cchr == rchr ? 0 : 1);
            }

            hamming = lhamming < hamming ? lhamming : hamming;
        }

        d_hamming[idx] = hamming;
    }
}

/* Embed read and reference candidates.
 * Call expects crefs to have positions. ninput should be d_cresf.size() + 1.
 * d_crefs at offset 0 is a dummy entry. Actual candidates should start at 1.
 */
__global__
void device_bench_embed(int num_str, int num_char,
        unsigned char *d_hasheb, char *d_ref,
        unsigned ninput, unsigned nin_chars,  char *d_Q, uint32_t *d_crefs,
        unsigned noutput, unsigned nout_chars, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < ninput) {
        uint32_t pos = d_crefs[idx];
        char *str_input = (idx == 0 ? d_Q : (d_ref + pos));

        for (int i = 0; i < num_str; i++) {
            unsigned in_pos = 0;
            unsigned char *thash_eb = d_hasheb + i * num_char * nout_chars;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < nin_chars ? str_input[in_pos] : EMBED_PAD;
                d_output[i * ninput * nout_chars + j * ninput + idx] = s;
                char hash_bit = thash_eb[j * num_char + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);
            }
        }
    }
}

/* Function that does random memory access on input but sequential on writing
 * out the output. The input is also a position rather than actual strings.
 */
__global__
void device_bench_embed_pos_input(int num_str, int num_char,
        unsigned char *d_hasheb, char *d_ref,
        unsigned ninput, unsigned nin_chars, uint32_t *d_input,
        unsigned noutput, unsigned nout_chars, char *d_output)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < ninput) {
        uint32_t pos = d_input[idx];
        char *str_input = d_ref + pos;

        for (int i = 0; i < num_str; i++) {
            unsigned in_pos = 0;
            unsigned char *thash_eb = d_hasheb + i * num_char * nout_chars;
            for (int j = 0; j < nout_chars; j++) {
                uint8_t s = in_pos < nin_chars ? str_input[in_pos] : EMBED_PAD;
                d_output[i * ninput * nout_chars + j * ninput + idx] = s;
                char hash_bit = thash_eb[j * num_char + s];
                in_pos = in_pos + (hash_bit >= 1 ? 0 : 1);
            }
        }
    }
}


void bench_gpu_embed_data_by_str(Embedding *e, vector<const char *> &input, unsigned in_size,
        vector<string> &output)
{
    struct timeval tstart, tend;
    unsigned ninput = input.size();
    unsigned nchars = in_size;
    unsigned nin_bytes = ninput * nchars;
    char *h_input = new char[nin_bytes];
    /*
    for (unsigned i = 0; i < nchars; i++)
        for (unsigned j = 0; j < ninput; j++)
            h_input[i * ninput + j] = input[j][i];
    */
    for (unsigned i = 0; i < ninput; i++)
        memcpy(h_input + i * nchars, input[i], nchars);
    //cerr << "Done formatting input" << endl;

    // copy input
    gettimeofday(&tstart, NULL);
    GPUMemMgr &my_mgr = g_memmgr[0];
    char *d_input = reinterpret_cast<char *>(my_mgr.alloc(nin_bytes));
    assert(d_input);
    cuda(Memcpy(d_input, h_input, nin_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done copying " << nin_bytes << " of input to gpu " << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // alloc space for output
    gettimeofday(&tstart, NULL);
    unsigned noutput = ninput * NUM_STR;
    unsigned nout_chars = nchars * 3;
    unsigned nout_bytes = noutput * nout_chars;
    char *d_output = reinterpret_cast<char *>(my_mgr.alloc(nout_bytes));
    assert(d_output);
    //cerr << "Done allocing output on gpu" << endl;

    // copy ds needed for embedding
    gettimeofday(&tstart, NULL);
    unsigned nhash_bytes = NUM_STR * NUM_CHAR * nout_chars;
    unsigned char *d_hasheb = reinterpret_cast<unsigned char *>(my_mgr.alloc(nhash_bytes));
    assert(d_hasheb);
    std::bitset<TOTAL_RBITS> *hash_eb_p;
    hash_eb_p = &(e->hash_eb);
    cuda(Memcpy(d_hasheb, hash_eb_p, nhash_bytes, cudaMemcpyHostToDevice));
   // cuda(Memcpy(d_hasheb, &(e->hash_eb[0]), nhash_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done allocing hasheb on gpu" << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 block(1024);
    dim3 grid((ninput + block.x -1) / block.x);
    gettimeofday(&tstart, NULL);
    device_embed_unaligned<<<grid.x, block>>>(NUM_STR, NUM_CHAR, d_hasheb,
            ninput, nchars, d_input,
            noutput, nout_chars, d_output);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running kernel. " << endl;

    // copy back output
    gettimeofday(&tstart, NULL);
    char *h_output = new char[nout_bytes];
    cuda(Memcpy(h_output, d_output, nout_bytes, cudaMemcpyDeviceToHost));
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done copying output. Reformatting now." << endl;

    for (unsigned i = 0; i < nout_bytes; i++) {
        int rnd = i / (ninput * nout_chars);
        int strid = i % ninput;
        int chrid = (i / ninput) % nout_chars;
        output[NUM_STR * strid + rnd][chrid] = h_output[i];
    }

    delete[] h_output;
    my_mgr.free(d_output);
    my_mgr.free(d_hasheb);
    my_mgr.free(d_input);
}

void bench_gpu_embed_data_by_pos(char **d_ref, Embedding *e,
        vector<uint32_t> &input, unsigned in_size, vector<string> &output)
{
    struct timeval tstart, tend;
    unsigned ninput = input.size();
    unsigned nin_bytes = ninput * sizeof(uint32_t);

    // copy input
    gettimeofday(&tstart, NULL);
    GPUMemMgr &my_mgr = g_memmgr[0];
    uint32_t *d_input = reinterpret_cast<uint32_t *>(my_mgr.alloc(nin_bytes));
    assert(d_input);
    cuda(Memcpy(d_input, &input[0], nin_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done copying " << nin_bytes << " of input to gpu " << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // alloc space for output
    gettimeofday(&tstart, NULL);
    unsigned noutput = ninput * NUM_STR;
    unsigned nout_chars = in_size * 3;
    unsigned nout_bytes = noutput * nout_chars;
    char *d_output = reinterpret_cast<char *>(my_mgr.alloc(nout_bytes));
    assert(d_output);
    //cerr << "Done allocing output on gpu" << endl;

    // copy ds needed for embedding
    gettimeofday(&tstart, NULL);
    unsigned nhash_bytes = NUM_STR * NUM_CHAR * nout_chars;
    unsigned char *d_hasheb = reinterpret_cast<unsigned char *>(my_mgr.alloc(nhash_bytes));
    assert(d_hasheb);
    std::bitset<TOTAL_RBITS> *hash_eb_p;
    hash_eb_p = &(e->hash_eb);
    cuda(Memcpy(d_hasheb, hash_eb_p, nhash_bytes, cudaMemcpyHostToDevice));
    //cuda(Memcpy(d_hasheb, &(e->hash_eb[0]), nhash_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done allocing hasheb on gpu" << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 block(1024);
    dim3 grid((ninput + block.x -1) / block.x);
    gettimeofday(&tstart, NULL);
    /*
    device_embed_unaligned<<<grid.x, block>>>(NUM_STR, NUM_CHAR, d_hasheb,
            ninput, in_size, d_input,
            noutput, nout_chars, d_output);
     */
    device_bench_embed_pos_input<<<grid.x, block>>>(NUM_STR, NUM_CHAR,
            d_hasheb, d_ref[0],
            ninput, in_size, d_input,
            noutput, nout_chars, d_output);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running embedding kernel. " << endl;

    //cerr << "Now running hamming kernel. " << endl;
    int nhamming_bytes = ninput * sizeof(short);
    short *d_hamming = reinterpret_cast<short *>(my_mgr.alloc(nhamming_bytes));
    assert(d_hamming);
    device_bench_hamming<<<grid.x, block>>>(NUM_STR, NUM_CHAR, ninput,
            noutput, nout_chars, d_output, d_hamming);

    // copy back output and hamming
    short *h_hamming = new short[ninput];
    char *h_output = new char[nout_bytes];
    gettimeofday(&tstart, NULL);
    cuda(Memcpy(h_hamming, d_hamming, nhamming_bytes, cudaMemcpyDeviceToHost));
    cuda(Memcpy(h_output, d_output, nout_bytes, cudaMemcpyDeviceToHost));
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done copying output. Reformatting now." << endl;

    for (unsigned i = 0; i < nout_bytes; i++) {
        int rnd = i / (ninput * nout_chars);
        int strid = i % ninput;
        int chrid = (i / ninput) % nout_chars;
        output[NUM_STR * strid + rnd][chrid] = h_output[i];
    }

    for (unsigned i = 1; i < ninput; i++) {
        int max_mismatch = nout_chars;
        for (int r = 0; r < NUM_STR; ++r) {
            int nmismatch = 0;
            for (unsigned j = 0; j < nout_chars; ++j) {
                if (output[i * NUM_STR + r][j] != output[r][j])
                    nmismatch++;
            }

            if (nmismatch < max_mismatch)
                max_mismatch = nmismatch;
        }

        assert(h_hamming[i] == max_mismatch);

    }

    delete[] h_output;
    my_mgr.free(d_output);
    my_mgr.free(d_hasheb);
    my_mgr.free(d_input);
}

void benchmark_gpu_embed(uint64_t **d_ref, Embedding *e, vector<uint32_t> &input, string &Q,
        unsigned in_size, vector<short> &hamming)
{
    struct timeval tstart, tend;
    unsigned ninput = input.size();
    unsigned nin_bytes = ninput * sizeof(uint32_t);
    GPUMemMgr &my_mgr = g_memmgr[0];

    // copy input
    gettimeofday(&tstart, NULL);
    char *d_Q = reinterpret_cast<char *>(my_mgr.alloc(in_size));
    assert(d_Q);
    cuda(Memcpy(d_Q, Q.c_str(), in_size, cudaMemcpyHostToDevice)); 
    uint32_t *d_input = reinterpret_cast<uint32_t *>(my_mgr.alloc(nin_bytes));
    assert(d_input);
    cuda(Memcpy(d_input, &input[0], nin_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done copying " << nin_bytes << " of input to gpu " << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // alloc space for output
    gettimeofday(&tstart, NULL);
    //cerr << "Allocing output for " << ninput << " on gpu" << endl;
    unsigned noutput = ninput * NUM_STR;
    unsigned nout_chars = in_size * 3;
    unsigned nout_bytes = noutput * nout_chars;
    char *d_output = reinterpret_cast<char *>(my_mgr.alloc(nout_bytes));
    assert(d_output);

    // copy ds needed for embedding
    gettimeofday(&tstart, NULL);
    unsigned nhash_bytes = NUM_STR * NUM_CHAR * nout_chars;
    unsigned char *d_hasheb = reinterpret_cast<unsigned char *>(my_mgr.alloc(nhash_bytes));
    assert(d_hasheb);
    std::bitset<TOTAL_RBITS> *hash_eb_p;
    hash_eb_p = &(e->hash_eb);
    cuda(Memcpy(d_hasheb, hash_eb_p, nhash_bytes, cudaMemcpyHostToDevice));
    //cuda(Memcpy(d_hasheb, &(e->hash_eb[0]), nhash_bytes, cudaMemcpyHostToDevice)); 
    //cerr << "Done allocing hasheb on gpu" << endl;
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);

    // call kernel
    gettimeofday(&tstart, NULL);
    dim3 block(1024);
    dim3 grid((ninput + block.x -1) / block.x);
    gettimeofday(&tstart, NULL);
    device_bench_embed<<<grid.x, block>>>(NUM_STR, NUM_CHAR,
            d_hasheb, (char *)d_ref[0],
            ninput, in_size, d_Q, d_input,
            noutput, nout_chars, d_output);
    cuda(DeviceSynchronize());
    gettimeofday(&tend, NULL);
    gstats[0].gpu_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done running embedding kernel. " << endl;

    //cerr << "Now running hamming kernel. " << endl;
    int nhamming_bytes = ninput * sizeof(short);
    short *d_hamming = reinterpret_cast<short *>(my_mgr.alloc(nhamming_bytes));
    assert(d_hamming);
    device_bench_hamming<<<grid.x, block>>>(NUM_STR, NUM_CHAR, ninput,
            noutput, nout_chars, d_output, d_hamming);

    // copy back hamming
    gettimeofday(&tstart, NULL);
    cuda(Memcpy(&hamming[0], d_hamming, nhamming_bytes, cudaMemcpyDeviceToHost));
    gettimeofday(&tend, NULL);
    gstats[0].op_pcie_time += compute_elapsed(&tstart, &tend);
    //cerr << "Done copying output. Reformatting now." << endl;

    my_mgr.free(d_Q);
    my_mgr.free(d_output);
    my_mgr.free(d_hasheb);
    my_mgr.free(d_input);
}
