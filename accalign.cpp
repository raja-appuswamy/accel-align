#include "header.h"
#include "accalign.h"
#include "ksw2.h"

using namespace tbb::flow;
using namespace std;

// XXX: step needs to be in sync with the index. Make this automatic later.
int f = 0, r = 0;
int kmer_len = 32;
int kmer_step = 3;
int g_mode = SHORT_READ_MODE;
uint64_t mask;
unsigned g_filter = PIGEONHOLE_FILTER;
const unsigned pairdis = 1000;
unsigned g_nreads = 0;
string g_out, g_batch_file, g_embed_file;
char symbol[6] = "ACGTN", rcsymbol[6] = "TGCAN";
uint8_t code[256];
bool toExtend = true;

#if ENABLE_GPU
GPUMemMgr *g_memmgr;
HostMemMgr *h_memmgr;
#endif
struct gpu_stats gstats[50];
std::mutex *g_gpu_mutex;
int g_ngpus = 1, g_ncpus = 1;
static volatile int g_ncpu_align_threads = NCORES - 1 /*root*/ - 1 /*gpumapblk*/ - NGPU_ALIGN_THREADS;
int g_hit_threshold = 40;
float gztime = 0, buildTime = 0, otherBuildTime = 0, delTime = 0, cpuDelTime = 0;
float eraseTime = 0, inTime = 0, resizeTime = 0, pushTime = 0, popTime = 0, min_len_time = 0, alignTime = 0, mapqTime;

/* XXX: The logic for dealing with N's is spread throughput the code. Needs to
 * be fixed.
 */
void make_code(void) {
    for (size_t i = 0; i < 256; i++)
        code[i] = 4;

    code['A'] = code['a'] = 0;
    code['C'] = code['c'] = 1;
    code['G'] = code['g'] = 2;
    code['T'] = code['t'] = 3;

    // we set N's also to 0. Shouldn't matter as index construction logic
    // doesn't consider kmers with Ns anyway
    code['N'] = code['n'] = 0;
}

//static void set_affinity(pthread_t thread, int cpuid) {
//    int ntotal_threads = std::thread::hardware_concurrency() - 1;
//    cpu_set_t cpuset;
//    CPU_ZERO(&cpuset);
//    CPU_SET(cpuid % ntotal_threads, &cpuset);
//    //cerr << "Setting thread to cpu " << cpuid % ntotal_threads << " out of " << ntotal_threads << endl;
//
//    int r = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
//    assert(r == 0);
//}

static void parse(char seq[], char fwd[], char rev[], char rev_str[]) {
    unsigned len = strlen(seq);

    for (size_t i = 0; i < len; i++) {
        uint8_t c = *(code + seq[i]);
        fwd[i] = c;
        rev[len - 1 - i] = c == 4 ? c : 3 - c;
        rev_str[len - 1 - i] = rcsymbol[c];
    }
    *(fwd+len) = '\0';
    *(rev+len) = '\0';
    *(rev_str+len) = '\0';
}

gzFile &operator>>(gzFile &in, Read &r) {
    char temp[MAX_LEN];

    if (gzgets(in, r.name, MAX_LEN) == NULL) return in;
    if (gzgets(in, r.seq, MAX_LEN) == NULL) return in;
    if (gzgets(in, temp, MAX_LEN) == NULL) return in;
    if (gzgets(in, r.qua, MAX_LEN) == NULL) return in;

    // last char '\n' should be removed
    r.name[strlen(r.name)-1] = '\0';
    r.qua[strlen(r.qua)-1] = '\0';
    r.seq[strlen(r.seq)-1] = '\0';

    r.tid = r.pos = 0;
    r.as = numeric_limits<int32_t>::min();
    r.strand = '*';

    return in;
}

void print_usage()
{
    cerr<<"accalign [options] <ref.fa> [read1.fastq] [read2.fastq]\n";
    cerr<<"options:\n";
    cerr<<"\t-t INT number of threads to use[1]\n";
    cerr<<"\t-l INT length of seed [32].\n";
    cerr<<"\t-o name of output file to use\n";
    cerr<<"\t-x alignment-free\n";
    cerr<<"\t maximum read length and read name length supported are 1024";
}

void AccAlign::print_stats() {

#if ENABLE_GPU
    cerr << "Breakdown:\n" <<
         "Input IO time:\t" << input_io_time / 1000000 << "\n" <<
         "\t load in time: " << inTime / 1000000 << "\n" <<
         "\t resize time: " << resizeTime / 1000000 << "\n" <<
         "\t erase time: " << eraseTime / 1000000 << "\n" <<
         "\t push time: " << pushTime / 1000000 << "\n" <<
         "\t pop time: " << popTime / 1000000 << "\n" <<
         "delete vector<Read> * time: " << delTime / 1000000 << "\n" <<
         "Get min_rlen cpu time (if ENABLE_GPU):\t" << min_len_time / 1000000 << "\n" <<
         "Parse time:\t" << parse_time / 1000000 << "\n" <<
         "Seeding time: \t" << seeding_time / 1000000 << "\n" <<
         "\t Probe time:\t" << probe_time / 1000000 << "\n" <<
         "\t Hit count time:\t" << hit_count_time / 1000000 << "\n" <<
         "\t Vpair build:\t" << vpair_build_time / 1000000 << "\n" <<
         "\t Vpair sort:\t" << vpair_sort_time / 1000000 << "\n" <<
         "\t Vote time(total/pg/nonpg):\t" << (pg_vote_time + nonpg_vote_time) / 1000000 << "/" <<
         pg_vote_time / 1000000 << "/" << nonpg_vote_time / 1000000 << "\n" <<
         "Embedding time(total/actual):\t" << embedding_time / 1000000 << "/" << embedding->embed_time / 1000000 << "\n" <<
         "Extending time (+ build output string if ENABLE_GPU):\t" << alignTime / 1000000 << "\n" <<
         "\t extending time: " << sw_time / 1000000 << "\n" <<
         "\t build string time: " << sam_pre_time / 1000000 << "\n" <<
         "SAM output time :\t" << sam_time / 1000000 << "\n" <<
         std::endl << endl;

    cerr << "GPU Stat breakdown:\n" <<
//    "del vector<read> * for gpu thread: " << delTime / 1000000  << "\n" <<
//    "del vector<read> * for cpu thread: " << cpuDelTime / 1000000  << "\n" <<
//    "gztime: " << gztime / 1000000 << "\n" <<
//    "build asign string read time: " << buildTime / 1000000 << "\n" <<
//    "other build read time: " << otherBuildTime / 1000000 << "\n" <<
    " format read time: " << (double) gstats[19].gpu_time / 1000000 << "\n" <<
    " seed_hash copy to device time: " << (double) gstats[20].op_pcie_time / 1000000 << "\n" <<
    " seed_hash time: " << (double) gstats[20].gpu_time / 1000000 << "\n" <<
    " embed reads time: " << (double) gstats[21].gpu_time / 1000000 << "\n" <<
    " seed_lookup cpu time: " << (double) gstats[2].gpu_time / 1000000 << "\n" <<
    " seed_lookup gpu time: " << (double) gstats[3].gpu_time / 1000000 << "\n" <<
    " seed_lookup gpu unrolled time: " << (double) gstats[4].gpu_time / 1000000 << "\n" <<
    " opt_gpu_find_candidates fwd gpu time: " << (double) gstats[5].gpu_time / 1000000 << "\n" <<
    " opt_gpu_find_candidates rev gpu time: " << (double) gstats[11].gpu_time / 1000000 << "\n" <<
    " opt_device_find_candidates ExclusiveSum time: " << (double) gstats[6].gpu_time / 1000000 << "\n" <<
    " opt_device_find_candidates Sum time: " << (double) gstats[7].gpu_time / 1000000 << "\n" <<
    " opt_device_find_candidates scatter_if time: " << (double) gstats[8].gpu_time / 1000000 << "\n" <<
    " opt_device_find_candidates InclusiveScan time: " << (double) gstats[9].gpu_time / 1000000 << "\n" <<
    " opt_device_find_candidates 10 gpu time: " << (double) gstats[10].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize Unique: get total number of unique hits: " << (double) gstats[30].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize ReduceByKey: get cnt of each unique hit && the unique hits: " << (double) gstats[31].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize Unique: get the number of reads which has at least one hit: " << (double) gstats[32].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize reduce_by_key: count the number of unique hits for each read: " << (double) gstats[33].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize InclusiveSum: get the offset of every reads' hits: " << (double) gstats[34].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize ArgMax: get the hcov_cnt, and it's index of the segment: " << (double) gstats[35].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize call kernal get hcov_hit, hcov_cnt: " << (double) gstats[36].gpu_time / 1000000 << "\n" <<
    " gpu_dedup_and_prioritize transform: append all unique hit and count with rid: " << (double) gstats[37].gpu_time / 1000000 << "\n" <<
    " embed: embed and find hamming distance of hcov hits: " << (double) gstats[22].gpu_time / 1000000 << "\n" <<
    " embed: find f/r minimum hamming distance: " << (double) gstats[23].gpu_time / 1000000 << "\n" <<
    " embed: go over all f/r hits: " << (double) gstats[24].gpu_time / 1000000 << "\n" <<
    " embed: build align params for extension: " << (double) gstats[25].gpu_time / 1000000 << "\n" <<
    //    " Probe: GPU exec time: " << (double) gstats[0].gpu_time / 1000000 <<
//    " Probe: pcie time: " << (double) gstats[0].op_pcie_time / 1000000 << "\n" <<
    " sort: GPU exec time: " << (double) gstats[1].gpu_time / 1000000 <<
    " sort: pcie time: " << (double) gstats[1].op_pcie_time / 1000000 << endl;
#else
    cerr << "Breakdown:\n" <<
         "Input IO time:\t" << input_io_time / 1000000 << "\n" <<
         "Parse time:\t" << parse_time / 1000000 << "\n" <<
         "Seeding time: \t" << seeding_time / 1000000 << "\n" <<
         "\t Probe time:\t" << probe_time / 1000000 << "\n" <<
         "\t Hit count time:\t" << hit_count_time / 1000000 << "\n" <<
         "\t Swap high cov time:\t" << swap_time / 1000000 << "\n" <<
         "Embedding time(total/actual):\t" << embedding_time / 1000000 << "/" << embedding->embed_time / 1000000 << "\n" <<
         "Extending time (+ build output string if ENABLE_GPU):\t" << sw_time / 1000000 << "\n" <<
         "Mapq && mark for extention time:\t" << mapqTime / 1000000 << "\n" <<
         "SAM output time :\t" << sam_time / 1000000 << "\n" <<
          std::endl << endl;
#endif

    cerr << "Total pairs sorted: " << vpair_sort_count << endl;
}

typedef tuple<Read *, Read *> ReadPair;

struct tbb_map {
    AccAlign *accalign;

public:
    tbb_map(AccAlign *obj) : accalign(obj) {}

    Read *operator()(Read *r) {
        accalign->map_read(*r);
        return r;
    }

    ReadPair operator()(ReadPair p) {
        Read *mate1 = std::get<0>(p);
        Read *mate2 = std::get<1>(p);
        accalign->map_paired_read(*mate1, *mate2);

        return p;
    }
};

struct tbb_align {
    AccAlign *accalign;

public:
    tbb_align(AccAlign *obj) : accalign(obj) {}

    Read *operator()(Read *r) {
        accalign->align_read(*r);
        //accalign->wfa_align_read(*r);
	return r;
    }

    ReadPair operator()(ReadPair p) {
        Read *mate1 = std::get<0>(p);
        Read *mate2 = std::get<1>(p);
        accalign->align_paired_read(*mate1, *mate2);

        return p;
    }
};

struct tbb_score {
    AccAlign *accalign;

public:
    tbb_score(AccAlign *obj) : accalign(obj) {}

    continue_msg operator()(Read *r) {
        accalign->print_sam(*r);
#if ENABLE_GPU
#else
        delete r;
#endif
        return continue_msg();
    }

    continue_msg operator()(ReadPair p) {
        Read *mate1 = std::get<0>(p);
        Read *mate2 = std::get<1>(p);
        accalign->print_paired_sam(*mate1, *mate2);
        delete mate1;
        delete mate2;

        return continue_msg();
    }
};

bool AccAlign::tbb_fastq(const char *F1, const char *F2, int gpu_id) {
    struct timeval start, end;

    gettimeofday(&start, NULL);
    gzFile in1 = gzopen(F1, "rt");
    if (in1 == Z_NULL)
        return false;

    gzFile in2 = Z_NULL;
    char is_paired = false;
    if (strlen(F2) > 0) {
        is_paired = true;
        in2 = gzopen(F2, "rt");
        if (in2 == Z_NULL)
            return false;
    }

    gettimeofday(&end, NULL);
    input_io_time += compute_elapsed(&start, &end);
    cerr << "Reading fastq file " << F1 << ", " << F2 << "\n";

    // replace this broadcast node with source node
    if (!is_paired) {
        graph g;

        source_node < Read * > input_node(g, [&](Read *&r) -> bool {
            gettimeofday(&start, NULL);
            if (gzeof(in1)|| (gzgetc(in1) == EOF))
                return false;

            r = new Read;
            in1 >> *r;
            if (!strlen(r->seq))
                return false;

            return true;
            gettimeofday(&end, NULL);
            input_io_time += compute_elapsed(&start, &end);
        }, false);
        int max_objects = 10000000;
        limiter_node < Read * > lnode(g, max_objects);
        function_node < Read * , Read * > map_node(g, unlimited, tbb_map(this));
        function_node < Read * , Read * > align_node(g, unlimited, tbb_align(this));

        function_node < Read * , continue_msg > score_node(g, 1, tbb_score(this));

        make_edge(score_node, lnode.decrement);
        make_edge(align_node, score_node);
        make_edge(map_node, align_node);
        make_edge(lnode, map_node);
        make_edge(input_node, map_node);
        input_node.activate();
        g.wait_for_all();
    } else {
        graph g;
        source_node <ReadPair> input_node(g, [&](ReadPair &rp) -> bool {
            bool end1 = gzgetc(in1) == EOF;
            bool end2 = gzgetc(in2) == EOF;
            if (gzeof(in1) || end1 || end2)
                return false;

            Read *r = new Read;
            in1 >> *r;
            if (!strlen(r->seq))
                return false;
            get<0>(rp) = r;

            Read *r2 = new Read;
            in2 >> *r2;
            if (!strlen(r2->seq))
                return false;
            get<1>(rp) = r2;

            return true;
        }, false);

        int max_objects = 1000000;
        limiter_node < Read * > lnode(g, max_objects);
        function_node <ReadPair, ReadPair> map_node(g, unlimited, tbb_map(this));
        function_node <ReadPair, ReadPair> align_node(g, unlimited, tbb_align(this));
        function_node <ReadPair, continue_msg> score_node(g, 1, tbb_score(this));

        make_edge(score_node, lnode.decrement);
        make_edge(align_node, score_node);
        make_edge(map_node, align_node);
        make_edge(input_node, map_node);
        input_node.activate();
        g.wait_for_all();
    }

    gzclose(in1);
    gzclose(in2);

    return true;
}

int AccAlign::getdata(bool is_paired, int buf_size, gzFile &in1,
                      vector<Read> *ptlread, gzFile &in2, vector<Read> *ptlread2) {
    int nreads = 0;
    struct timeval start, end;
    gettimeofday(&start, NULL);
    while (!gzeof(in1)) {
        // get first read
        Read r;
        in1 >> r;
        if (!strlen(r.seq))
            break;
        ptlread->push_back(r);

        // if paired end, get second read
        if (is_paired) {
            Read r2;
            in2 >> r2;
            if (strlen(r2.seq) != strlen(r.seq)) {
                cout << "Failed to read read pair.\n";
                exit(1);
            }
            ptlread2->push_back(r2);
        }

        ++nreads;
        if (nreads % buf_size == 0)
            break;
    }

    gettimeofday(&end, NULL);
    input_io_time += compute_elapsed(&start, &end);
    return nreads;
}

//bool AccAlign::fastq(const char *F1, const char *F2, int gpu_id) {
//    vector<Read> *ptlread = new vector<Read>;
//    vector<vector<Read> *> vtlread;
//    gzFile in1 = gzopen(F1, "rt");
//    bool batch_mode = gpu_id == -1 ? false : true;
//    if (in1 == Z_NULL)
//        return false;
//
//    gzFile in2 = Z_NULL;
//    char is_paired = false;
//    vector<Read> *ptlread2 = NULL;
//    vector<vector<Read> *> vtlread2;
//    if (strlen(F2) > 0) {
//        is_paired = true;
//        in2 = gzopen(F2, "rt");
//        if (in2 == Z_NULL)
//            return false;
//        ptlread2 = new vector<Read>;
//    }
//
//    vector<thread> gpu_threads;
//    size_t nreads = 0;
//    cerr << "Reading fastq file " << F1 << ", " << F2 << "\n";
//    int cpu_id = batch_mode ? gpu_id : 1;
//    gpu_id = 0;
//
//    int ngpu_launches = 0;
//    do {
//        // now launch GPU jobs
//        nreads = getdata(is_paired, GPU_BUFFER, in1, ptlread, in2, ptlread2);
//        for (auto &t : gpu_threads)
//            t.join();
//        gpu_threads.clear();
//
//        // launch a new mapblock
//        if (nreads) {
//            ++ngpu_launches;
//            gpu_threads.push_back(thread(&AccAlign::map_block_wrapper,
//                                         this, cpu_id, gpu_id, nreads, ptlread, ptlread2));
//            set_affinity(gpu_threads.back().native_handle(), 2);
//        }
//
//        // now process previous round if there was one
//        if (ngpu_launches > 1 || nreads == 0) {
//            vector<Read> *old_ptl = vtlread.back();
//            size_t nreads = old_ptl->size();
//            vector<Read> *old_ptl2 = NULL;
//            if (is_paired)
//                old_ptl2 = vtlread2.back();
//
//            vector<thread> align_threads;
//            int nalign_threads = 4;
//            int soff = 0, nreads_per_thread = old_ptl->size() / nalign_threads;
//            assert(nreads % nalign_threads == 0);
//            for (int i = 0; i < nalign_threads; i++, soff += nreads_per_thread) {
//                align_threads.push_back(thread(&AccAlign::align_wrapper,
//                                               this, cpu_id, soff, soff + nreads_per_thread,
//                                               old_ptl, old_ptl2, (bool *) NULL));
//
//                set_affinity(align_threads.back().native_handle(), 4 + i * 2);
//            }
//
//            for (auto &t : align_threads)
//                t.join();
//        }
//
//        //prep for new round of input
//        if (nreads) {
//            vtlread.push_back(ptlread);
//            if (is_paired)
//                vtlread2.push_back(ptlread2);
//
//            ptlread = new vector<Read>;
//            if (is_paired)
//                ptlread2 = new vector<Read>;
//        }
//    } while (nreads);
//
//    // free off previous inputs
//    for (auto ptlread : vtlread)
//        delete ptlread;
//    vtlread.clear();
//    if (is_paired) {
//        for (auto ptlread2 : vtlread2)
//            delete ptlread2;
//        vtlread2.clear();
//    }
//
//    cerr << "done reading fastq file " << F1 << "\n";
//
//    cerr << '\n';
//    gzclose(in1);
//    gzclose(in2);
//
//    //for (size_t i = 0; i < vtlread.size(); i++)
//    //    delete vtlread[i];
//
//#if FLUSH_EMBEDDING
//    assert(g_embed_file.size());
//    embedding->flush(g_embed_file.c_str());
//#endif
//
//    return true;
//}

bool AccAlign::fastq(const char *F1) {
    gzFile in1 = gzopen(F1, "rt");
    if (in1 == Z_NULL)
        return false;
    cerr << "Reading fastq file " << F1 << endl;

    // start CPU and GPU master threads, they consume reads from inputQ
    // dataQ is to re-use
    tbb::concurrent_bounded_queue < ReadCnt> inputQ;
    tbb::concurrent_bounded_queue < ReadCnt > outputQ;
    tbb::concurrent_bounded_queue < Read *> dataQ;
    thread gpu_threads[g_ngpus];
    for (int i = 0; i < g_ngpus; i++){
        gpu_threads[i] = thread(&AccAlign::gpu_root_fn, this,  &inputQ, &outputQ, i);
    }
//    thread cpu_thread = thread(&AccAlign::cpu_root_fn, this, &inputQ, &outputQ);
    thread out_thread = thread(&AccAlign::output_root_fn, this, &outputQ, &dataQ);

    struct timeval start, end, pstart, pend;
    gettimeofday(&start, NULL);

    int total_nreads = 0, nreads_per_vec = 0, vec_index = 0, vec_size = 50;

    gettimeofday(&pstart, NULL);
    Read *reads[vec_size];
    reads[vec_index] = new Read[GPU_BUFFER];
    gettimeofday(&pend, NULL);
    resizeTime += compute_elapsed(&pstart, &pend);

    while ((vec_index < vec_size) && (!gzeof(in1)) && (gzgetc(in1) != EOF)) {
        gettimeofday(&pstart, NULL);
        Read &r = *(reads[vec_index]+nreads_per_vec);
        in1 >> r;
        gettimeofday(&pend, NULL);
        inTime += compute_elapsed(&pstart, &pend);

        if (!strlen(r.seq)){
            break;
        }

	    ++nreads_per_vec;

        if (nreads_per_vec == GPU_BUFFER) {
            gettimeofday(&pstart, NULL);
            inputQ.push(make_tuple(reads[vec_index], GPU_BUFFER));
            gettimeofday(&pend, NULL);
            pushTime += compute_elapsed(&pstart, &pend);

            vec_index++;
            gettimeofday(&pstart, NULL);
            if (vec_index < vec_size){
                reads[vec_index] = new Read[GPU_BUFFER];
            }
            gettimeofday(&pend, NULL);
            resizeTime += compute_elapsed(&pstart, &pend);
            total_nreads += nreads_per_vec;
            nreads_per_vec = 0;
        }
    }

    Read *cur_vec = NULL;

    // the nb of reads is less than vec_size *GPU_BUFFER, and there are some reads not pushed to inputQ
    if (nreads_per_vec && vec_index < vec_size){
        // the remaining reads
        inputQ.push(make_tuple(reads[vec_index], nreads_per_vec));
        total_nreads += nreads_per_vec;
    }else{
        // still have reads not loaded
        gettimeofday(&pstart, NULL);
        dataQ.pop(cur_vec);
        gettimeofday(&pend, NULL);
        popTime += compute_elapsed(&pstart, &pend);

        while ((!gzeof(in1)) && (gzgetc(in1) != EOF)) {

            gettimeofday(&pstart, NULL);
            Read &r = *(cur_vec+nreads_per_vec);
            in1 >> r;
            gettimeofday(&pend, NULL);
            inTime += compute_elapsed(&pstart, &pend);

            if (!strlen(r.seq)){
                break;
            }

            ++nreads_per_vec;
            if (nreads_per_vec == GPU_BUFFER) {
                gettimeofday(&pstart, NULL);
                inputQ.push(make_tuple(cur_vec, GPU_BUFFER));
                gettimeofday(&pend, NULL);
                pushTime += compute_elapsed(&pstart, &pend);

                gettimeofday(&pstart, NULL);
                dataQ.pop(cur_vec);
                gettimeofday(&pend, NULL);
                popTime += compute_elapsed(&pstart, &pend);

                total_nreads += nreads_per_vec;
                nreads_per_vec = 0;
            }
        }

        // the remaining reads
        gettimeofday(&pstart, NULL);
        if (nreads_per_vec){
            total_nreads += nreads_per_vec;
            inputQ.push(make_tuple(cur_vec, nreads_per_vec));
        }
        gettimeofday(&pend, NULL);
        eraseTime += compute_elapsed(&pstart, &pend);
    }

    gzclose(in1);

    gettimeofday(&end, NULL);
    input_io_time += compute_elapsed(&start, &end);
    cerr << "done reading " << total_nreads << " reads from fastq file " << F1 << " in " <<
         compute_elapsed(&start, &end) / 1000000.0 << " secs\n";

    ReadCnt sentinel = make_tuple((Read *) NULL, 0);
    inputQ.push(sentinel);

    int size = vec_index < vec_size ? vec_index : vec_size;
    gettimeofday(&start, NULL);
    if (total_nreads % GPU_BUFFER == 0){
        //because the last popped cur_vec has not been pushed back
        size -= 1;
        delete[] cur_vec;
    }
    for (int i = 0; i < size; i++){
        dataQ.pop(cur_vec);
        delete[] cur_vec;
    }
    gettimeofday(&end, NULL);
    delTime += compute_elapsed(&start, &end);

//    cpu_thread.join();
    for (int i = 0; i < g_ngpus; i++) {
        gpu_threads[i].join();
    }

    outputQ.push(sentinel);
    out_thread.join();
//    del_thread.join();

    cerr << "Processed " << total_nreads << " in total \n";
    return true;
}

void AccAlign::del_root_fn(tbb::concurrent_bounded_queue<Read *> *dataQ, int size) {
    cerr << "Starting delete vectors" << endl;

    struct timeval start, end;
    gettimeofday(&start, NULL);
    for (int i = 0; i < size; i++){
        Read *cur_vec = NULL;
        dataQ->pop(cur_vec);
        delete[] cur_vec;
    }
    gettimeofday(&end, NULL);
    delTime += compute_elapsed(&start, &end);
}

void AccAlign::output_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *outputQ,
                              tbb::concurrent_bounded_queue<Read *> *dataQ) {
    cerr << "Extension and output function starting.." << endl;

    unsigned nreads = 0;
    tbb::concurrent_bounded_queue < ReadCnt > *targetQ = outputQ;
    do {
        ReadCnt gpu_reads;
        targetQ->pop(gpu_reads);
        nreads = std::get<1>(gpu_reads);
        if (!nreads) {
            targetQ->push(gpu_reads);   //put sentinel back
            break;
        }
        align_wrapper(0, 0, nreads, std::get<0>(gpu_reads), (Read *) NULL, dataQ);
    } while (1);

    cerr << "Extension and output function quitting...\n";
}

void AccAlign::gpu_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *inputQ,
                           tbb::concurrent_bounded_queue<ReadCnt> *outputQ, int gpu_id) {
    cerr << "GPU Root function starting.." << endl;

    unsigned nreads = 0, total = 0;
    tbb::concurrent_bounded_queue < ReadCnt > *targetQ = inputQ;
    do {
        ReadCnt gpu_reads;
        targetQ->pop(gpu_reads);
        nreads = std::get<1>(gpu_reads);
        total += nreads;
        if (!nreads) {
            targetQ->push(gpu_reads);   //put sentinel back
            break;
        }
        map_block_wrapper( 0, gpu_id, nreads, std::get<0>(gpu_reads), (Read *) NULL, outputQ);
    } while (1);

    cerr << "Processed " << total << " reads in GPU " << gpu_id <<" \n";
}

class Parallel_mapper {
    Read *all_reads;
    AccAlign *acc_obj;

public:
    Parallel_mapper(Read *_all_reads, AccAlign *_acc_obj) :
            all_reads(_all_reads), acc_obj(_acc_obj) {}

    void operator()(const tbb::blocked_range <size_t> &r) const {
        for (size_t i = r.begin(); i != r.end(); ++i) {
            acc_obj -> map_read(*(all_reads+i));
        }
    }
};

void AccAlign::cpu_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *inputQ,
                           tbb::concurrent_bounded_queue<ReadCnt> *outputQ) {
    cerr << "CPU Root function starting.." << endl;

    tbb::concurrent_bounded_queue < ReadCnt > *targetQ = inputQ;
    int nreads = 0, total = 0;
    do {
        ReadCnt cpu_readcnt;
        targetQ->pop(cpu_readcnt);
        Read *reads = std::get<0>(cpu_readcnt);
        nreads = std::get<1>(cpu_readcnt);
        total += nreads;
        if (nreads == 0) {
            inputQ->push(cpu_readcnt);    // push sentinel back
            break;
        }

        tbb::task_scheduler_init init(g_ncpus);
        tbb::parallel_for(tbb::blocked_range<size_t>(0, nreads), Parallel_mapper(reads, this));

        outputQ->push(cpu_readcnt);
    }while(1);

    cerr << "Processed " << total << " reads in cpu \n";
    cerr << "CPU Root function quitting.." << endl;
}

void AccAlign::mark_for_extension(Read &read, char S, Region &cregion) {
    int rlen = strlen(read.seq);

//    if (cregion.cov >= ap.best_cov) {
//        if (cregion.cov > ap.best_cov) {
//            ap.next_cov = ap.best_cov;
//            ap.best_cov = cregion.cov;
//            ap.nbest = 0;
//        }
//
//        ++ap.nbest;
//
//        /*
//         * XXX: The following code filters regions further based on the nr
//         * of hits. Technically, it is possible for a true hit to be missed
//         * because a substitution led to some other false region with more
//         * coverage. This is the very core of seed and vote.
//         * So removing this as this can lose accuracy.
//
//         cregion.beg = cregion.pos > MAX_INDEL ? cregion.pos - MAX_INDEL : 0;
//         cregion.end = cregion.pos + rlen + MAX_INDEL < ref.size() ?
//         cregion.pos + rlen + MAX_INDEL : ref.size();
//         cregion.str = S;
//         cregion.is_exact = false;
//         ap.best_regions.push_back(cregion);
//         */
//    } else if (cregion.cov > ap.next_cov) {
//        ap.next_cov = cregion.cov;
//    }

#if DEFAULT_ALIGNER
    cregion.beg = cregion.pos > MAX_INDEL ? cregion.pos - MAX_INDEL : 0;
    cregion.end = cregion.pos + rlen + MAX_INDEL < ref.size() ?
        cregion.pos + rlen + MAX_INDEL : ref.size();
#else
    cregion.beg = cregion.pos;
    cregion.end = cregion.pos + rlen < ref.size() ? cregion.pos + rlen :
                  ref.size();
#endif
    cregion.str = S;
    cregion.is_exact = cregion.is_aligned = false;
    read.best_region = cregion;
}

void AccAlign::lsh_filter(char *Q, size_t rlen,
                          vector<Region> &candidate_regions,
                          int &best_threshold, int &next_threshold,
                          int &best_idx, int &next_idx) {
    const char **candidate_refs;
    unsigned ncandidates = candidate_regions.size();

    if (!ncandidates)
        return;

    // alloc space
    candidate_refs = new const char *[ncandidates + 1];
    candidate_refs[0] = Q;

    // add all remaining candidates for embedding
    const char *ptr_ref = ref.c_str();
    for (unsigned i = 0; i < ncandidates; ++i) {
        Region &r = candidate_regions[i];
        candidate_refs[i + 1] = ptr_ref + r.pos;
    }

    // now do embedding and lsh
//    struct timeval start, end;
//    gettimeofday(&start, NULL);
    embedding->embeddata(candidate_regions, candidate_refs, ncandidates + 1,
                         rlen, best_threshold, next_threshold, true, best_idx, next_idx);
//    gettimeofday(&end, NULL);
//    embedding_time += compute_elapsed(&start, &end);

    delete[] candidate_refs;
}

void AccAlign::pigeonhole_query(char *Q, size_t rlen, vector<Region> &candidate_regions,
                                char S, int err_threshold, int &best) {
    struct timeval start, end;
    int max_cov = 0;

    // Take non-overlapping seeds and find all hits
    gettimeofday(&start, NULL);
    int kmer_window = kmer_len + kmer_step - 1;
    int nwindows = rlen / kmer_window;
    int nkmers = nwindows * kmer_step;
    size_t ntotal_hits = 0;
    size_t b[nkmers], e[nkmers];
    int kmer_idx = 0;
    for (size_t i = 0; i + kmer_window <= rlen; i += kmer_window) {
        for (int step = 0; step < kmer_step; step++) {
            uint64_t k = 0;
            for (size_t j = i + step; j < i + kmer_len + step; j++)
                k = (k << 2) + *(Q+j);
            size_t hash = (k & mask) % MOD;
            b[kmer_idx] = keyv[hash];
            e[kmer_idx] = keyv[hash + 1];
            ntotal_hits += (e[kmer_idx] - b[kmer_idx]);
            kmer_idx++;
        }
    }
    assert(kmer_idx == nkmers);
    gettimeofday(&end, NULL);
    probe_time += compute_elapsed(&start, &end);

    // if we have no hits, we are done
    if (!ntotal_hits)
        return;

    gettimeofday(&start, NULL);
    uint32_t top_pos[nkmers];
    int step_off[nkmers], rel_off[nkmers];
    uint32_t MAX_POS = numeric_limits<uint32_t>::max();

    // initialize top values with first values for each kmer.
    for (int i = 0; i < nkmers; i++) {
        if (b[i] < e[i]) {
            top_pos[i] = posv[b[i]];
            step_off[i] = i % kmer_step;
            rel_off[i] = (i / kmer_step) * kmer_window;
            top_pos[i] -= (rel_off[i] + step_off[i]);
        } else {
            top_pos[i] = MAX_POS;
        }
    }

    size_t nprocessed = 0;
    uint32_t last_pos = MAX_POS;
    int last_cov = 0;

    while (nprocessed < ntotal_hits) {
        //find min
        uint32_t *min_item = min_element(top_pos, top_pos + nkmers);
        uint32_t min_pos = *min_item;
        int min_kmer = min_item - top_pos;

        // kick off prefetch for next round
        __builtin_prefetch(posv + b[min_kmer] + 1);

        // if previous min element was same as current one, increment coverage.
        // otherwise, check if last min element's coverage was high enough to
        // make it a candidate region
        if (min_pos == last_pos) {
            last_cov++;
        } else {
            if (last_cov >= err_threshold) {
                Region r;
                r.cov = last_cov;
                r.pos = last_pos;
                //cerr << "Adding " << last_pos << " with cov " << r.cov <<
                //    " as candidate for dir " << S << endl;
                assert(r.pos != MAX_POS && r.pos < MAX_POS);

                // add high coverage regions up front so that we dont have to
                // sort later.
                // NOTE: Inserting at vector front is crazy expensive. If we
                // simply change > max_cov to >=max_cov, timing will blow up.
                // Just keep it > max_cov as we are anyway interested in only
                // the region with max cov, and we need to embed all regions any
                // way.
                if (last_cov > max_cov) {
                    max_cov = last_cov;
                    best = candidate_regions.size();
                    //candidate_regions.insert(candidate_regions.begin(), r);
                } //else {
                candidate_regions.push_back(r);
                //}
            }
            last_cov = 1;
        }
        last_pos = min_pos;

        // add next element
        b[min_kmer]++;
        uint32_t next_pos = b[min_kmer] < e[min_kmer] ? posv[b[min_kmer]] : MAX_POS;
        if (next_pos != MAX_POS)
            *min_item = next_pos - (rel_off[min_kmer] + step_off[min_kmer]);
        else
            *min_item = MAX_POS;
        ++nprocessed;
    }

    // we will have the last few positions not processed. check here.
    if (last_cov >= err_threshold && last_pos != MAX_POS) {
        Region r;
        r.cov = last_cov;
        r.pos = last_pos;
        if (last_cov > max_cov) {
            max_cov = last_cov;
            best = candidate_regions.size();
            //candidate_regions.insert(candidate_regions.begin(), r);
        } //else {
        candidate_regions.push_back(r);
        //}
    }
    gettimeofday(&end, NULL);
    hit_count_time += compute_elapsed(&start, &end);

    //cout << "Found " << candidate_regions.size() << " regions for direction " << S << endl;
//    for (Region &r : candidate_regions)
//        cout << "posn: " << r.pos << " and cov " << r.cov << endl;
}

/*
 * This function implements normal querying that assumes step size is 1. So
 * every seed is taken into consideration. The one where step size is > 1
 * requires special application of pigeonhole. That is done by pigeonhole_query.
 */
void AccAlign::query(string &Q, vector<Region> &candidate_regions, char S) {
    vector<uint32_t> hit;
    hit.reserve(1024);
    uint64_t k = 0;
    struct timeval start, end;

    for (int i = 0; i < kmer_len - 1; i++) {
        k = (k << 2) + Q[i];
    }

    gettimeofday(&start, NULL);
    for (size_t i = kmer_len - 1; i < Q.size(); i++) {
        k = (k << 2) + Q[i];
        size_t hash = (k & mask) % MOD;
        size_t b = keyv[hash];
        size_t e = keyv[hash + 1];

        for (size_t j = b; j < e; j++)
            hit.push_back(posv[j] + kmer_len > 1 + i ? posv[j] + kmer_len - 1 - i : 0);
    }
    gettimeofday(&end, NULL);
    probe_time += compute_elapsed(&start, &end);

    gettimeofday(&start, NULL);
    cerr << "Direction " << S << " found " << hit.size() << " hits." << endl;
    radix_sort(&hit[0], &hit[0] + hit.size());
    gettimeofday(&end, NULL);
    vpair_sort_time += compute_elapsed(&start, &end);

    gettimeofday(&start, NULL);
    int ERR_THRESHOLD = 2;
    for (size_t i = 0; i < hit.size();) {
        size_t j;
        for (j = i + 1; j < hit.size() && hit[j] == hit[i]; j++);

        if ((int) (j - i) >= ERR_THRESHOLD) {
            Region r;
            r.cov = j - i;
            r.pos = hit[i];
            candidate_regions.push_back(r);
        }

        i = j;
    }
    gettimeofday(&end, NULL);
    vpair_build_time += compute_elapsed(&start, &end);

    //cerr << "Direction " << S << " found " << candidate_regions.size() << " candidate regions. " << endl;

    //lsh_filter(Q, ap, S, ispe, candidate_regions);
}

void AccAlign::pghole_wrapper(Read &R, vector<Region> &fcandidate_regions,
                              vector<Region> &rcandidate_regions, int &fbest, int &rbest) {
    size_t rlen = strlen(R.seq);
    pigeonhole_query(R.fwd, rlen, fcandidate_regions, '+', 2, fbest);
    pigeonhole_query(R.rev, rlen, rcandidate_regions, '-', 2, rbest);
    unsigned nfregions = fcandidate_regions.size();
    unsigned nrregions = rcandidate_regions.size();

    // if we don't find any regions, try lowering the error threshold
    if (!nfregions && !nrregions) {
        pigeonhole_query(R.fwd, rlen, fcandidate_regions, '+', 1, fbest);
        pigeonhole_query(R.rev, rlen, rcandidate_regions, '-', 1, rbest);
    }
}

void AccAlign::embed_wrapper(Read &R, bool ispe,
                             vector<Region> &fcandidate_regions, vector<Region> &rcandidate_regions,
                             int &fbest, int &fnext, int &rbest, int &rnext) {
    unsigned nfregions = fcandidate_regions.size();
    unsigned nrregions = rcandidate_regions.size();
    static int nunique = 0, nnon_unique = 0;

    // for single-end mapping, we better have things sorted by coverage. for
    // paired end, it is possible for a region with high cov to be eliminated if
    // no pairs are found
    if (!ispe && nfregions > 1)
        assert(fcandidate_regions[0].cov >= fcandidate_regions[1].cov);
    if (!ispe && nrregions > 1)
        assert(rcandidate_regions[0].cov >= rcandidate_regions[1].cov);

    // pass the one with the highest coverage in first
    bool fwd_first = true;
    vpair_sort_count += nfregions + nrregions;
    if (nfregions > 1 && nrregions > 1) {
        if (fcandidate_regions[0].cov < rcandidate_regions[0].cov) {
            fwd_first = false;
        }
    }

    // embed now, but only in the case where we have > 1 regions either for the
    // forward or for reverse or for both strands. If we have only 1 region
    // globally, there is no point embedding.
    size_t rlen = strlen(R.seq);
    int best_threshold = rlen * embedding->efactor;
    int next_threshold = rlen * embedding->efactor;

    if ((nfregions == 0) || (nrregions == 0)) {
        fbest = fnext = rbest = rnext = 0;
        if (nfregions) {
            lsh_filter(R.fwd, rlen, fcandidate_regions, best_threshold, next_threshold, fbest, fnext);
        }
        if (nrregions) {
            lsh_filter(R.rev, rlen, rcandidate_regions, best_threshold, next_threshold, rbest, rnext);
        }
        ++nunique;
    } else {
        ++nnon_unique;
        if (fwd_first) {
            lsh_filter(R.fwd, rlen, fcandidate_regions, best_threshold, next_threshold, fbest, fnext);
            lsh_filter(R.rev, rlen, rcandidate_regions, best_threshold, next_threshold, rbest, rnext);
        } else {
            lsh_filter(R.rev, rlen, rcandidate_regions, best_threshold, next_threshold, rbest, rnext);
            lsh_filter(R.fwd, rlen, fcandidate_regions, best_threshold, next_threshold, fbest, fnext);
        }
    }

//#if DBGPRINT
//    cerr << "Read " << R.name << " (uniq/non=" << nunique << "/" << nnon_unique
//        << ")" << " efactor " << embedding->efactor <<
//        " Thresholds:" << best_threshold << ":" <<
//        next_threshold << ":" << endl;
//#endif
}

int AccAlign::get_mapq(int best, int secbest, bool hasSecbest, int rlen) {
    best = MIS_PENALTY * best;
    secbest = MIS_PENALTY * secbest;

    int64_t scMin = -0.6 + -0.6 * rlen;
    int64_t diff = abs(scMin);
    int64_t bestOver = best - scMin;
    int ret;

    if (!hasSecbest) {
        if (bestOver >= diff * (double) 0.9f) ret = 42;
        else if (bestOver >= diff * (double) 0.8f) ret = 40;
        else if (bestOver >= diff * (double) 0.7f) ret = 24;
        else if (bestOver >= diff * (double) 0.6f) ret = 23;
        else if (bestOver >= diff * (double) 0.5f) ret = 8;
        else if (bestOver >= diff * (double) 0.4f) ret = 3;
        else ret = 0;
    } else {
        int64_t bestdiff = abs(abs(static_cast<long>(best)) - abs(static_cast<long>(secbest)));
        if (bestdiff >= diff * (double) 0.9f) {
            if (bestOver == diff) {
                ret = 39;
            } else {
                ret = 33;
            }
        } else if (bestdiff >= diff * (double) 0.8f) {
            if (bestOver == diff) {
                ret = 38;
            } else {
                ret = 27;
            }
        } else if (bestdiff >= diff * (double) 0.7f) {
            if (bestOver == diff) {
                ret = 37;
            } else {
                ret = 26;
            }
        } else if (bestdiff >= diff * (double) 0.6f) {
            if (bestOver == diff) {
                ret = 36;
            } else {
                ret = 22;
            }
        } else if (bestdiff >= diff * (double) 0.5f) {
            // Top third is still pretty good
            if (bestOver == diff) {
                ret = 35;
            } else if (bestOver >= diff * (double) 0.84f) {
                ret = 25;
            } else if (bestOver >= diff * (double) 0.68f) {
                ret = 16;
            } else {
                ret = 5;
            }
        } else if (bestdiff >= diff * (double) 0.4f) {
            // Top third is still pretty good
            if (bestOver == diff) {
                ret = 34;
            } else if (bestOver >= diff * (double) 0.84f) {
                ret = 21;
            } else if (bestOver >= diff * (double) 0.68f) {
                ret = 14;
            } else {
                ret = 4;
            }
        } else if (bestdiff >= diff * (double) 0.3f) {
            // Top third is still pretty good
            if (bestOver == diff) {
                ret = 32;
            } else if (bestOver >= diff * (double) 0.88f) {
                ret = 18;
            } else if (bestOver >= diff * (double) 0.67f) {
                ret = 15;
            } else {
                ret = 3;
            }
        } else if (bestdiff >= diff * (double) 0.2f) {
            // Top third is still pretty good
            if (bestOver == diff) {
                ret = 31;
            } else if (bestOver >= diff * (double) 0.88f) {
                ret = 17;
            } else if (bestOver >= diff * (double) 0.67f) {
                ret = 11;
            } else {
                ret = 0;
            }
        } else if (bestdiff >= diff * (double) 0.1f) {
            // Top third is still pretty good
            if (bestOver == diff) {
                ret = 30;
            } else if (bestOver >= diff * (double) 0.88f) {
                ret = 12;
            } else if (bestOver >= diff * (double) 0.67f) {
                ret = 7;
            } else {
                ret = 0;
            }
        } else if (bestdiff > 0) {
            // Top third is still pretty good
            if (bestOver >= diff * (double) 0.67f) {
                ret = 6;
            } else {
                ret = 2;
            }
        } else {
            assert(bestdiff == 0);
            // Top third is still pretty good
            if (bestOver >= diff * (double) 0.67f) {
                ret = 1;
            } else {
                ret = 0;
            }
        }
    }
    return ret;
}

void AccAlign::set_mapq(Read &R, vector<Region> &fcandidate_regions,
                        vector<Region> &rcandidate_regions, int fbest, int fnext,
                        int rbest, int rnext) {
    int len = embedding->efactor * strlen(R.seq);

    unsigned nfregions = fcandidate_regions.size();
    unsigned nrregions = rcandidate_regions.size();
    if (nfregions == 0) {
        if (nrregions == 0)
            return;
        int best_score = rcandidate_regions[rbest].embed_dist;
        if (nrregions > 1) {
            int next_score = rcandidate_regions[rnext].embed_dist;
            R.mapq = get_mapq(best_score, next_score, true, len);
        } else {
            //only 1 alignment
            R.mapq = get_mapq(best_score, 0, false, len);
        }
    } else if (nrregions == 0) {
        int best_score = fcandidate_regions[fbest].embed_dist;
        if (nfregions > 1) {
            int next_score = fcandidate_regions[fnext].embed_dist;
            R.mapq = get_mapq(best_score, next_score, true, len);
        } else {
            // only 1 alignment
            R.mapq = get_mapq(best_score, 0, false, len);
        }
    } else {
        // we have atleast 1 forward and 1 reverse candidate
        if (fcandidate_regions[fbest].embed_dist <
            rcandidate_regions[rbest].embed_dist) {
            int second_best = rcandidate_regions[rbest].embed_dist;
            if (nfregions > 1 && fcandidate_regions[fnext].embed_dist < second_best)
                second_best = fcandidate_regions[fnext].embed_dist;
            R.mapq = get_mapq(fcandidate_regions[fbest].embed_dist, second_best, true, len);
        } else {
            int second_best = fcandidate_regions[fbest].embed_dist;
            if (nrregions > 1 && rcandidate_regions[rnext].embed_dist < second_best)
                second_best = rcandidate_regions[rnext].embed_dist;
            R.mapq = get_mapq(rcandidate_regions[rbest].embed_dist, second_best, true, len);
        }
    }
}

void AccAlign::map_read(Read &R) {
    struct timeval start, end;

//#if ENABLE_GPU
//    ;
//#else
    gettimeofday(&start, NULL);
    parse(R.seq, R.fwd, R.rev, R.rev_str);
    gettimeofday(&end, NULL);
    parse_time += compute_elapsed(&start, &end);
//#endif

    gettimeofday(&start, NULL);
    vector<Region> fcandidate_regions, rcandidate_regions;

    // first try pigenhole. if we generate any candidates, pass them through
    // lsh. If something comes out, we are done.
    // XXX: On experimentation, it was found that using pigeonhole filtering
    // produces wrong results and invalid mappings when errors are too large.
    int fbest, rbest;
    pghole_wrapper(R, fcandidate_regions, rcandidate_regions, fbest, rbest);
    unsigned nfregions = fcandidate_regions.size();
    unsigned nrregions = rcandidate_regions.size();
    gettimeofday(&end, NULL);
    seeding_time += compute_elapsed(&start, &end);

//     f += nfregions;
//     r += nrregions;
//     cout  << f << "," << r << endl;
//     cerr << R.name << "," << nfregions <<"," << nrregions << endl;

    gettimeofday(&start, NULL);
    // before doing embedding, move highest cov region to front
    if (nfregions > 1 && fbest != 0) {
        iter_swap(fcandidate_regions.begin() + fbest,
                  fcandidate_regions.begin());
    }
    if (nrregions > 1 && rbest != 0) {
        iter_swap(rcandidate_regions.begin() + rbest,
                  rcandidate_regions.begin());
    }
    gettimeofday(&end, NULL);
    swap_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);

    int fnext, rnext;
    gettimeofday(&start, NULL);
    embed_wrapper(R, false, fcandidate_regions, rcandidate_regions,
                  fbest, fnext, rbest, rnext);
    gettimeofday(&end, NULL);
    embedding_time += compute_elapsed(&start, &end);

    //cerr << "Read " << R.name << " has f/r=" << fcandidate_regions.size() <<
    //    "/" << rcandidate_regions.size() << " and embedding took " <<
    //    compute_elapsed(&start, &end) / 1000000.0 << " secs." << endl;

    gettimeofday(&start, NULL);
    set_mapq(R, fcandidate_regions, rcandidate_regions, fbest, fnext,
             rbest, rnext);
    if (nfregions == 0) {
        if (nrregions == 0)
            return;
        mark_for_extension(R, '-', rcandidate_regions[rbest]);
    } else if (nrregions == 0) {
        mark_for_extension(R, '+', fcandidate_regions[fbest]);
    } else {
        // pick the candidate with smallest embed dist
        // if fwd/rev have same embed_dist, take the hcov one
        // if hcov one not the min dist, take the one with smaller pos (to be consistent with gpu)
        if (fcandidate_regions[fbest].embed_dist < rcandidate_regions[rbest].embed_dist){
            mark_for_extension(R, '+', fcandidate_regions[fbest]);
        } else if (fcandidate_regions[fbest].embed_dist > rcandidate_regions[rbest].embed_dist){
            mark_for_extension(R, '-', rcandidate_regions[rbest]);
        }else {
            //if (fbest == 0 && rbest != 0)
            //    mark_for_extension(R, '+', fcandidate_regions[fbest]);
            //else if (fbest != 0 && rbest == 0)
            //    mark_for_extension(R, '-', rcandidate_regions[rbest]);
            if (fcandidate_regions[fbest].pos < rcandidate_regions[rbest].pos)
                mark_for_extension(R, '+', fcandidate_regions[fbest]);
            else
                mark_for_extension(R, '-', rcandidate_regions[rbest]);
        }
    }

    gettimeofday(&end, NULL);
    mapqTime += compute_elapsed(&start, &end);
}

void AccAlign::map_paired_read(Read &mate1, Read &mate2) {
    struct timeval start, end;

    gettimeofday(&start, NULL);
    parse(mate1.seq, mate1.fwd, mate1.rev, mate1.rev_str);
    parse(mate2.seq, mate2.fwd, mate2.rev, mate2.rev_str);
    gettimeofday(&end, NULL);
    parse_time += compute_elapsed(&start, &end);

    // lookup candidates
    vector<Region> fcandidate_regions1, rcandidate_regions1;
    int fbest1, rbest1, fbest2, rbest2;
    pghole_wrapper(mate1, fcandidate_regions1, rcandidate_regions1,
                   fbest1, rbest1);
    unsigned nfregions1 = fcandidate_regions1.size();
    unsigned nrregions1 = rcandidate_regions1.size();
    vector<Region> fcandidate_regions2, rcandidate_regions2;
    pghole_wrapper(mate2, fcandidate_regions2, rcandidate_regions2,
                   fbest2, rbest2);
    unsigned nfregions2 = fcandidate_regions2.size();
    unsigned nrregions2 = rcandidate_regions2.size();

    // filter based on pairdis
    // Note that after filtering, regions wont be sorted based on coverage. They
    // will instead be sorted based on position. pairdis filter needs this
    // ordering so that it can adopt some shortcuts.
    gettimeofday(&start, NULL);
    vector<Region> filtered_fregions1, filtered_rregions2;
    pairdis_filter(fcandidate_regions1, rcandidate_regions2,
                   filtered_fregions1, filtered_rregions2, fbest1, rbest2);
    nfregions1 = filtered_fregions1.size();
    nrregions2 = filtered_rregions2.size();
    vector<Region> filtered_rregions1, filtered_fregions2;
    pairdis_filter(rcandidate_regions1, fcandidate_regions2,
                   filtered_rregions1, filtered_fregions2, rbest1, fbest2);
    nfregions2 = filtered_fregions2.size();
    nrregions1 = filtered_rregions1.size();
    gettimeofday(&end, NULL);
    vpair_build_time += compute_elapsed(&start, &end);

    // now apply embedding filter on filtered regions.
    // But before, rearrange so that regions with high coverage are at the top
    int fnext1, rnext1;
    if (nfregions1 || nrregions1) {
        int fbest1_tmp = 0, rbest1_tmp = 0;
        if (nfregions1 > 1 && fbest1 != 0) {
            fbest1_tmp = fbest1;
            iter_swap(filtered_fregions1.begin() + fbest1, filtered_fregions1.begin());
        }
        if (nrregions1 > 1 && rbest1 != 0) {
            rbest1_tmp = rbest1;
            iter_swap(filtered_rregions1.begin() + rbest1, filtered_rregions1.begin());
        }
        embed_wrapper(mate1, true, filtered_fregions1, filtered_rregions1,
                      fbest1, fnext1, rbest1, rnext1);

        //swap back to keep filtered_regions sorted by pos
        if (nfregions1 > 1) {
            iter_swap(filtered_fregions1.begin() + fbest1_tmp, filtered_fregions1.begin());
        }
        if (nrregions1 > 1) {
            iter_swap(filtered_rregions1.begin() + rbest1_tmp, filtered_rregions1.begin());
        }
    }

    int fnext2, rnext2;
    if (nfregions2 || nrregions2) {
        int fbest2_tmp = 0, rbest2_tmp = 0;
        if (nfregions2 > 1 && fbest2 != 0) {
            fbest2_tmp = fbest2;
            iter_swap(filtered_fregions2.begin() + fbest2, filtered_fregions2.begin());
        }
        if (nrregions2 > 1 && rbest2 != 0) {
            rbest2_tmp = rbest2;
            iter_swap(filtered_rregions2.begin() + rbest2, filtered_rregions2.begin());
        }
        embed_wrapper(mate2, true, filtered_fregions2, filtered_rregions2,
                      fbest2, fnext2, rbest2, rnext2);

        //swap back to keep filtered_regions sorted by pos
        if (nfregions2 > 1) {
            iter_swap(filtered_fregions2.begin() + fbest2_tmp, filtered_fregions2.begin());
        }
        if (nrregions2 > 1) {
            iter_swap(filtered_rregions2.begin() + rbest2_tmp, filtered_rregions2.begin());
        }
    }

    // finally, pick regions for further extension. here, we make use of pairing
    // again by first checking overall, across mate1 and mate2, who has lowest
    // embed dist. then, we use the fwd or rev strand from that mate as the
    // deciding factor. if we chose fwd for that mate, we pick rev for other
    // mate, and vice versa.
    gettimeofday(&start, NULL);

    int min_sum_embed_dist1 = INT_MAX;
    int min_sum_embed_dist2 = INT_MAX;
    Region best_fregion, best_rregion;

    if (nfregions1 && nrregions2) {
        for (unsigned i = 0; i < nfregions1; i++) {
            Region tmp;
            tmp.pos = filtered_fregions1[i].pos - pairdis;
            auto start = lower_bound(filtered_rregions2.begin(), filtered_rregions2.end(), tmp,
                                     [](const Region &left, const Region &right) {
                                         return left.pos < right.pos;
                                     }
            );

            tmp.pos = filtered_fregions1[i].pos + pairdis;
            auto end = upper_bound(filtered_rregions2.begin(), filtered_rregions2.end(), tmp,
                                   [](const Region &left, const Region &right) {
                                       return left.pos < right.pos;
                                   }
            );

            for (auto itr = start; itr != end; ++itr) {
                int sum_embed_dist = filtered_fregions1[i].embed_dist + itr->embed_dist;
                if (sum_embed_dist < min_sum_embed_dist1) {
                    min_sum_embed_dist1 = sum_embed_dist;
                    best_fregion = filtered_fregions1[i];
                    best_rregion = *itr;
                }
            }
        }
    }

    if (nrregions1 && nfregions2) {
        for (unsigned i = 0; i < nrregions1; i++) {
            Region tmp;
            tmp.pos = filtered_rregions1[i].pos - pairdis;
            auto start = lower_bound(filtered_fregions2.begin(), filtered_fregions2.end(), tmp,
                                     [](const Region &left, const Region &right) {
                                         return left.pos < right.pos;
                                     }
            );

            tmp.pos = filtered_rregions1[i].pos + pairdis;
            auto end = upper_bound(filtered_fregions2.begin(), filtered_fregions2.end(), tmp,
                                   [](const Region &left, const Region &right) {
                                       return left.pos < right.pos;
                                   }
            );

            for (auto itr = start; itr != end; ++itr) {
                int sum_embed_dist = filtered_rregions1[i].embed_dist + itr->embed_dist;
                if (sum_embed_dist < min_sum_embed_dist1 && sum_embed_dist < min_sum_embed_dist2) {
                    min_sum_embed_dist2 = sum_embed_dist;
                    best_fregion = *itr;
                    best_rregion = filtered_rregions1[i];
                }
            }
        }
    }

    if (min_sum_embed_dist1 < INT_MAX || min_sum_embed_dist2 < INT_MAX) { //in case there are no candidates
        if (min_sum_embed_dist1 <= min_sum_embed_dist2) {
            mark_for_extension(mate1, '+', best_fregion);
            mark_for_extension(mate2, '-', best_rregion);
        } else {
            mark_for_extension(mate2, '+', best_fregion);
            mark_for_extension(mate1, '-', best_rregion);
        }
    }

    set_mapq(mate1, filtered_fregions1, filtered_rregions1,
             fbest1, fnext1, rbest1, rnext1);
    set_mapq(mate2, filtered_fregions2, filtered_rregions2,
             fbest2, fnext2, rbest2, rnext2);

    gettimeofday(&end, NULL);
    lsh_time += compute_elapsed(&start, &end);
}


void AccAlign::cpu_embed_map_block(vector<Read> *ptlread, int soff, int eoff) {
    struct timeval start, end;
    gettimeofday(&start, NULL);
    vector<Read> &tlread = *ptlread;
    for (int i = soff; i < eoff; i++) {
        map_read(tlread[i]);
        align_read(tlread[i]);
        print_sam(tlread[i]);
    }
    gettimeofday(&end, NULL);
    float time = compute_elapsed(&start, &end);
    cerr << "CPU finished map block on " << eoff - soff << " reads in " << time / 1000000 << " secs." << endl;
}

void AccAlign::gpu_enabled_map_block(int gpu_id, Read *ptlread, int nreads,
        tbb::concurrent_bounded_queue<ReadCnt> *outputQ) {
#if ENABLE_GPU
    struct timeval start, end;
    GPUMemMgr &my_mgr = g_memmgr[gpu_id];
    int kmer_window = kmer_len + kmer_step - 1;

    gettimeofday(&start, NULL);
    //take the shortest read in the batch
    int rlen = MAX_LEN;
    for (int i = 0; i < nreads; ++i) {
        int len = strlen((ptlread+i)->seq);
        rlen = rlen < len ? rlen : len;
    }
    gettimeofday(&end, NULL);
    min_len_time += compute_elapsed(&start, &end);

    int nwindows = rlen / kmer_window;
    int nseeds_per_strand = nwindows * kmer_step;
    int nseeds_per_read = nseeds_per_strand * 2;
    int total_nseeds = nreads * nseeds_per_read;
    int max_hamming = rlen * EMBED_FACTOR;

    // lock gpu mutex before operation.
    g_gpu_mutex[gpu_id].lock();

    // parse, convert ACGT/acgt => 0123
    gettimeofday(&start, NULL);
    char *d_fParsed, *d_rParsed, *d_rev_str;
    vector<const char *> read_queue;
    for (int i = 0; i < nreads; ++i) {
        read_queue.push_back((*(ptlread+i)).seq);
    }
    gpu_parse(gpu_id, read_queue, code, rcsymbol, rlen, &d_fParsed, &d_rParsed, &d_rev_str);
    gettimeofday(&end, NULL);
    parse_time += compute_elapsed(&start, &end);

    // get the seeds and hash, and embed reads,
    // do together so that d_fParsed/d_rParsed could be freed
    gettimeofday(&start, NULL);
    char *d_fQembed, *d_rQembed;
    size_t *d_fseed_hash, *d_rseed_hash;
    gpu_seedhash_and_embedread(gpu_id, d_fParsed, nreads, rlen, total_nseeds/2, nwindows, kmer_window, &d_fseed_hash, embedding, &d_fQembed);
    gpu_seedhash_and_embedread(gpu_id, d_rParsed, nreads, rlen, total_nseeds/2, nwindows, kmer_window, &d_rseed_hash, embedding, &d_rQembed);
    gettimeofday(&end, NULL);
    hit_count_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);

    // perform lookup now to get candidates
    gettimeofday(&start, NULL);
    int total_fhits = 0, total_rhits = 0;
    uint64_t *d_fhits = NULL, *d_rhits = NULL;
    gpu_lookup(this, gpu_id, d_fseed_hash, d_rseed_hash, total_nseeds, nreads, rlen,
            total_fhits, total_rhits, &d_fhits, &d_rhits);
    gettimeofday(&end, NULL);
    probe_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);
    cerr << "gpu lookup found " << total_fhits << " fhits and " << total_rhits << " rhits" << endl;

    // sort hits to group candidates of reads together
    gettimeofday(&start, NULL);
    vpair_sort_count += total_fhits + total_rhits;
    gpu_sort(gpu_id, d_fhits, total_fhits);
    cerr << "fwd sorted" << endl;
    gpu_sort(gpu_id, d_rhits, total_rhits);
    cerr << "rev sorted" << endl;
    gettimeofday(&end, NULL);
    vpair_sort_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);

    // get candidates
    gettimeofday(&start, NULL);
    uint64_t *d_uniq_fhits = NULL, *d_fhit_counts = NULL, *d_uniq_rhits = NULL, *d_rhit_counts = NULL;
    uint64_t *d_hcov_fcounts = NULL, *d_hcov_fhits = NULL, *d_hcov_rcounts = NULL, *d_hcov_rhits = NULL;
    int nhcov_fhits = 0, nhcov_rhits = 0;
    int unique_fhits = 0, unique_rhits = 0;
    // Get all unique hits and hits' count
    // Get the number of unique hits overall
    // Get the hit and the hit's count with highest coverage of each read
    // Get the number of reads containing at least one hit fwd/rev
    gpu_dedup_and_prioritize(gpu_id, d_fhits, total_fhits, &d_uniq_fhits, &d_fhit_counts, unique_fhits,
            &d_hcov_fhits, &d_hcov_fcounts, nreads, nhcov_fhits);
    cerr << "fwd dedup" << endl;
    gpu_dedup_and_prioritize(gpu_id, d_rhits, total_rhits, &d_uniq_rhits, &d_rhit_counts, unique_rhits,
            &d_hcov_rhits, &d_hcov_rcounts, nreads, nhcov_rhits);
    cerr << "rev dedup" << endl;
    my_mgr.free(d_fhits);
    my_mgr.free(d_rhits);
    gettimeofday(&end, NULL);
    vpair_build_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);
    cerr << "unique candidates: " << unique_fhits << " fhits and " << unique_rhits << " rhits" << endl;

    //remove the hit with cov 1 while this read's hcov > 1
    gettimeofday(&start, NULL);
    bool *d_over_one_hit = NULL;
    gpu_over_one_hit(gpu_id, d_hcov_fcounts, d_hcov_rcounts, nreads, &d_over_one_hit);
    gpu_remove_hits(gpu_id, d_uniq_fhits, d_fhit_counts, unique_fhits, d_over_one_hit);
    gpu_remove_hits(gpu_id, d_uniq_rhits, d_rhit_counts, unique_rhits, d_over_one_hit);
    //hcov
    gpu_remove_hits(gpu_id, d_hcov_fhits, d_hcov_fcounts, nhcov_fhits, d_over_one_hit);
    gpu_remove_hits(gpu_id, d_hcov_rhits, d_hcov_rcounts, nhcov_rhits, d_over_one_hit);
    my_mgr.free(d_over_one_hit);
    my_mgr.free(d_hcov_fcounts);
    my_mgr.free(d_hcov_rcounts);
    my_mgr.free(d_fhit_counts);
    my_mgr.free(d_rhit_counts);
    cerr << "gpu gphole " << unique_fhits << " fhits and " << unique_rhits << " rhits" << endl;
    gettimeofday(&end, NULL);
    pg_vote_time += compute_elapsed(&start, &end);
    seeding_time += compute_elapsed(&start, &end);

    // embed and find hamming distance of hcov hits
    gettimeofday(&start, NULL);
    uint64_t *d_hcov_fhamming, *d_hcov_rhamming;
    cout << "embed and find hamming distance of [fwd] hcov hits" << endl;
    gpu_merged_embed_hamming(gpu_id, d_ref, embedding, rlen, d_hcov_fhits, nhcov_fhits, d_fQembed, nreads, NULL, &d_hcov_fhamming);
    cout << "embed and find hamming distance of [rev] hcov hits" << endl;
    gpu_merged_embed_hamming(gpu_id, d_ref, embedding, rlen, d_hcov_rhits, nhcov_rhits, d_rQembed, nreads, NULL, &d_hcov_rhamming);
    my_mgr.free(d_hcov_fhits);
    my_mgr.free(d_hcov_rhits);
    gettimeofday(&end, NULL);
    gstats[22].gpu_time += compute_elapsed(&start, &end);
    embedding_time += compute_elapsed(&start, &end);

    // combine the f and r hammings into f hamming. after this, fhamming has min hamming value per read
    gettimeofday(&start, NULL);
    my_mgr.compact();
    gettimeofday(&end, NULL);
    gstats[23].gpu_time += compute_elapsed(&start, &end);
    embedding_time += compute_elapsed(&start, &end);

    gettimeofday(&start, NULL);
    //compute embedding and hamming computation for all forward hits
    uint64_t *d_fhamming, *d_rhamming;
    cout << "embed and find hamming distance of all [fwd] hits" << endl;
    gpu_merged_embed_hamming(gpu_id, d_ref, embedding, rlen, d_uniq_fhits, unique_fhits, d_fQembed, nreads, NULL, &d_fhamming);
    cout << "embed and find hamming distance of all [rev] hits" << endl;
    gpu_merged_embed_hamming(gpu_id, d_ref, embedding, rlen, d_uniq_rhits, unique_rhits, d_rQembed, nreads, NULL, &d_rhamming);
    my_mgr.free(d_fQembed);
    my_mgr.free(d_rQembed);

    // Note: some reads may have no hits, so use nreads_fhas_hit/nreads_rhas_hit
    uint64_t *d_fhit_offset_per_read = NULL, *d_rhit_offset_per_read = NULL;
    int nreads_fhas_hit = 0, nreads_rhas_hit = 0;
    gpu_hit_offset_per_read(gpu_id, unique_fhits, d_uniq_fhits, nreads_fhas_hit, &d_fhit_offset_per_read);
    gpu_hit_offset_per_read(gpu_id, unique_rhits, d_uniq_rhits, nreads_rhas_hit, &d_rhit_offset_per_read);
    my_mgr.free(d_uniq_fhits);
    my_mgr.free(d_uniq_rhits);

    uint64_t *d_min_fhamming, *d_min_rhamming;
    gpu_get_min_hamming(gpu_id, unique_fhits, max_hamming, d_fhamming, d_fhit_offset_per_read, nreads_fhas_hit, &d_min_fhamming);
    gpu_get_min_hamming(gpu_id, unique_rhits, max_hamming, d_rhamming, d_rhit_offset_per_read, nreads_rhas_hit, &d_min_rhamming);

    //if same embed dist, take the hcov as priority
    gpu_get_best_pos(nreads_fhas_hit, d_hcov_fhamming, d_min_fhamming);
    gpu_get_best_pos(nreads_rhas_hit, d_hcov_rhamming, d_min_rhamming);
    my_mgr.free(d_hcov_fhamming);
    my_mgr.free(d_hcov_rhamming);

    //TODO: get secMin_hamming for mapq

    uint64_t *h_min_fhamming, *h_min_rhamming;
    h_min_fhamming = reinterpret_cast<uint64_t *>(h_memmgr->alloc(2 * nreads_fhas_hit * sizeof(uint64_t)));
    assert(h_min_fhamming);
    cuda(Memcpy(h_min_fhamming, d_min_fhamming, 2 * nreads_fhas_hit * sizeof(uint64_t), cudaMemcpyDeviceToHost));
    h_min_rhamming = reinterpret_cast<uint64_t *>(h_memmgr->alloc(2 * nreads_rhas_hit * sizeof(uint64_t)));
    assert(h_min_rhamming);
    cuda(Memcpy(h_min_rhamming, d_min_rhamming, 2 * nreads_rhas_hit * sizeof(uint64_t), cudaMemcpyDeviceToHost));
    //cpy d_rev_str to read
    char *h_fwd, *h_rev, *h_rev_str;
    h_rev_str = reinterpret_cast<char *>(h_memmgr->alloc(nreads*rlen));
    assert(h_rev_str);
    cuda(Memcpy(h_rev_str, d_rev_str, nreads*rlen, cudaMemcpyDeviceToHost));
    if(toExtend){
         //only needed for extension
        h_fwd = reinterpret_cast<char *>(h_memmgr->alloc(nreads*rlen));
        assert(h_fwd);
        cuda(Memcpy(h_fwd, d_fParsed, nreads*rlen, cudaMemcpyDeviceToHost));
        h_rev = reinterpret_cast<char *>(h_memmgr->alloc(nreads*rlen));
        assert(h_rev);
        cuda(Memcpy(h_rev, d_rParsed, nreads*rlen, cudaMemcpyDeviceToHost));
    }
    my_mgr.free(d_min_fhamming);
    my_mgr.free(d_min_rhamming);
    my_mgr.free(d_rev_str);
    my_mgr.free(d_fParsed);
    my_mgr.free(d_rParsed);
    my_mgr.compact();

    gettimeofday(&end, NULL);
    gstats[24].gpu_time += compute_elapsed(&start, &end);
    embedding_time += compute_elapsed(&start, &end);

    // unlock gpu mutex
    g_gpu_mutex[gpu_id].unlock();

    // build align params now
    gettimeofday(&start, NULL);

    int fidx = 0, ridx = 0, Qid = 0;
    do {
        //fill the rev_str
        memcpy((*(ptlread+Qid)).rev_str, h_rev_str + Qid*rlen, rlen);
        *((*(ptlread+Qid)).rev_str+rlen) = '\0';
        if(toExtend){
             //only needed for extension
             memcpy((*(ptlread+Qid)).fwd, h_fwd + Qid*rlen, rlen);
             *((*(ptlread+Qid)).fwd+rlen) = '\0';
             memcpy((*(ptlread+Qid)).rev, h_rev + Qid*rlen, rlen);
             *((*(ptlread+Qid)).rev+rlen) = '\0';
         }

        // Find first entry in h_fhamming for Qid
        bool has_fhit = false;
        for (; fidx < nreads_fhas_hit; ++fidx) {
            int fqid = (h_min_fhamming[fidx] & QID_MASK) >> QID_SHIFT;
            if (fqid > Qid)
                break;
            if (fqid == Qid){
                has_fhit = true;
                break;
            }
        }
        short min_fhamming = has_fhit ? ((h_min_fhamming[fidx] & HAMMING_MASK) >> HAMMING_SHIFT) : (max_hamming + 1);
        uint32_t min_fcpos = (h_min_fhamming[fidx] & CPOS_MASK);

        // Find first entry in h_rhamming for Qid
        bool has_rhit = false;
        for (; ridx < nreads_rhas_hit; ++ridx) {
            int rqid = (h_min_rhamming[ridx] & QID_MASK) >> QID_SHIFT;
            if (rqid > Qid)
                break;
            if (rqid == Qid){
                has_rhit = true;
                break;
            }
        }
        short min_rhamming = has_rhit ? ((h_min_rhamming[ridx] & HAMMING_MASK) >> HAMMING_SHIFT) : (max_hamming + 1);
	    uint32_t min_rcpos = (h_min_rhamming[ridx] & CPOS_MASK);

        Region r;
        short mapq;
        if (min_fhamming < min_rhamming || (min_fhamming == min_rhamming && min_fcpos < min_rcpos )) {
            // Choose forward strand
            r.beg = r.pos = min_fcpos;
            r.cov = r.embed_dist = min_fhamming;
            r.str = '+';

            // mapq
            short minsec_fhamming = ((h_min_fhamming[fidx + nreads_fhas_hit] & HAMMING_MASK) >> HAMMING_SHIFT);
            if (minsec_fhamming > max_hamming && min_rhamming > max_hamming){
                mapq = get_mapq(min_fhamming, 0, false, max_hamming);
            } else{
                short sec_min_ham = minsec_fhamming < min_rhamming ? minsec_fhamming : min_rhamming;
                mapq = get_mapq(min_fhamming, sec_min_ham, true, max_hamming);
            }

        } else {
            // Choose reverse strand
            r.beg = r.pos = min_rcpos;
            r.cov = r.embed_dist = min_rhamming;
            r.str = '-';

            // mapq
            short minsec_rhamming = ((h_min_rhamming[ridx + nreads_rhas_hit] & HAMMING_MASK) >> HAMMING_SHIFT);
            if (minsec_rhamming > max_hamming && min_fhamming > max_hamming){
                mapq = get_mapq(min_rhamming, 0, false, max_hamming);
            } else{
                short sec_min_ham = minsec_rhamming < min_fhamming ? minsec_rhamming : min_fhamming;
                mapq = get_mapq(min_rhamming, sec_min_ham, true, max_hamming);
            }
        }
        (*(ptlread+Qid)).best_region = r;
        (*(ptlread+Qid)).mapq = mapq;
        Qid++;
    } while (Qid < nreads);

    outputQ -> push(make_tuple(ptlread, nreads));

    h_memmgr->free(h_min_fhamming);
    h_memmgr->free(h_min_rhamming);
    h_memmgr->free(h_rev_str);
    if(toExtend) {
        h_memmgr->free(h_fwd);
        h_memmgr->free(h_rev);
    }
    h_memmgr->compact();

    gettimeofday(&end, NULL);
    gstats[25].gpu_time += compute_elapsed(&start, &end);
    embedding_time += compute_elapsed(&start, &end);

#endif
}

void AccAlign::print_paired_sam(Read &tlread, Read &tlread2) {
    struct timeval start, end;
    gettimeofday(&start, NULL);

    ostringstream so;
    char strand1 = tlread.strand;
    char strand2 = tlread2.strand;
    string name1 = tlread.name;
    so << name1.substr(0, name1.find_last_of("/")) << '\t';
    uint16_t flag = 0x1;
    if (strand1 == '*')
        flag |= 0x4;
    if (strand2 == '*')
        flag |= 0x8;
    if (!(flag & 0x4) && !(flag & 0x8))
        flag |= 0x2;
    if (strand1 == '-')
        flag |= 0x10;
    if (strand2 == '-')
        flag |= 0x20;
    flag |= 0x40;
    so << flag;
    so << '\t' << (strand1 == '*' ? "*" : name[tlread.tid]);
    so << '\t' << (strand1 == '*' ? 0 : tlread.pos);
    so << '\t' << (strand1 == '*' ? 0 : (int) tlread.mapq) << '\t';
    if (strand1 == '*')
        so << "*";
    else
        so << tlread.cigar;
    if (strand2 != '*')
        so << '\t' << name[tlread2.tid] << '\t' << tlread2.pos << "\t0\t";
    else
        so << "\t*\t0\t0\t";
    if (strand1 == '-') {
        so << tlread.rev_str << "\t";
        std::reverse(tlread.qua, tlread.qua+strlen(tlread.qua));
        so << tlread.qua;
    } else {
        so << tlread.seq << '\t' << tlread.qua;
    }
    so << endl;

    //read2
    //so.str("");
    string name2 = tlread.name;
    so << name2.substr(0, name2.find_last_of("/")) << '\t';
    flag = 0x1;
    if (strand2 == '*')
        flag |= 0x4;
    if (strand1 == '*')
        flag |= 0x8;
    if (!(flag & 0x4) && !(flag & 0x8))
        flag |= 0x2;
    if (strand2 == '-')
        flag |= 0x10;
    if (strand1 == '-')
        flag |= 0x20;
    flag |= 0x80;
    so << flag;
    so << '\t' << (strand2 == '*' ? "*" : name[tlread2.tid]);
    so << '\t' << (strand2 == '*' ? 0 : tlread2.pos);
    so << '\t' << (strand2 == '*' ? 0 : (int) tlread2.mapq) << '\t';
    if (strand2 == '*')
        so << "*";
    else
        so << tlread2.cigar;
    if (strand1 != '*')
        so << '\t' << name[tlread.tid] << '\t' << tlread.pos << "\t0\t";
    else
        so << "\t*\t0\t0\t";
    if (strand2 == '-') {
        so << tlread2.rev_str << "\t";
        std::reverse(tlread2.qua, tlread2.qua + strlen(tlread2.qua));
        so << tlread2.qua;
    } else {
        so << tlread2.seq << '\t' << tlread2.qua;
    }
    so << endl;

    {
        std::lock_guard<std::mutex> guard(sam_mutex);
        if (sam_name.length()) {
            sam_stream << so.str();
            sam_stream.flush();
        } else {
            cout << so.str();
        }
    }

    gettimeofday(&end, NULL);
    sam_time += compute_elapsed(&start, &end);
}

void AccAlign::print_sam(Read &R) {
    struct timeval start, end;
    gettimeofday(&start, NULL);
    stringstream ss;
    ss << R.name << "\t";
    if (R.strand == '*') {
        ss << "4\t*\t0\t255\t*\t*\t0\t0\t";
    } else {
        ss << (R.strand == '+' ? 0 : 16) << "\t" <<
           name[R.tid] << "\t" <<
           R.pos << "\t" <<
           (int) R.mapq << "\t" <<
           R.cigar << "\t*\t0\t0\t";
    }

    if (R.strand == '-') {
        ss << R.rev_str << "\t";
        std::reverse(R.qua, R.qua+strlen(R.qua));
        ss << R.qua;
    } else {
        ss << R.seq << "\t" << R.qua;
    }

    if (R.strand != '*')
        ss << "\tAS:i:" << R.as;
    ss << endl;

    {
        std::lock_guard<std::mutex> guard(sam_mutex);
        if (sam_name.length()) {
            sam_stream << ss.str();
            //sam_stream.flush();
        } else {
            cout << ss.str();
        }
    }
    gettimeofday(&end, NULL);
    sam_time += compute_elapsed(&start, &end);
}

void AccAlign::snprintf_sam(Read &R, string *s) {
    struct timeval start, end;
    gettimeofday(&start, NULL);

    // 50 is the approximate length for all int
    int size;
    if (!toExtend){
        size = 50;
    }else{
        size = 50 + strlen(R.seq); //assume the length of cigar will not longer than the read
    }

    if (R.strand == '*'){
        size += strlen(R.name) + 2*strlen(R.seq);
        char buf[size];
        snprintf(buf, size, "%s\t%d\t%s\t%d\t%d\t%s\t*\t0\t0\t%s\t%s\tAS:i:%d\n",
                 R.name, 4, '*', 0, 255, '*', R.seq, R.qua, '\0');
        *s = buf;
    } else{
        size += strlen(R.name) + name[R.tid].length() + 2 * strlen(R.seq);
        char buf[size];
        if (R.strand == '+'){
            snprintf(buf, size, "%s\t%d\t%s\t%d\t%d\t%s\t*\t0\t0\t%s\t%s\tAS:i:%d\n",
                     R.name, 0, name[R.tid].c_str(), R.pos, (int) R.mapq, R.cigar,
                     R.seq, R.qua, R.as);
        }else{
            std::reverse(R.qua, R.qua+strlen(R.qua));
            snprintf(buf, size, "%s\t%d\t%s\t%d\t%d\t%s\t*\t0\t0\t%s\t%s\tAS:i:%d\n",
                     R.name, 16, name[R.tid].c_str(), R.pos, (int) R.mapq, R.cigar,
                     R.rev_str, R.qua, R.as);
        }
        *s = buf;
    }

    gettimeofday(&end, NULL);
    sam_pre_time += compute_elapsed(&start, &end);
}

void AccAlign::out_sam(string *s) {
    struct timeval start, end;
    gettimeofday(&start, NULL);
    {
//        std::lock_guard<std::mutex> guard(sam_mutex);
        if (sam_name.length()) {
            sam_stream << *s;
            //sam_stream.flush();
        } else {
            cout << *s;
        }
    }
    gettimeofday(&end, NULL);
    sam_out_time += compute_elapsed(&start, &end);
}

void AccAlign::map_block_wrapper(int tid, int my_gpu, int nreads,
                                 Read *ptlread, Read *ptlread2,
                                 tbb::concurrent_bounded_queue<ReadCnt> *outputQ) {
    struct timeval start, end;

    gettimeofday(&start, NULL);
#if ENABLE_GPU
    cerr << "Thread(" << tid << "): Running on gpu " << my_gpu << " map block on mono read " << endl;
    cudaSetDevice(my_gpu);
    gpu_enabled_map_block(my_gpu, ptlread, nreads, outputQ);

    if (ptlread2) {
        cerr << "Thread(" << tid << "): Running on gpu " << my_gpu << " map block on pair" << endl;
        gpu_enabled_map_block(my_gpu, ptlread2, nreads, outputQ);
    }
#endif
    gettimeofday(&end, NULL);

    cerr << "Thread(" << tid << "): finished map block in " << compute_elapsed(&start, &end) / 1000000 << " secs." << endl;
}

class Tbb_aligner {
    Read *all_reads;
    string *sams;
    AccAlign *acc_obj;

public:
    Tbb_aligner(Read *_all_reads, string *_sams, AccAlign *_acc_obj) :
            all_reads(_all_reads), sams(_sams), acc_obj(_acc_obj) {}

    void operator()(const tbb::blocked_range <size_t> &r) const {
        for (size_t i = r.begin(); i != r.end(); ++i) {
            //acc_obj -> align_read(*(all_reads+i));
            acc_obj -> wfa_align_read(*(all_reads+i));
            acc_obj -> snprintf_sam(*(all_reads+i), sams+i);
        }
    }
};


void AccAlign::align_wrapper(int tid, int soff, int eoff, Read *ptlread, Read *ptlread2,
        tbb::concurrent_bounded_queue<Read *> *dataQ) {
    cerr << "Thread(" << tid << "): Performing Smith Waterman extension" << endl;
    struct timeval start, end;

    if (!ptlread2) {
        // single-end read alignment
        string sams[eoff];
        gettimeofday(&start, NULL);
        tbb::task_scheduler_init init(g_ncpus);
        tbb::parallel_for(tbb::blocked_range<size_t>(soff, eoff), Tbb_aligner(ptlread, sams, this));
        gettimeofday(&end, NULL);
        alignTime += compute_elapsed(&start, &end);

        gettimeofday(&start, NULL);
        for (int i = soff; i < eoff; i++) {
            out_sam(sams+i);
        }
        gettimeofday(&end, NULL);
        sam_time += compute_elapsed(&start, &end);

        dataQ->push(ptlread);
    }
//    else {
//        //paired-end read alignment
//        for (int i = soff; i < eoff; i++) {
//            align_paired_read((*ptlread)[i], (*ptlread2)[i], (*ptlread)[i].ap, (*ptlread2)[i].ap);
//            print_paired_sam((*ptlread)[i], (*ptlread2)[i]);
//        }
//        delete ptlread;
//        delete ptlread2;
//    }
}

void ksw_align(const char *tseq, int tlen, const char *qseq, int qlen,
               int sc_mch, int sc_mis, int gapo, int gape, ksw_extz_t &ez) {
    int8_t a = sc_mch, b = sc_mis < 0 ? sc_mis : -sc_mis; // a>0 and b<0
    int8_t mat[25] = {a, b, b, b, 0, b, a, b, b, 0, b, b, a, b, 0, b, b, b, a, 0, 0, 0, 0, 0, 0};
    const uint8_t *ts = reinterpret_cast<const uint8_t *>(tseq);
    const uint8_t *qs = reinterpret_cast<const uint8_t *>(qseq);
    memset(&ez, 0, sizeof(ksw_extz_t));
    ksw_extz2_sse(0, qlen, qs, tlen, ts, 5, mat, gapo, gape, -1, -1, 0, 0, &ez);
}

void AccAlign::score_region(Read &r, char *strand, Region &region,
                            StripedSmithWaterman::Alignment &a) {
    unsigned len = strlen(r.seq);

//    cout << "Scoring region " << region.beg << " with cov " << region.cov << endl;

    // if the region has a embed distance of 0, then its an exact match
    if (!region.embed_dist || !toExtend) {
        // we found an exact match
        region.is_exact = true;

        // XXX: the scoring here of setting it to len is based on the
        // assumption that our current ssw impl. gives a best score of 150. Fix
        // this later.
        region.score = len;
    } else {
        region.is_exact = false;
        const char *ptr_ref = ref.c_str() + region.beg;
        const char *ptr_read = strand;

#if DEFAULT_ALIGNER
        aligner.Align(ptr_read, len, ptr_ref, region.end - region.beg, filter, &a);
        region.score = a.sw_score;
#else
        ksw_extz_t ez;
        if (g_mode == SHORT_READ_MODE)
            ksw_align(ptr_ref, len, ptr_read, len, 1, 4, 6, 1, ez);
        else
            ksw_align(ptr_ref, len, ptr_read, len, 1, 1, 1, 1, ez);

        stringstream cigar_string;
        int edit_mismatch = 0;
        unsigned ref_pos = 0, read_pos = 0;
        for (int i = 0; i < ez.n_cigar; i++) {
            int count = ez.cigar[i] >> 4;
            char op = "MID"[ez.cigar[i] & 0xf];
            cigar_string << count << op;
            switch (op) {
                case 'M':
                    for (int j = 0; j < count; j++, ref_pos++, read_pos++) {
                        if (ptr_ref[ref_pos] != ptr_read[read_pos])
                            edit_mismatch++;
                    }
                    break;
                case 'D':
                    edit_mismatch += count;
                    ref_pos += count;
                    break;
                case 'I':
                    edit_mismatch += count;
                    read_pos += count;
                    break;
                default:
                    assert(0);
            }
        }

        a.cigar_string = cigar_string.str();
        free(ez.cigar);
        a.ref_begin = 0;
        region.score = a.sw_score = ez.score;
        a.mismatches = edit_mismatch;
#endif //DEFAULT_ALIGNER
    }

    region.is_aligned = true;
}

void AccAlign::save_region(Read &R, size_t rlen, Region &region,
                           StripedSmithWaterman::Alignment &a) {
    if (region.is_exact) {
        R.pos = region.pos;
        sprintf(R.cigar, "%uM", (unsigned) rlen);
    } else {
        R.pos = region.beg + a.ref_begin;
        int cigar_len = a.cigar_string.size();
        strncpy(R.cigar, a.cigar_string.c_str(), cigar_len);
        R.cigar[cigar_len] = '\0';
    }
    R.tid = 0;
    for (size_t j = 0; j < name.size(); j++) {
        if (offset[j + 1] > R.pos) {
            R.tid = j;
            break;
        }
    }
    //cerr << "Saving region at pos " << R.pos << " as pos " << R.pos -
    //    offset[R.tid] + 1 << " for read " << R.name << endl;

    R.pos = R.pos - offset[R.tid] + 1;
    R.strand = region.str;
    R.as = region.score;
}

void AccAlign::align_read(Read &R) {
    struct timeval start, end;
    gettimeofday(&start, NULL);

    Region region = R.best_region;
    char *s = region.str == '+' ? R.fwd : R.rev;

    StripedSmithWaterman::Alignment a;
    size_t rlen = strlen(R.seq);
    if (!region.is_aligned)
        score_region(R, s, region, a);

    if (region.score > R.as)
        save_region(R, rlen, region, a);

//#if DBGPRINT
//    cout << "Final region saved for read " << R.name << " is at pos " <<
//        R.final_match.pos << " and has embed hamming " <<
//        R.final_match.embed_dist << " cov " << R.final_match.cov <<
//        " best cov " << R.ap.best_cov << " nbest_cov " << R.ap.nbest << endl;
//
//    unsigned final_pos = 0;
//    for (unsigned j = 0; j < nbest; j++) {
//        if (R.final_match.pos == vr[j].pos) {
//            final_pos = j;
//            //break;
//        }
//    }
//
//    cout << "Final position offset for read " << R.name << " is " << final_pos << endl;
//    if (final_pos != 0) {
//        for (unsigned j = 0; j < nbest; j++) {
//            cout << j << ":" << vr[j].pos <<
//                ":" << vr[j].embed_dist <<
//                ":" << vr[j].cov << endl;
//        }
//    }
//#endif

    gettimeofday(&end, NULL);
    sw_time += compute_elapsed(&start, &end);
}

void AccAlign::wfa_align_read(Read &R) {
    struct timeval start, end;
    gettimeofday(&start, NULL);

    Region region = R.best_region;
    size_t rlen = strlen(R.seq);
    char *text = region.str == '+' ? R.fwd : R.rev;
    const char *pattern = ref.c_str() + region.beg;

    if (toExtend){
        // Allocate MM
        mm_allocator_t* const mm_allocator = mm_allocator_new(BUFFER_SIZE_8M);
        // Set penalties
        affine_penalties_t affine_penalties = {
                .match = -1,
                .mismatch = 4,
                .gap_opening = 6,
                .gap_extension = 1,
        };

        // Init Affine-WFA
        affine_wavefronts_t* affine_wavefronts = affine_wavefronts_new_complete(
                rlen,rlen,&affine_penalties,NULL,mm_allocator);
        // Align
        affine_wavefronts_align(affine_wavefronts,pattern,rlen,text,rlen);

        // Display alignment
        edit_cigar_t*  edit_cigar = &affine_wavefronts->edit_cigar;
        std::stringstream cigar;
        char last_op = edit_cigar->operations[edit_cigar->begin_offset];

        int last_op_length = 1;
        int i;
        for (i=edit_cigar->begin_offset+1;i<edit_cigar->end_offset;++i) {
            //convert to M if X
            last_op = last_op == 'X'? 'M':last_op;
            if (edit_cigar->operations[i]==last_op || (edit_cigar->operations[i] == 'X' && last_op=='M')) {
                ++last_op_length;
            } else {
                cigar << last_op_length << last_op;
                last_op = edit_cigar->operations[i];
                last_op_length = 1;
            }
        }
        cigar << last_op_length << last_op;
        int cigar_len = cigar.str().length();
        strncpy(R.cigar, cigar.str().c_str(), cigar_len);
        R.cigar[cigar_len] = '\0';

        R.as = edit_cigar_score_gap_affine(edit_cigar,&affine_penalties);

        // Free
        affine_wavefronts_delete(affine_wavefronts);
        mm_allocator_delete(mm_allocator);
    } else{
        sprintf(R.cigar, "%uM", (unsigned) rlen);
        R.as = 0;
    }

    //TODO: why R.pos = region.beg + a.ref_begin; in save_region
    R.pos = region.pos;
    R.tid = 0;
    for (size_t j = 0; j < name.size(); j++) {
        if (offset[j + 1] > R.pos) {
            R.tid = j;
            break;
        }
    }
    R.pos = R.pos - offset[R.tid] + 1;
    R.strand = region.str;

    gettimeofday(&end, NULL);
    sw_time += compute_elapsed(&start, &end);
}

void AccAlign::pairdis_filter(vector<Region> &in_regions1,
                              vector<Region> &in_regions2, vector<Region> &out_regions1,
                              vector<Region> &out_regions2, int &best1, int &best2) {
    unsigned nregions1 = in_regions1.size();
    int max_cov1 = 0, max_cov2 = 0;
    for (unsigned i = 0; i < nregions1; i++) {
        Region tmp;
        tmp.pos = in_regions1[i].pos - pairdis;
        auto start = lower_bound(in_regions2.begin(), in_regions2.end(), tmp,
                                 [](const Region &left, const Region &right) {
                                     return left.pos < right.pos;
                                 }
        );

        tmp.pos = in_regions1[i].pos + pairdis;
        auto end = upper_bound(in_regions2.begin(), in_regions2.end(), tmp,
                               [](const Region &left, const Region &right) {
                                   return left.pos < right.pos;
                               }
        );

        for (auto itr = start; itr != end; ++itr) {
            // add region1 if not already added which we track by setting
            // str to *.
            if (in_regions1[i].str != '*') {
                if (in_regions1[i].cov > max_cov1) {
                    max_cov1 = in_regions1[i].cov;
                    best1 = out_regions1.size();
                }
                out_regions1.push_back(in_regions1[i]);
                in_regions1[i].str = '*';
            }

            // add region2 if not already added
            if (itr->str != '*') {
                if (itr->cov > max_cov2) {
                    max_cov2 = itr->cov;
                    best2 = out_regions2.size();
                }
                out_regions2.push_back(*itr);
                itr->str = '*';
            }
        }
    }
}

void AccAlign::align_paired_read(Read &mate1, Read &mate2) {

    /* Now do the alignment */
    struct timeval start, end;
    gettimeofday(&start, NULL);

    Region r1 = mate1.best_region;
    Region r2 = mate2.best_region;
    char *s1 = r1.str == '+' ? mate1.fwd : mate1.rev;
    char *s2 = r2.str == '+' ? mate2.fwd : mate2.rev;
    size_t rlen = strlen(mate1.seq);

    StripedSmithWaterman::Alignment a1, a2;
    if (!r1.is_aligned)
        score_region(mate1, s1, r1, a1);
    if (!r2.is_aligned)
        score_region(mate2, s2, r2, a2);

    // if combined score is better than what we have on file, record this.
    if (r1.score + r2.score > mate1.as + mate2.as) {
        save_region(mate1, rlen, r1, a1);
        save_region(mate2, rlen, r2, a2);
    }

    gettimeofday(&end, NULL);
    sw_time += compute_elapsed(&start, &end);
}

void AccAlign::sam_header(void) {
    ostringstream so;

    so << "@HD\tVN:1.3\tSO:coordinate\n";
    for (size_t i = 0; i < name.size(); i++)
        so << "@SQ\tSN:" << name[i] << '\t' << "LN:" << offset[i + 1] - offset[i] << '\n';
    so << "@PG\tID:AccAlign\tPN:AccAlign\tVN:0.0\n";

    if (sam_name.length())
        sam_stream << so.str();
    else
        cout << so.str();
}

void AccAlign::open_output(string &out_file) {
    sam_name = out_file;

    if (out_file.length()) {
        cerr << "setting output file as " << out_file << endl;
        sam_stream.open(out_file);
    } else {
        assert(g_batch_file.length() == 0);
        setvbuf(stdout, NULL, _IOFBF, 16 * 1024 * 1024);
    }

    sam_header();
}

void AccAlign::close_output() {
    if (sam_name.length()) {
        cerr << "Closing output file " << sam_name << endl;
        sam_stream.close();
    }
}

AccAlign::AccAlign(Reference &r) :
        ref(r.ref), name(r.name),
        offset(r.offset), hash_keys(r.hash_keys),
        keyv(r.keyv), posv(r.posv), nposv(r.nposv),
        d_keyv(r.d_keyv), d_posv(r.d_posv), d_ref(r.d_ref) {
    aligner.Clear();
    aligner.ReBuild(1, 4, 6, 1);

    input_io_time = parse_time = 0;
    seeding_time = probe_time = hit_count_time = 0;
    vpair_build_time = vpair_sort_time = 0;
    embedding_time = lsh_time = 0;
    pg_vote_time = nonpg_vote_time = sw_time = sam_time = sam_pre_time = sam_out_time = 0;
    vpair_sort_count = 0;
    h_seed_hash = NULL;

    if (g_embed_file.size())
        embedding = new Embedding(g_embed_file.c_str());
    else
        embedding = new Embedding;

#if 0
    // benchmark embedding
    size_t limit = ref.size() - kmer_len + 1;
    int ninput = 0;
    vector<const char *> input;
    vector<uint32_t> in_pos;
    const char *ref_ptr = ref.c_str();
    for(unsigned i = 0; i < limit; i += kmer_step) {
        input.push_back(ref_ptr + i);
        in_pos.push_back(i);
        ++ninput;

#define NBENCH_INPUT 262144
        if (ninput == NBENCH_INPUT)
            break;
    }

    struct timeval start, end;
    gettimeofday(&start, NULL);
    vector<string> output;
    output.resize(NBENCH_INPUT * embedding->num_str);
    for (int j = 0; j < NBENCH_INPUT * embedding->num_str; j++)
        output[j].resize(embedding->num_lsh);
    cerr << "Embedding vector of " << ninput << " strings." << endl;
    //tbb::parallel_for(tbb::blocked_range<size_t>(0, ninput),
    //        EmbedWrapper(embedding, input, output));
    embedding->embeddata(input, output, true);
    gettimeofday(&end, NULL);
    cerr << "Embedded 1M strings into strings in " <<
        (float)compute_elapsed(&start, &end) / 1000000 << " secs\n";

    gettimeofday(&start, NULL);
    cerr << "Embedding vector on gpu" << endl;
    vector<string> gpu_output;
    gpu_output.resize(NBENCH_INPUT * embedding->num_str);
    for (int j = 0; j < NBENCH_INPUT * embedding->num_str; j++)
        gpu_output[j].resize(embedding->num_lsh);
    //gpu_embed_data_by_pos(d_ref, embedding, in_pos, g_readlen, gpu_output);
    //gpu_embed_data_by_str(embedding, input, g_readlen, gpu_output);
    string Q(input[0]);
    vector<short> hamming;
    hamming.resize(in_pos.size() + 1);
    benchmark_gpu_embed(d_ref, embedding, in_pos, Q, g_readlen, hamming);
    gettimeofday(&end, NULL);
    cerr << "Embedded 1M strings into strings on gpu in " <<
        (float)compute_elapsed(&start, &end) / 1000000 << " secs\n";

    //for (unsigned i = 0; i < output.size(); i++) {
    //    assert (output[i] == gpu_output[i]);
    //}

    print_stats();

    exit(0);
#endif
}

AccAlign::~AccAlign() {
    delete embedding;
#if ENABLE_GPU
    cuda(FreeHost(h_seed_hash));
#endif
}

//void process_input(int tid, Reference &r, string first, string pair, string output) {
//    size_t per_file_begin = time(NULL);
//    AccAlign f(r);
//    f.open_output(output);
//
//    if (!pair.length()) {
//        f.fastq(first.c_str(), "\0", tid % g_ngpus);
//
//        cerr << "Time to align input " << first.c_str() << " : " <<
//             time(NULL) - per_file_begin << " secs" << endl;
//
//    } else {
//        f.fastq(first.c_str(), pair.c_str(), tid % g_ngpus);
//
//        cerr << "Time to align inputs " << first.c_str() << "," <<
//             pair.c_str() << " : " << time(NULL) - per_file_begin <<
//             " secs" << endl;
//
//    }
//
//    f.print_stats();
//    f.close_output();
//
//    cerr << "Total time for thread(" << tid << "): " <<
//         time(NULL) - per_file_begin << " secs" << endl;
//}

int main(int ac, char **av) {
    if (ac < 3) {
        print_usage();
        return 0;
    }

    int opn = 1;
    int kmer_temp = 0;
    while (opn < ac) {
        bool flag = false;
        if (av[opn][0] == '-') {
            if (av[opn][1] == 'f') {
                g_filter = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 't') {
                g_ncpus = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'l') {
                kmer_temp = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'n') {
                g_nreads = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'g') {
                g_ngpus = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'o') {
                g_out = av[opn + 1];
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'e') {
                g_embed_file = av[opn + 1];
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'b') {
                g_batch_file = av[opn + 1];
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'h') {
                g_hit_threshold = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'm') {
                g_mode = atoi(av[opn + 1]);
                opn += 2;
                flag = true;
            }
            else if (av[opn][1] == 'x') {
                toExtend = false;
                opn += 1;
                flag = true;
            }
            else { print_usage(); }
        }
        if (!flag)break;
    }
    if (kmer_temp != 0)kmer_len = kmer_temp;
    mask = kmer_len == 32 ? ~0 : (1ULL << (kmer_len * 2)) - 1;

    cerr << "Using " << g_ncpus << " cpus " << endl;
    cerr << "Using " << g_ngpus << " gpus " << endl;
    cerr << "Using kmer length " << kmer_len << " and step size " << kmer_step << endl;
    cerr << "Running in " << (g_mode == SHORT_READ_MODE ? "short" : "long ") <<
         " read mode\n";

#if ENABLE_GPU
    /* initialize gpu mutex based on ngpus */
    if (!g_ngpus) {
        cudaGetDeviceCount(&g_ngpus);
        assert(g_ngpus);
    }
    g_gpu_mutex = new std::mutex[g_ngpus];
    g_memmgr = new GPUMemMgr[g_ngpus];
    for (int i = 0; i < g_ngpus; i++) {
        g_memmgr[i].init(i);
    }
    h_memmgr = new HostMemMgr;
    h_memmgr->init();
#endif

#if ENABLE_GPU
#else
    tbb::task_scheduler_init init(g_ncpus);
#endif
    make_code();

    // set affinity of this thread
    // XXX: We have to make sure that hashtable particularly is in same socket
    // as lookup threads. otherwise, there's a bit of a slowdown. For now, we
    // just set first thread to cpu 0. Make this better.
//    set_affinity(pthread_self(), ROOT_CORE_ID);

    // load reference once
    Reference *r = new Reference(av[opn]);
    opn++;

    size_t total_begin = time(NULL);
    int nprocessed = 0;

    if (g_batch_file.length()) {
        /* batch file lists pair1,pair2,output one per line.
         * for each create a new AccAlign object and align them
         */
        ifstream batch_stream(g_batch_file);
        vector<string> file_names;
        vector<thread> threads;
        string line;
        size_t pos;

        while (getline(batch_stream, line)) {
            string delimiter = ",";
            file_names.clear();
            while ((pos = line.find(delimiter)) != string::npos) {
                file_names.push_back(line.substr(0, pos));
                line.erase(0, pos + delimiter.length());
            }
            assert(file_names.size() <= 2);

//            if (file_names.size() == 1) {
//                threads.push_back(thread(process_input, nprocessed,
//                                         std::ref(*r), file_names[0], string(""), line));
//            } else {
//                threads.push_back(thread(process_input, nprocessed,
//                                         std::ref(*r), file_names[0], file_names[1], line));
//            }

            nprocessed++;
        }

        for (auto &t : threads)
            t.join();
    } else {
        auto start = std::chrono::system_clock::now();

        AccAlign f(*r);
        f.open_output(g_out);
#if ENABLE_GPU
        if (opn == ac - 1) {
            f.fastq(av[opn]);
        } else if (opn == ac - 2) {
//            f.fastq(av[opn], av[opn + 1], -1);
        } else {
            print_usage();
            return 0;
        }
#else
        if (opn == ac - 1) {
            f.tbb_fastq(av[opn], "\0", -1);
        } else if (opn == ac - 2) {
            f.tbb_fastq(av[opn], av[opn + 1], -1);
        } else {
            print_usage();
            return 0;
        }
#endif

        auto end = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        cerr << "Time to align: " << elapsed.count() << "msec\n";

        f.print_stats();
        f.close_output();
    }

    delete r;

#if ENABLE_GPU
    delete[] g_gpu_mutex;
    delete[] g_memmgr;
#endif

    cerr << "Total time: " << (time(NULL) - total_begin) << " secs\n";

    return 0;
}
