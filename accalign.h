#pragma once
class AccAlign{
    private:
        std::string &ref;
        std::vector<std::string> &name;
        std::vector<uint32_t> &offset, &hash_keys;
        Embedding *embedding;

        float input_io_time, parse_time;
        float seeding_time, probe_time, hit_count_time, vpair_build_time, vpair_sort_time;
        float embedding_time, lsh_time, swap_time;
        float pg_vote_time, nonpg_vote_time, sw_time, sam_time, sam_pre_time, sam_out_time;
        long vpair_sort_count;
        StripedSmithWaterman::Aligner aligner;
        StripedSmithWaterman::Filter filter;
        std::vector<Read> read;
        std::vector<Read> read2;

        // output fields
        std::string sam_name;
        std::ofstream sam_stream;
        std::mutex sam_mutex;

        int getdata(bool is_paired, int sz, gzFile &in1,
                vector<Read> *ptlread, gzFile &in2, vector<Read> *ptlread2);
        void cpu_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *inputQ,
                         tbb::concurrent_bounded_queue<ReadCnt> *outputQ);
        void gpu_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *inputQ,
                tbb::concurrent_bounded_queue<ReadCnt> *outputQ, int gpu_id);
        void output_root_fn(tbb::concurrent_bounded_queue<ReadCnt> *outputQ,
                            tbb::concurrent_bounded_queue<Read *> *dataQ);
        void map_block_wrapper(int tid, int my_gpu, int nreads,
                Read *ptlread, Read *ptlread2,
                tbb::concurrent_bounded_queue<ReadCnt> *outputQ);
        void align_wrapper(int tid, int soff, int eoff,
                Read *ptlread, Read *ptlread2,
                tbb::concurrent_bounded_queue<Read *> *dataQ);
        void gpu_enabled_map_block(int gpu_id, Read *ptlread,
                int nreads, tbb::concurrent_bounded_queue<ReadCnt> *outputQ);
        void cpu_embed_map_block(vector<Read> *ptlread, int soff, int eoff);
        void embed_wrapper(Read &R, bool ispe, vector<Region> &fregion,
                vector<Region> &rregion, int &fbest, int &fnext, int &rbest,
                int &rnext);
        void pghole_wrapper(Read &R, vector<Region> &fcandidate_regions,
                vector<Region> &rcandidate_regions, int &fbest, int &rbest);
        void pigeonhole_query(char *Q, size_t rlen, vector<Region> &candidate_regions,
                char S, int err_threshold, int &best);
        void pairdis_filter(vector<Region> &in_regions1,
                vector<Region> &in_regions2, vector<Region> &out_regions1,
                vector<Region> &out_regions2, int &best1, int &best2);
        void query(std::string &Q, vector<Region> &candidate_regions,char S);
        void lsh_filter(char *Q, size_t rlen,
                vector<Region> &candidate_regions, int &best_threshold,
                int &next_threshold, int &best_idx, int &next_idx);
	void set_mapq(Read &R, vector<Region> &fcandidate_regions,
		vector<Region> &rcandidate_regions, int fbest, int fnext,
		int rbest, int rnext);
        void mark_for_extension(Read &read, char S, Region &cregion);
        void save_region(Read &R, size_t rlen, Region &region,
                StripedSmithWaterman::Alignment &a);
        void score_region(Read &r, char *strand, Region &region,
                StripedSmithWaterman::Alignment &a);
        void print_paired_sam(std::vector<Read> &read1, std::vector<Read> &read2, int soff, int eoff);
        void sam_header(void);
        int get_mapq(int best, int secbest, bool hasSecbest, int rlen);

		public:
        uint32_t *keyv, *posv;
        int nposv;
        uint32_t **d_keyv, **d_posv;
        uint64_t **d_ref;
        size_t *h_seed_hash;

        void open_output(std::string &out_file);
        void close_output();
        bool fastq(const char *F, const char *F2, int my_gpu);
        bool fastq(const char *F1);
        bool tbb_fastq(const char *F, const char *F2, int my_gpu);
        void print_stats();
        void map_read(Read &R);
        void align_read(Read &R);
        void print_sam(Read &R);
        void out_sam(string *sam);
        void snprintf_sam(Read &R, string *s);
        void map_paired_read(Read &mate1, Read &mate2);
        void align_paired_read(Read &mate1, Read &mate2);
        void print_paired_sam(Read &tlread, Read &tlread2);
        void wfa_align_read(Read &R);
        void del_root_fn(tbb::concurrent_bounded_queue<Read *> *dataQ, int size);
        AccAlign(Reference &r);
        ~AccAlign();
};


